var isAndroid = null;
$(document).ready(function () {
    $.init();
    var u = navigator.userAgent;
    isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1;

    $(document).on('click', '.create-actions', function (e) {
        if(isAndroid){// 是安卓
            $("#cameraInput_andriod").click();
            // $('#cameraInput_andriod').trigger('click');
        }else{
            $("#cameraInput").click();
            $('#cameraInput').trigger('click');
        }
    });
});



String.prototype.replaceAll = function (s1, s2) {
    return this.replace(new RegExp(s1, "gm"), s2);
}

//
// $('#cameraInput_andriod').on('change', function () { // 该方法在这里找不到
$(document).on('change', '#cameraInput_andriod', function (e) { // 该种写法可以保证找到并进入需要的方法

    $.showIndicator();
    lrz(this.files[0])
        .then(function (rst) {
            var submitData={
                imgData:rst.base64,
                imgPath:$('#cameraInput_andriod').val(),
            };
            $('#cameraInput_andriod').val("");
            $.ajax({
                type: "POST",
                url: "/product/uploadImg",
                data: submitData,
                timeout: 50000,
                success: function(data){
                    $.hideIndicator();
                    console.log("data.realPath=="+data.realPath);
                    if (data.realPath == null || data.realPath == "") {
                        $.toast("上传异常,请重试");
                        return;
                    }
                    addImg(data.realPath);
                    if ($(".imgs").length > 6) {
                        $("#addli").hide();
                    } else {
                        $("#addli").show();
                    }
                },
                complete :function(XMLHttpRequest, textStatus){
                },
                error:function(XMLHttpRequest, textStatus, errorThrown){ //上传失败
                    $.toast("出现错误,请重试");
                }
            });
        })
        .catch(function (err) {
            // 处理失败会执行
            $.toast("出现错误,请重试");
        })
        .always(function () {
            // 不管是成功失败，都会执行
        });
});

$(document).on('change', '#cameraInput', function (e) {
    $.showIndicator();
    lrz(this.files[0])
        .then(function (rst) {
            var submitData={
                imgData:rst.base64,
                imgPath:$('#cameraInput').val(),
            };
            $('#cameraInput').val("");
            $.ajax({
                type: "POST",
                url: "/product/uploadImg",
                data: submitData,
                timeout: 50000,
                success: function(data){
                    $.hideIndicator();
                    if (data.realPath == null || data.realPath == "") {
                        $.toast("上传异常,请重试");
                        return;
                    }
                    addImg(data.realPath);
                    if ($(".imgs").length > 6) {
                        $("#addli").hide();
                    } else {
                        $("#addli").show();
                    }
                },
                complete :function(XMLHttpRequest, textStatus){
                },
                error:function(XMLHttpRequest, textStatus, errorThrown){ //上传失败
                    $.toast("出现错误,请重试");
                }
            });
        })
        .catch(function (err) {
            // 处理失败会执行
            $.toast("出现错误,请重试");
        })
        .always(function () {
            // 不管是成功失败，都会执行
        });
});

function addImg(src) {
    console.log("src==="+src)
    $("#img_src").attr("src",src + "?x-oss-process=image/resize,m_fill,h_400,w_400");
    $('#img_src_hidden').val(src + "?x-oss-process=image/resize,m_fill,h_400,w_400");
    var img_tmp = $("#img_tmp").clone();
    //去掉存放图片信息标签的ID,防止下次上传图片时出现问题
    img_tmp.find('input').removeAttr('id');
    img_tmp.find('img').removeAttr('id');
    var img_htm = img_tmp.html();
    // img_htm = img_htm.replaceAll("imgSrc", src + "?x-oss-process=image/resize,m_fill,h_800,w_800");
    var $li = img_tmp.html(img_htm).find("dd");
    $("#addli").before($li);
    var imgLi = $("#addli").prev("dd");
    imgLi.find("a").bind("click",function(){
        $(this).parent().remove();
        if ($(".imgs").length > 6) {
            $("#addli").hide();
        } else {
            $("#addli").show();
        }
    })
    img_tmp.remove();
}

