package order;

import play.db.jpa.Model;
import product.ProductSpec;

import javax.persistence.*;

/**
 * Created by liming on 16/7/18.
 */
@Entity
@Table(name = "admin_order_item_splite")
public class AdminOrderItemSplite extends Model {
    /**
     * 订单
     */
    @ManyToOne
    @JoinColumn(name = "admin_order")
    public AdminOrder adminOrder;

    /**
     * 关联订单明细Id
     */
    @ManyToOne
    @JoinColumn(name = "admin_order_item_id")
    public AdminOrderItem adminOrderItem;

    /**
     * 出库单Id
     */
    @Column(name = "admin_stock_out_id")
    public Long adminStockOutId;

    /**
     * 原产品
     */
    @ManyToOne
    @JoinColumn(name = "combo_product_spec_id")
    public ProductSpec comboPorductSpec;

    /**
     * 拆分产品
     */
    @ManyToOne
    @JoinColumn(name = "split_product_spec_id")
    public ProductSpec splitProductSpec;

    /**
     * 购买数量
     */
    @Column(name = "num")
    public Double num;

    /**
     * 出库时成本单价
     */
    @Column(name = "cost_price")
    public Double costPrice;

    /**
     * 分摊销售价格
     */
    @Column(name = "sale_price")
    public Double salePrice;

}
