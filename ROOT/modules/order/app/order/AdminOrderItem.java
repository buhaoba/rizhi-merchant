package order;

import enums.GoodsStatus;
import enums.OrderItemStatus;
import enums.OrderItemType;
import goods.Goods;
import models.BaseModel;
import models.constants.DeletedStatus;
import product.merchant.ProductSpecMerchant;
import util.common.ConvertUtil;
import util.common.UStringUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by liming on 16/7/18.
 */
@Entity
@Table(name = "admin_order_item")
public class AdminOrderItem extends BaseModel {

    public static final String CACHE_BY_ORDER_ID = "CACHE_BY_ORDER_ID_";

    /**
     * 商品信息
     */
    @JoinColumn(name = "goods_id", nullable = true)
    @ManyToOne
    public Goods goods;

    /**
     * 订单信息
     */
    @JoinColumn(name = "admin_order", nullable = true)
    @ManyToOne
    public AdminOrder adminOrder;

    /**
     * 购买数量
     */
    @Column(name = "buy_number")
    public Double buyNumber = 0D;

    /**
     * 销售单价
     */
    @Column(name = "sale_price")
    public Double salePrice = 0D;

    /**
     * 总金额 = 销售单价*购买数量
     */
    @Column(name = "amount")
    public Double amount = 0D;

    /**
     * 成本价
     */
    @Column(name = "original_price")
    public Double originalPrice = 0D;

    /**
     * 合伙人价格
     */
    @Column(name = "partner_price")
    public Double partnerPrice = 0D;

    /**
     * 分摊折扣金额
     */
    @Column(name = "discount_pay")
    public Double discountPay = 0D;

    /**
     * 类型
     */
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    public OrderItemType type;

    /**
     * 状态
     */
    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    public GoodsStatus status;   //FIXME: OrderStatus or OrderItemStatus

    /**
     * 订单明细状态:
     * 正常交易
     * 拆单
     * 退款
     */
    @Column(name = "order_item_status")
    @Enumerated(EnumType.STRING)
    public OrderItemStatus orderItemStatus = OrderItemStatus.NORMAL;

    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 订单创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 更新时间
     */
    @Column(name = "updated_at")
    public Date updatedAt;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;

    @Transient
    public String url;

    /**
     * 临时产品规格
     */
    @Transient
    public ProductSpecMerchant productSpecMerchant;

    public AdminOrderItem() {
        super();
    }

    public AdminOrderItem(OrderItem orderItem, AdminOrder adminOrder) {
        this.goods = orderItem.goods;
        this.adminOrder = adminOrder;
        this.buyNumber = ConvertUtil.toDouble(orderItem.buyNumber) == null?0:ConvertUtil.toDouble(orderItem.buyNumber);
        this.amount = orderItem.amount == null?0:orderItem.amount;
        this.originalPrice = orderItem.originalPrice == null?0:orderItem.originalPrice;
        this.salePrice = orderItem.salePrice == null?0:orderItem.salePrice;
        this.partnerPrice = orderItem.partnerPrice == null?0:orderItem.partnerPrice;
        //订单总折扣金额 / 该订单条目金额 。
//        String _discountPay = new DecimalFormat("#.00").format(adminOrder.discountPay / adminOrder.amount * orderItem.facePrice );
//        this.discountPay = Double.valueOf(_discountPay);
        this.type = orderItem.type;
        this.status = orderItem.status;
        this.orderItemStatus = OrderItemStatus.NORMAL;
        this.deleted = DeletedStatus.UN_DELETED;
        this.createdAt = orderItem.createdAt;
        this.updatedAt = orderItem.updatedAt;
    }

    public static List<AdminOrderItem> findByOrderId(long orderId) {
        return AdminOrderItem.find("adminOrder.id = ?", orderId).fetch();
    }

    /**
     * 根据id删除订单明细
     * @param delItemArray
     */
    public static void delByIds(Integer[] delItemArray) {
        String delItemStr = UStringUtil.concatStr(",", delItemArray);
        AdminOrderItemSplite.delete("adminOrderItem.id in (" + delItemStr+ ")");
        AdminOrderItem.delete("id in (" + delItemStr + ")");

    }


}
