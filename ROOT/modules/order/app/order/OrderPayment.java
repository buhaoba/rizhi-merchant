package order;


import activity.give.GiveActivitySpecProduct;
import address.Address;
import build.OrderBuilder;
import coupon.DiscountCoupon;
import coupon.DiscountCouponDetail;
import coupon.WebUserCoupon;
import coupon.WebUserCouponHistory;
import enums.*;
import helper.CacheHelper;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import models.weixin.WebUser;
import models.weixin.amount.WebUserAccount;
import models.weixin.amount.WebUserAccountHistory;
import net.sf.ehcache.search.expression.Or;
import play.Logger;
import play.db.jpa.Model;
import product.enums.ProductAttribute;
import product.merchant.ProductSpecMerchant;
import shopcart.ShopCart;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by upshan on 15/7/7.
 */
@Entity
@Table(name = "order_payment")
public class OrderPayment extends Model {


    /**
     * 订单信息
     */
    @JoinColumn(name = "order_id", nullable = true)
    @ManyToOne
    public Order order;

    /**
     * 卡券信息
     */
    @JoinColumn(name = "discount_coupon_detail_id", nullable = true)
    @ManyToOne
    public DiscountCouponDetail discountCouponDetail;

    /**
     * 使用金额
     */
    @Column(name = "amount")
   public Double amount;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;


    public OrderPayment() {
        super();
    }

    public OrderPayment(Order order , DiscountCouponDetail detail , Double amount) {
        this.order = order;
        this.discountCouponDetail = detail;
        this.amount = amount;
        this.deleted = DeletedStatus.UN_DELETED;
    }


    public static List<OrderPayment> findByOrder(Order order) {
        return find("order = ? and deleted = ? order by discountCouponDetail.discountCoupon.priority" , order , DeletedStatus.UN_DELETED).fetch();
    }

    /**
     * 生成 预估折扣金额
     * @param amount
     */
    public static Double estimateDiscountPay(WebUser webUser , Double amount) {
        Double discountPayment = 0d;
        List<DiscountCouponDetail> detailList = DiscountCouponDetail.findByWebUser(webUser);
        Boolean isFirst = true;
        for(DiscountCouponDetail detail : detailList) {
            if(detail.amount != null && detail.amount > 0) {
                Double proportion = detail.discountCoupon.avgProportion == null ? 1 : detail.discountCoupon.avgProportion;
                if(isFirst) {
                    Double detailDiscountPay = amount * proportion/100;
                    Double actualDisCountPay = detail.amount > detailDiscountPay ? detailDiscountPay : detail.amount;
                    discountPayment += actualDisCountPay;

                } else {
                    if(detail.discountCoupon.isAccumulation != null && detail.discountCoupon.isAccumulation) {
                        Double detailDiscountPay = amount * proportion/100;
                        Double actualDisCountPay = detail.amount > detailDiscountPay ? detailDiscountPay : detail.amount;
                        discountPayment += actualDisCountPay;
                    }
                }
            }
        }
        return discountPayment;
    }

    /**
     * 生成 订单流水账
     * @param order
     */
    public static Double createOrderUsedCoupon(Order order) {
        Double discountPayment = 0d;
        Double amount = order.amount;
        List<DiscountCouponDetail> detailList = DiscountCouponDetail.findByWebUser(order.webUser);
        Boolean isFirst = true;
        for(DiscountCouponDetail detail : detailList) {
            if(detail.amount != null && detail.amount > 0) {
                Double proportion = detail.discountCoupon.avgProportion == null ? 1 : detail.discountCoupon.avgProportion;
                if(isFirst) {
                    //当前卡券的折扣金额
                    Double detailDiscountPay = amount * proportion/100;
                    //抵扣券实际抵扣的金额
                    Double actualDisCountPay = detail.amount > detailDiscountPay ? detailDiscountPay : detail.amount;
                    amount = amount - actualDisCountPay;
                    discountPayment += actualDisCountPay;
                    new OrderPayment(order , detail , actualDisCountPay).save();

                } else {
                    if(detail.discountCoupon.isAccumulation != null && detail.discountCoupon.isAccumulation) {
                        Double detailDiscountPay = amount * proportion/100;
                        Double actualDisCountPay = detail.amount > detailDiscountPay ? detailDiscountPay : detail.amount;
                        amount = amount-actualDisCountPay ;
                        discountPayment += actualDisCountPay;
                        new OrderPayment(order , detail , actualDisCountPay).save();
                    }
                }
            }
        }
        return discountPayment;
    }


    /**
     * 根据订单统计折扣金额
     * @param order
     * @return
     */
    public static Double statisticsDiscountPay(Order order) {
        Double discountPayment = 0d;
        List<OrderPayment> orderPaymentList = OrderPayment.findByOrder(order);
        for(OrderPayment orderPayment : orderPaymentList) {
            discountPayment += orderPayment.amount;
        }
        return discountPayment;
    }

}
