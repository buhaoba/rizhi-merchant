package order;

import enums.PaymentType;
import models.constants.DeletedStatus;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by buhaoba on 16/7/15.
 */
@Entity
@Table(name = "payment_infos")
public class PaymentInfo extends Model {

    /**
     * 订单信息
     */
    @JoinColumn(name = "order_id", nullable = true)
    @ManyToOne
    public Order order;

    /**
     * 支付类型
     */
    @Enumerated(EnumType.STRING)
    public PaymentType paymentType;


    /**
     * 支付金额
     */
    @Column(name = "amount")
    public Double amount;


    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;


    public PaymentInfo() {
        super();
    }


    public PaymentInfo(Order order , PaymentType type , Double amount) {
        this.order = order;
        this.paymentType = type;
        this.deleted = DeletedStatus.UN_DELETED;
        this.createdAt = new Date();
        this.save();

        /**
         * 代金券支付
         */
        if(type == PaymentType.ECASH) {
            //TODO 代金券扣款
            return;
        }

        /**
         * 代金券支付
         */
        if(type == PaymentType.WEIXIN) {
            //TODO 增加积分
            return;
        }

        /**
         * 积分支付
         */
        if(type == PaymentType.INTEGRAL) {
            //TODO
            return;
        }

        if(type == PaymentType.ALI_PC_DIRECT) {
            //TODO 增加积分
            return;
        }


    }






}
