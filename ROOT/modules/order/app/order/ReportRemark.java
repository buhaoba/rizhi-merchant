package order;

import enums.ReportUserType;
import models.BaseModel;
import models.constants.DeletedStatus;
import models.merchant.Merchant;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/9/7.
 */
@Entity
@Table(name = "report_remark")
public class ReportRemark extends BaseModel {

    /**
     * 用户投诉备注
     */
    @Column(name = "report_remarks")
    public String reportRemarks;


    /**
     * 用户投诉图片
     */
    @Column(name = "report_image")
    public String reportImage;

    /**
     * 投诉时间
     */
    @Column(name = "report_date")
    public Date reportDate;

    /**
     * 投诉单ID
     */
    @JoinColumn(name = "report_id", nullable = true)
    @ManyToOne
    public Report report;

    /**
     * 投诉用户类型
     */
    @Column(name = "report_user_type")
    @Enumerated(EnumType.STRING)
    public ReportUserType reportUserType;


    /**
     * 投诉用户ID
     */
    @Column(name = "report_user")
    public Long reportUser;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;



    /**
     * 商户查询 被投诉订单
     * @param merchant
     * @return
     */
    public static List<ReportRemark> getListByMerchantAndReport(Merchant merchant,Report report) {
        return ReportRemark.find(" reportUser = ?1  and report.id = ?2", merchant.id,report.id).fetch();
    }

    /**
     * 商户查询 被投诉订单
     * @param merchant
     * @return
     */
    public static List<ReportRemark> getListByTypeAndMerchantAndReport(Merchant merchant,Report report) {
        return ReportRemark.find(" reportUser = ?1  and report.id = ?2 and reportUserType = ?3", merchant.id,report.id , ReportUserType.MERCHANT).fetch();
    }

    /**
     * 商户查询 被投诉订单
     * @param merchant
     * @return
     */
    public static ReportRemark getByMerchantAndReport(Merchant merchant,Report report) {
        return ReportRemark.find("  report.id = ?1 and  reportUserType = ?2   ", report.id,ReportUserType.USER).first();
    }

    /**
     * 商户查询 被投诉订单
     * @param report
     * @return
     */
    public static List<ReportRemark> getBydReport(Report report) {
        return ReportRemark.find("  report.id = ?1   ", report.id).fetch();
    }
}
