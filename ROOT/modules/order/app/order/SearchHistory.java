package order;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import models.weixin.WebUser;

import javax.persistence.*;
import java.util.Date;

/**
 * 搜索历史记录
 * 将搜索历史记录存到数据库中
 * Created by LiuBin on 2016/7/25.
 */
@Entity
@Table(name = "search_history")
public class SearchHistory extends BaseModel {
    /**
     * 下单用户
     */
    @JoinColumn(name = "web_user_id", nullable = true)
    @ManyToOne
    public WebUser webUser;

    /**
     * 所属商户
     */
    @JoinColumn(name = "fee_merchant_id", nullable = true)
    @ManyToOne
    public Merchant merchant;

    @Column(name = "history")
    public String history;

    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 加入时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    public SearchHistory(WebUser webUser , String history) {
        this.webUser = webUser;

        this.history = history;
        this.deleted = DeletedStatus.UN_DELETED;
        this.createdAt = new Date();
    }

    /**
     * 根据下单用户 及商户获取搜索记录的商品
     * @param webUser
     * @return
     */
    public static SearchHistory findByWxUserAndMerchant(WebUser webUser) {
        return SearchHistory.find("webUser = ?1  and deleted = ?2 ", webUser, DeletedStatus.UN_DELETED).first();
    }
}
