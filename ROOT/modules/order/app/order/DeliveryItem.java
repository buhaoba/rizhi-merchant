package order;

import address.Address;
import enums.DistributeStatus;
import enums.DistributeType;
import models.BaseModel;
import models.merchant.Merchant;
import models.weixin.WebUser;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2017/10/9.
 */
@Entity
@Table(name = "delivery_item")
public class DeliveryItem extends BaseModel {

    /**
     * 订单派发类型
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "distribute_type")
    public DistributeType distributeType;

    /**
     * 订单明细ID
     */
    @JoinColumn(name = "order_item_id", nullable = true)
    @ManyToOne
    public OrderItem orderItem;

    /**
     * 订单派发状态
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "distribute_status")
    public DistributeStatus distributeStatus = DistributeStatus.UN_DISTRIBUTE;

    /**
     * 订单派发状态
     */
    @Transient
    public String distributeStatusStr;

    /**
     * 派单时间
     */
    @Column(name = "distribute_at")
    public Date distributeAt;

    /**
     * 派单时间
     */
    @Transient
    public String distributeAtStr;

    /**
     * 抢单时间
     */
    @Column(name = "rob_distribute_at")
    public Date robDistributeAt;

    /**
     * 配送员送到时间
     */
    @Column(name = "sent_at")
    public Date sentAt;

    /**
     * 用户确认收货时间
     */
    @Column(name = "web_user_sent_at")
    public Date webUserSentAt;


    /**
     * 抢单人
     */
    @JoinColumn(name = "distribute_web_user_id", nullable = true)
    @ManyToOne
    public WebUser distributeWebUser;

    /**
     * 配送费
     */
    @Column(name = "delivery_price")
    public Double deliveryPrice;

    /**
     * 关联商户
     */
    @JoinColumn(name = "merchant_id")
    @ManyToOne
    public Merchant merchant;

    /**
     * 配送地址
     */
    @JoinColumn(name = "address_id", nullable = true)
    @ManyToOne
    public Address address;


    public static DeliveryItem findOrderItemByDeli(long itemId){
        return DeliveryItem.find("  orderItem.id = ?1 ",itemId).first();
    }

    public static DeliveryItem findByOrderItemAndDistribute(long itemId , DistributeStatus distributeStatus){
        return DeliveryItem.find(" orderItem.id = ?1  and distributeStatus = ?2",itemId,distributeStatus).first();
    }

}
