package address;

import address.enums.AddressType;
import jodd.bean.BeanCopy;
import models.BaseModel;
import models.constants.DeletedStatus;
import models.weixin.WebUser;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Administrator on 2015/8/21.
 */
@Entity
@Table(name = "address")
public class Address extends BaseModel {

    /**
     * 关联用户
     */
    @JoinColumn(name = "web_user_id")
    @ManyToOne
    public WebUser webUser;

    /**
     * 收货人名字
     */
    @Column(name = "user_name")
    public String userName;

    /**
     * 收货人电话
     */
    @Column(name = "phone")
    public String phone;

    /**
     * 关联小区
     */
    @JoinColumn(name = "community_id")
    @ManyToOne
    public Community community;

    /**
     * 备用小区名称
     */
    @Column(name = "spare_community")
    public String spareCommunity;

    /**
     * 详细地址
     */
    @Column(name = "address")
    public String address;

    /**
     * 确认区域
     */
    @JoinColumn(name = "confirm_city_id")
    @ManyToOne
    public City confirmCity;

    /**
     * 内部关联小区
     */
    @JoinColumn(name = "confirm_community_id")
    @ManyToOne
    public Community confirmCommunity;

    /**
     * 内部详细地址
     */
    @Column(name = "confirm_address")
    public String confirmAddress;

    /**
     * 地址是否经过审核确认
     */
    @Column(name = "is_confirm")
    public Boolean isConfirm;

    /**
     * 是否是默认地址
     */
    @Column(name = "is_default")
    public Boolean isDefault;

    /**
     * 地址类型
     */
    @Column(name = "address_type")
    @Enumerated(EnumType.STRING)
    public AddressType addressType;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;

    /**
     * 用户手输的小区名
     */
    @Column(name = "community_name")
    public String communityName;



    public Address() {
        super();
    }

    public Address(WebUser webUser , Community community) {
        this.webUser = webUser;
        this.userName = webUser.nickName;
        this.phone = "4008019127";
        this.community = community;
        this.address = community.address;
        this.addressType = AddressType.WEIXINUSER;
        this.deleted = DeletedStatus.UN_DELETED;
        this.isDefault = Boolean.TRUE;
        this.isConfirm = Boolean.FALSE;
    }




    public static List<Address> findAddressByUserId(Long userId) {
        return Address.find("deleted  =  ?1  and  webUser.id =  ?2 ", DeletedStatus.UN_DELETED, userId).fetch();
    }

    public static Long countDefaultAddressByUserId(Long userId) {
        return Address.count("deleted  =  ?1  and  webUser.id =  ?2 and isDefault = ?3", DeletedStatus.UN_DELETED, userId , Boolean.TRUE);
    }

    //获取当前用户的默认地址
    public static Address findDefaultAddressByUser(Long userId) {
        return Address.find("deleted  =  ?1  and  webUser.id =  ?2 and isDefault = ?3 ", DeletedStatus.UN_DELETED, userId,Boolean.TRUE).first();
    }

    public static Address findAddressById(Long id) {
        return Address.find("deleted  =  ?1  and  id  =  ?2", DeletedStatus.UN_DELETED, id).first();
    }

    public static Address findAddressByUserAndId(Long userId, Long id) {
        return Address.find("deleted  =  ?1  and  webUser.id =  ?2 and id  = ?3 ", DeletedStatus.UN_DELETED, userId, id).first();
    }


    public static void updateAuditStatus(long communityId) {
        String sqlUpdate = " update address a set a.is_confirm = 0 where a.confirm_community_id = "+communityId ;
        JPA.em().createNativeQuery(sqlUpdate).executeUpdate();
    }

    public static Address findAddressNotWebCat() {
        return Address.all().first();
    }



    /**
     * Update
     *
     * @param id
     * @param address
     */
    public static void update(Long id, Address address) {
        Address oldEntity = Address.findById(id);
        BeanCopy.beans(address, oldEntity).ignoreNulls(true).copy();
        address.save();
    }

}
