package address;

import jodd.bean.BeanCopy;
import models.constants.DeletedStatus;
import play.Logger;
import play.db.jpa.Model;
import play.modules.paginate.JPAExtPaginator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 配送区域(不再使用)
 * Created by buhaoba on 16/3/19.
 */
@Entity
@Table(name = "address_distribution_address")
public class DistributionAddress extends Model {

//    /**
//     * 所属城市
//     */
//    @JoinColumn(name = "city_id")
//    @ManyToOne
//    public City city;

    /**
     * 所属配送区域
     */
    @JoinColumn(name = "parent_type_id")
    @ManyToOne
    public DistributionAddress parentType;


    /**
     * 配送区域名称
     */
    @Column(name = "name")
    public String name;


    /**
     * 排序
     */
    @Column(name = "order_by")
    public Integer orderBy;

    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;


    public static List<DistributionAddress> findAvailable(City city) {
        return DistributionAddress.find("deleted = ? and city = ? order by orderBy desc" , DeletedStatus.UN_DELETED , city).fetch();
    }


    /**
     * 修改
     * @param id
     * @param newObject
     */
    public static void update(Long id , DistributionAddress newObject) {
        DistributionAddress oldObject = DistributionAddress.findById(id);
        BeanCopy.beans(newObject, oldObject).ignoreNulls(true).copy();
        oldObject.save();
    }


//    /**
//     * 获取以及类别地址
//     */
//    public static List<DistributionAddress> findTopAddress(City city) {
//        return DistributionAddress.find("deleted = ? and parentType = null and city = ?  order by orderBy desc" , DeletedStatus.UN_DELETED , city).fetch();
//    }
//
//    /**
//     * 获取以及类别地址
//     */
//    public static List<DistributionAddress> findChildAddress(City city) {
//        return DistributionAddress.find("deleted = ? and parentType != null and city = ? and parentType.deleted = ? order by orderBy desc" , DeletedStatus.UN_DELETED , city , DeletedStatus.UN_DELETED).fetch();
//    }
//
//
//    /**
//     * 获取以及类别地址
//     */
//    public static List<DistributionAddress> findByParent(Long parentId , City city) {
//        return DistributionAddress.find("deleted = ? and parentType.id = ? and city = ? and parentType.deleted = ? order by orderBy desc" , DeletedStatus.UN_DELETED , parentId , city , DeletedStatus.UN_DELETED).fetch();
//    }




}
