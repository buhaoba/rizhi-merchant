package address;

import address.enums.CityType;
import models.BaseModel;
import models.constants.DeletedStatus;
import models.enums.TreeNodeStatus;

import javax.persistence.*;
import java.util.List;

/**
 * Created by upshan on 15/9/11.
 */
@Entity
@Table(name = "address_citys")
public class City extends BaseModel {

    /**
     * 父节点Id
     */
    @Column(name = "parent_id")
    public Long parentId;

    /**
     * 父节点Code
     */
    @Column(name = "parent_code")
    public String parentCode;

    /**
     * 节点编号
     */
    @Column(name = "code")
    public String code;

    /**
     * 区域名称
     */
    @Column(name = "name" , length = 100)
    public String  name;

    /**
     * STANDARD:标准行政单位,CUSTOM:自定义
     */
    @Column(name = "city_type")
    @Enumerated(EnumType.STRING)
    public CityType cityType;

    /**
     * 节点全路径
     */
    @Column(name = "node_id_path")
    public String nodeIdPath;

    /**
     * 区域级别
     */
    @Column(name = "level")
    public Integer level;

    /**
     * 是否为叶子节点
     */
    @Column(name = "is_leaf")
    public Boolean isLeaf;


    /**
     * 是否为叶子节点 open:叶子节点 close:非叶子节点
     */
    @Transient
    public TreeNodeStatus state;


    /**
     *排序号
     */
    @Column(name = "show_order")
    public Integer showOrder = 0;

    /**
     * 是否开通
     */
    @Column(name = "is_open")
    public Boolean isOpen;


    @Column(name = "remark")
    public String remark;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;



    /**
     * 获取开通的城市     * @return
     */
    public static List<City> findByOpenCity() {
        return City.find("isOpen = ? and level = ?" , true , 2).fetch();
    }


    /**
     * 某城市的
     * @param likeCode
     * @return
     */
    public static List<City> findByOpenCity(String likeCode) {
        return City.find("isOpen = ? and level = ? and parentCode = ?" , true , 2 , likeCode).fetch();
    }

    /**
     * 获取所有省
     * @return
     */
    public static List<City> findAllProvince(Boolean isOpen) {
        return City.find("parentCode = 1 and isOpen = ?" ,isOpen).fetch();
    }

    public static List<City> findDistrictByCity(String cityCode , Boolean isOpen) {
        return City.find("parentCode = ? and isOpen = ?" ,cityCode , isOpen).fetch();
    }

    /**
     * 获取省下的城市
     * @param provinceCode
     * @return
     */
    public static List<City> findCityByParovince(String provinceCode , Boolean isOpen) {
        return City.find("parentCode = ? and isOpen = ?" , provinceCode , isOpen).fetch();
    }

    public static City findByCode(String code) {
        return City.find("code = ? and isOpen = true" , code).first();
    }


    public static City findByName(String name) {
        return City.find("name like ? and isOpen = true" , name+"%").first();
    }

    /**
     * 查询所有的城市
     * @return
     */
    public static List<City> findAllCity(){
        return City.find("level = 2 and isOpen = true").fetch();
    }

    /**
     * 查询所有的城市
     * @return
     */
    public static List<City> findAllCity(String code){
        return City.find("level = 2 and isOpen = true and code like ?" , "37%").fetch();
    }


    /**
     * 加载孩子节点
     * @param id
     * @return
     */
    public static List<City> loadChildCity(long id) {
        List<City> cities = City.find("parentId = ? and deleted = ? order by showOrder,id desc", id, DeletedStatus.UN_DELETED).fetch();
        for(City city: cities){
            city.state= city.isLeaf !=null && city.isLeaf? TreeNodeStatus.open:TreeNodeStatus.closed;
        }

        return cities;
    }
    /**
     * 删除区域
     */
    public static void deleteCities(Long[] deleteCityList){
        for(Long cityId:deleteCityList){
            City city=City.findById(cityId);
            city.deleted=DeletedStatus.DELETED;
            city.save();
            Long brotherQty=City.loadBrotherQty(city);
            if(brotherQty==0){
                City parentCity=City.findById(city.parentId);
                parentCity.isLeaf=true;
                parentCity.save();
            }

        }
    }
    public static long loadBrotherQty(City  city) {
        return City.count("parentId = ? and deleted = ?", city.parentId, DeletedStatus.UN_DELETED);
    }

    /**
     * 检查区域是否被使用
     */
    public static Boolean checkIsUsed(long id){
        return Community.count("city.id=?",id)>0;
    }

}
