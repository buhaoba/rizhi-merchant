package address.enums;

/**
 * Created by liming on 16/6/29.
 */
public enum CityType {
    //标准行政单位
    STANDARD,
    //自定义
    CUSTOM
}
