package address.enums;

/**
 * Created by youliangcheng on 16/12/02.
 */
public enum AddressType {
    //微信用户地址
    WEIXINUSER,
    //康师傅代理客户地址
    KANGSHIFU
}
