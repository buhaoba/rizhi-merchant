package shopcart;

import enums.ShopCartStatus;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import models.weixin.WebUser;
import order.OrderPayment;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import product.Product;
import util.common.ConvertUtil;
import utils.ObjectUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LiuBin on 2016/7/14.
 */
public class ShopCartsVO {
    private  static java.text.DecimalFormat   df   =new   java.text.DecimalFormat("#.00");
    public Double sumPrice; // 总价
    public String  freightMSG; // 备注信息
    public String distributionDay; //配送时间
    public Double freight; // 运费
    public Double freePrice; // 免运费价格
    public Double allPrice; // 总价格
    public Double discountPay; // 优惠金额
    public Double paymentedAmount; //支付金额
    public List<ShopCartVO> shopCartsVOList;

    /**
     * 转换shopCarts 到 ShopCartsVO 用于页面展示
     * @param shopCarts
     * @return
     */
    public static ShopCartsVO createOrUpdateShopCartVO(List<ShopCart> shopCarts) {
        List<ShopCartVO> shopCartVOArrayList = new ArrayList<>();
        ShopCartsVO shopCartsVO = new ShopCartsVO();
        shopCartsVO.distributionDay = "明天";
        double merchantFreight = 0.00 ;
        Double sPrice = 0d; // 总价
        Boolean isPackage = false;
        for(ShopCart shopCart : shopCarts) {
            ShopCartVO vo = new ShopCartVO();
            Product product = Product.findByProductId(shopCart.goods.serialId);
            if(product == null) {
                shopCart.deleted = DeletedStatus.DELETED;
                shopCart.status = ShopCartStatus.DELETED;
                shopCart.save();
                continue;
            }
            vo.name = shopCart.goodsName;
            if(!isPackage && vo.name.indexOf("礼包") > 0) {
                isPackage = true;
            }
            vo.id = shopCart.id;
            vo.count = shopCart.buyNumber;
            vo.imgUrl = ObjectUtil.checkBlock(shopCart.goods.img) ? "/public/sui/img/p3.jpg" : shopCart.goods.img  ;
            vo.price = shopCart.salePrice;
            vo.productId = shopCart.goods.serialId;
            vo.maxCount = ConvertUtil.toInteger(product.stock!=null?product.stock : 0);
            String spec = "";
            if(StringUtils.isBlank(shopCart.goods.spec)){
                spec = product.spec != null ? product.spec : "--";
            }else {
                spec = shopCart.goods.spec ;
            }
            vo.standard = spec;
            vo.amount = Double.valueOf(df.format(vo.price * vo.count));
            shopCartVOArrayList.add(vo);
            sPrice = sPrice + (vo.price * vo.count) ; // sPrice.add(vo.price.multiply(BigDecimal.valueOf(vo.count)));
        }

        Logger.info("sPrice : %s" , sPrice);

        // 订单总金额  商品金额综合
        shopCartsVO.sumPrice = sPrice == null ? 0d : sPrice;

        shopCartsVO.sumPrice = Double.valueOf(df.format(shopCartsVO.sumPrice)) ;
        // 如果小于 25 5元运费. 大于25 运费为0
        shopCartsVO.freePrice = sPrice < merchantFreight ? 5d : 0d; // sPrice.compareTo(new BigDecimal(25l))  == -1 ? new BigDecimal(5l) : BigDecimal.ZERO;
        //如果包含礼包 运费为 0
        if(isPackage) {
            shopCartsVO.freePrice = 0d;
        }
        // 总支付金额  订单金额 + 运费
        shopCartsVO.allPrice = Double.valueOf(df.format(sPrice + shopCartsVO.freePrice)) ; // sPrice.add(shopCartsVO.freePrice);

        // 需要支付的金额   订单金额 － 所有优惠的金额
//        shopCartsVO.paymentedAmount = Setting.discountAmount(shopCartsVO.sumPrice , wxUser);
          shopCartsVO.paymentedAmount = shopCartsVO.allPrice;
        //优惠金额  订单金额 - 需要支付的金额
        shopCartsVO.discountPay = shopCartsVO.sumPrice - shopCartsVO.paymentedAmount ; //   shopCartsVO.sumPrice.subtract(shopCartsVO.paymentedAmount);//Setting.discountAmount(shopCartsVO.sumPrice , wxUser);

        shopCartsVO.shopCartsVOList = shopCartVOArrayList;
        return shopCartsVO;
    }

    /**
     * 转换shopCarts 到 ShopCartsVO 用于页面展示2
     * @param shopCarts
     * @return
     */
    public static ShopCartsVO createOrUpdateShopCartVO2(List<ShopCart> shopCarts) {
        List<ShopCartVO> shopCartVOArrayList = new ArrayList<>();
        ShopCartsVO shopCartsVO = new ShopCartsVO();
        Double sPrice = 0d; // 总价
        for(ShopCart shopCart : shopCarts) {
            ShopCartVO vo = new ShopCartVO();
            vo.name = shopCart.goodsName;
            vo.id = shopCart.id;
            vo.count = shopCart.buyNumber;
            vo.imgUrl = ObjectUtil.checkBlock(shopCart.goods.img) ? "/public/sui/img/p3.jpg" : shopCart.goods.img  ;
            vo.price = shopCart.salePrice;
            vo.productId = shopCart.goods.serialId;
            vo.amount = Double.valueOf(df.format(vo.price * vo.count));
            shopCartVOArrayList.add(vo);
            sPrice = sPrice + (vo.price * vo.count);
        }

        Logger.info("sPrice : %s" , sPrice);

        // 订单总金额  商品金额综合
        shopCartsVO.sumPrice = sPrice == null ? 0d : sPrice;

        shopCartsVO.sumPrice = Double.valueOf(df.format(shopCartsVO.sumPrice)) ;
        // 总支付金额
        shopCartsVO.allPrice = shopCartsVO.sumPrice;

        shopCartsVO.shopCartsVOList = shopCartVOArrayList;
        return shopCartsVO;
    }

    /**
     * 转换shopCarts 到 ShopCartsVO 用于页面展示
     * @param shopCarts
     * @return
     */
    public static ShopCartsVO createOrUpdateShopCartVO(List<ShopCart> shopCarts , WebUser webUser) {
        List<ShopCartVO> shopCartVOArrayList = new ArrayList<>();
        ShopCartsVO shopCartsVO = new ShopCartsVO();
        shopCartsVO.distributionDay = "明天";
        double merchantFreight =  0.00;
        Double sPrice = 0d; // 总价
        Boolean isPackage = false;
        for(ShopCart shopCart : shopCarts) {
            ShopCartVO vo = new ShopCartVO();
            vo.name = shopCart.goodsName;
            if(!isPackage && vo.name.indexOf("礼包") > 0) {
                isPackage = true;
            }
            vo.id = shopCart.id;
            vo.count = shopCart.buyNumber;
            vo.imgUrl = ObjectUtil.checkBlock(shopCart.goods.img) ? "/public/sui/img/p3.jpg" : shopCart.goods.img  ;
            vo.price = shopCart.salePrice;
            vo.productId = shopCart.goods.serialId;
            //vo.maxCount = shopCart.goods.maxNum;
            //vo.standard = HtmlExtension.truncate(shopCart.goods.spec , 12);
            vo.amount = Double.valueOf(df.format(vo.price * vo.count));
            shopCartVOArrayList.add(vo);
            sPrice = sPrice + (vo.price * vo.count); // sPrice.add(vo.price.multiply(BigDecimal.valueOf(vo.count)));
        }

        Logger.info("sPrice : %s" , sPrice);

        // 订单总金额  商品金额综合
        shopCartsVO.sumPrice = sPrice == null ? 0d : sPrice;

        shopCartsVO.sumPrice = Double.valueOf(df.format(shopCartsVO.sumPrice)) ;
        // 如果小于 25 5元运费. 大于25 运费为0
        shopCartsVO.freePrice = sPrice < merchantFreight ? 5d : 0d; // sPrice.compareTo(new BigDecimal(25l))  == -1 ? new BigDecimal(5l) : BigDecimal.ZERO;
        //如果包含礼包 运费为 0
        if(isPackage) {
            shopCartsVO.freePrice = 0d;
        }
        // 总支付金额  订单金额 + 运费
        shopCartsVO.allPrice = Double.valueOf(df.format(sPrice + shopCartsVO.freePrice)) ; // sPrice.add(shopCartsVO.freePrice);

        // 需要支付的金额   订单金额 － 所有优惠的金额
//        shopCartsVO.paymentedAmount = Setting.discountAmount(shopCartsVO.sumPrice , wxUser);
        //计算优惠券优惠的金额
//        shopCartsVO.discountPay = Double.valueOf(df.format(DiscountCoupon.disCountpayAmount(shopCartsVO.allPrice , webUser , merchant)));
        shopCartsVO.discountPay = Double.valueOf(df.format(OrderPayment.estimateDiscountPay(webUser , shopCartsVO.allPrice )));
        Logger.info("折扣金额为 : %s " , shopCartsVO.discountPay);
        //支付金额  订单金额 - 优惠金额
        shopCartsVO.paymentedAmount = Double.valueOf(df.format(shopCartsVO.sumPrice - shopCartsVO.discountPay)) ; //   shopCartsVO.sumPrice.subtract(shopCartsVO.paymentedAmount);//Setting.discountAmount(shopCartsVO.sumPrice , wxUser);

        shopCartsVO.shopCartsVOList = shopCartVOArrayList;
        return shopCartsVO;
    }


    /**
     * 转换shopCarts 到 ShopCartsVO 用于页面展示
     * @param shopCarts
     * @return
     */
    public static ShopCartsVO createOrUpdateShopCartVO(List<ShopCart> shopCarts , Merchant merchant) {
        List<ShopCartVO> shopCartVOArrayList = new ArrayList<>();
        ShopCartsVO shopCartsVO = new ShopCartsVO();
        shopCartsVO.distributionDay = "明天";
        double merchantFreight = 0.00 ;
        Double sPrice = 0d; // 总价
        Boolean isPackage = false;
        for(ShopCart shopCart : shopCarts) {
            ShopCartVO vo = new ShopCartVO();
            Product product = Product.findByProductId(shopCart.goods.serialId);
            vo.name = shopCart.goodsName;
//            if(!isPackage && vo.name.indexOf("礼包") > 0) {
//                isPackage = true;
//            }
            vo.id = shopCart.id;
            vo.count = shopCart.buyNumber;
            vo.imgUrl = ObjectUtil.checkBlock(shopCart.goods.img) ? "/public/sui/img/p3.jpg" : shopCart.goods.img  ;
            vo.price = shopCart.salePrice;
            vo.productId = shopCart.goods.serialId;
//            vo.producingArea = product.producingArea;
            //vo.maxCount = shopCart.goods.maxNum;
            //vo.standard = HtmlExtension.truncate(shopCart.goods.spec , 12);
            vo.maxCount = ConvertUtil.toInteger(product.stock!=null?product.stock : 0);
            String spec = "";
            if(StringUtils.isBlank(shopCart.goods.spec)){
                spec = product.spec != null ? product.spec : "  ";
            }else {
                spec = shopCart.goods.spec ;
            }
            vo.standard = spec;
            vo.amount = Double.valueOf(df.format(vo.price * vo.count));
            shopCartVOArrayList.add(vo);
            sPrice = sPrice + (vo.price * vo.count) ; // sPrice.add(vo.price.multiply(BigDecimal.valueOf(vo.count)));
        }

        Logger.info("sPrice : %s" , sPrice);

        // 订单总金额  商品金额综合
        shopCartsVO.sumPrice = sPrice == null ? 0d : sPrice;

        shopCartsVO.sumPrice = Double.valueOf(df.format(shopCartsVO.sumPrice)) ;
        // 如果小于 25 5元运费. 大于25 运费为0
        shopCartsVO.freePrice = sPrice < merchantFreight ? 5d : 0d; // sPrice.compareTo(new BigDecimal(25l))  == -1 ? new BigDecimal(5l) : BigDecimal.ZERO;
        //如果包含礼包 运费为 0
//        if(isPackage) {
//            shopCartsVO.freePrice = 0d;
//        }
        // 总支付金额  订单金额 + 运费
        shopCartsVO.allPrice = Double.valueOf(df.format(sPrice + shopCartsVO.freePrice)) ; // sPrice.add(shopCartsVO.freePrice);

        Logger.info("订单金额: %s | 免运费金额 : %s | 运费 : %s | 支付金额 : %s" , sPrice , merchant.freeFreightAmount, shopCartsVO.freePrice , shopCartsVO.allPrice);
        // 需要支付的金额   订单金额 － 所有优惠的金额
//        shopCartsVO.paymentedAmount = Setting.discountAmount(shopCartsVO.sumPrice , wxUser);
        shopCartsVO.paymentedAmount = shopCartsVO.allPrice;
        //优惠金额  订单金额 - 需要支付的金额
        shopCartsVO.discountPay = Double.valueOf(df.format(shopCartsVO.sumPrice - shopCartsVO.paymentedAmount)) ; //   shopCartsVO.sumPrice.subtract(shopCartsVO.paymentedAmount);//Setting.discountAmount(shopCartsVO.sumPrice , wxUser);

        shopCartsVO.shopCartsVOList = shopCartVOArrayList;
        return shopCartsVO;
    }
}
