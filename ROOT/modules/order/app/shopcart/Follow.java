package shopcart;

import goods.Goods;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import models.weixin.WebUser;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.Model;
import product.merchant.ProductSpecMerchant;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 我的关注
 * Created by shancheng on 16-6-27.
 */
@Entity
@Table(name = "follows")
public class Follow extends Model {

    /**
     * 前端用户
     */
    @JoinColumn(name = "web_user_id", nullable = true)
    @ManyToOne
    public WebUser webUser;

    /**
     * 商户信息 方便查询
     */
    @JoinColumn(name = "merchant_id", nullable = true)
    @ManyToOne
    public Merchant merchant;

    /**
     * 商品信息
     */
    @JoinColumn(name = "goods_id", nullable = true)
    @ManyToOne
    public Goods goods;


    /**
     * 商品名称
     */
    @Column(name = "goods_name")
    public String goodsName;

    /**
     * 购买数量
     */
    @Column(name = "buy_number")
    public Integer buyNumber;


    /**
     * 销售单价
     */
    @Column(name = "sale_price")
    public Double salePrice;


    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 购物篮创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;


    /**
     * 产品 加入购物车
     * @param webUser
     * @param productSpecMerchant
     * @param count
     */
    public static void addFollow(WebUser webUser , ProductSpecMerchant productSpecMerchant, Integer count) {
        Goods goods = productSpecMerchant.findGoods(webUser);
        if (goods != null) {
            Follow follow = Follow.find("goods = ? and deleted = ? and webUser = ?", goods, DeletedStatus.UN_DELETED,webUser).first();
            if (follow == null) {
                follow = new Follow();
                follow.goods = goods;
                follow.merchant = productSpecMerchant.merchant;
                follow.goodsName = goods.name;
                follow.buyNumber = count;
                follow.webUser = webUser;
                follow.deleted = DeletedStatus.UN_DELETED;
                follow.salePrice = goods.salePrice;
                follow.createdAt = new Date();
                follow.save();
            } else {
                follow.buyNumber = count;
                follow.save();
            }
        }else {
            Logger.info("收藏时找不到Goods");
        }
    }

    public static Follow findByWebUserAndProduct(WebUser webUser , ProductSpecMerchant productSpecMerchant){
        Goods goods = productSpecMerchant.findGoods(webUser);
        return Follow.find(" webUser = ? and goods = ? and deleted = ? ",webUser,goods,DeletedStatus.UN_DELETED).first();
    }

    public static List<HashMap<String,Object>> findFollowByWebUserAndMerchant(WebUser webUser , Merchant merchant){
        Query query = em().createNativeQuery("SELECT g.name as name ,g.spec as spec,g.main_image_url AS mainImage ,g.sale_price AS merchantPrice,g.serial_id as id from follows f LEFT JOIN goods g on f.goods_id = g.id where f.web_user_id = "+webUser.id+" and merchant_id = "+ merchant.id+ " and f.deleted = 0");
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        return query.getResultList();
//        return  Follow.find(" webUser = ? and deleted = ? ",webUser,DeletedStatus.UN_DELETED).fetch();
    }
    public static Boolean isFollow(WebUser webUser , ProductSpecMerchant productSpecMerchant){
        Goods goods = productSpecMerchant.findGoods(webUser);
        Follow follow = Follow.find(" webUser = ? and goods = ? and deleted = ? ",webUser,goods,DeletedStatus.UN_DELETED).first();
        if(follow == null ){
            return false;
        }
        return true;
    }


    /**
     * 根据商品ID删除收藏
     * @param specMerchantIds
     */
    public static void deleteBySpecmerchants(String specMerchantIds){
        String updateSql = " update  follows a set a.deleted = 1 where a.goods_id in (select a.id from goods a where a.serial_id in ("+specMerchantIds+"))" ;
        JPA.em().createNativeQuery(updateSql).executeUpdate();

    }
}
