package shopcart;


/**
 * Created by LiuBin on 2016/7/14.
 */
public class ShopCartVO {
    public Long id;

    public String name;

    public Double price;

    public String standard;//规格

    public Integer count;

    public String imgUrl;

    public Integer maxCount;

    public Long productId;

    public Double amount;

    public String producingArea;
}
