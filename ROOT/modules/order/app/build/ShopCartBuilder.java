package build;

import enums.ShopCartStatus;
import goods.Goods;
import models.constants.DeletedStatus;
import models.weixin.WebUser;
import shopcart.ShopCart;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 购物车Builder.
 */
public class ShopCartBuilder {

    public ShopCart shopCart;
    List<String> errorMessages;


    /**
     * 无参数构建，生成全新的订单，设置初始值.
     */
    private ShopCartBuilder() {
        shopCart = new ShopCart();
        shopCart.buyNumber = 0;
        shopCart.salePrice = 0d;
        shopCart.createdAt = new Date();
        shopCart.deleted = DeletedStatus.UN_DELETED;
//        shopCart.facePrice = BigDecimal.ZERO;
//        shopCart.originalPrice = BigDecimal.ZERO;
        shopCart.save();
        initErrorMessage(); //建立空错误信息.
    }



    private void initErrorMessage() {
        errorMessages = new ArrayList<>();
    }


    /**
     * 开始新订单的创建.
     *
     * @return OrderBuilder构建器
     */
    public static ShopCartBuilder forBuild() {
        return new ShopCartBuilder();
    }

    private ShopCartBuilder(Long id , Long userId ,Long districtId) {
//        shopCart = ShopCart.findByProductIdAndUserAndDistrict(id , userId ,districtId);
    }

    public static ShopCartBuilder findByShopCartAndWxUserAndDistrict(Long id , Long userId , Long districtId) {
        return new ShopCartBuilder(id , userId ,districtId);
    }



    public ShopCartBuilder originalPrice(Double originalPrice) {
//        this.shopCart.originalPrice = originalPrice;
        return this;
    }

    public ShopCartBuilder salePrice(Double salePrice) {
        this.shopCart.salePrice = salePrice;
        return this;
    }

//    public ShopCartBuilder product(ProductSpecMerchant product) {
//       return goods(product.findOrCreateGoods());
//    }
//

    public ShopCartBuilder user(WebUser user) {
        this.shopCart.webUser = user;
        return this;
    }

    public ShopCartBuilder goods(Goods goods) {
        this.shopCart.goods = goods;
        //this.shopCart.goodsName = goods.name;
        return this;
    }

    public ShopCartBuilder goodsName(String goodsName){
        this.shopCart.goodsName = goodsName;
        return this;
    }

    public ShopCartBuilder status(ShopCartStatus status){
        this.shopCart.status = status;
        return this;
    }

    public ShopCartBuilder deleted(DeletedStatus deleted){
        this.shopCart.deleted = deleted;
        return this;
    }

   /* public ShopCartBuilder merchant(Merchant merchant) {
        this.shopCart.merchant = merchant;
        return this;
    }*/


    public ShopCartBuilder buyNumber(Integer number) {
        this.shopCart.buyNumber = number;
        return this;
    }

    public ShopCartBuilder changeNumber(Integer number) {
        this.shopCart.buyNumber = this.shopCart.buyNumber + number;
        if(this.shopCart.buyNumber <= 0) {
            deleted();
        }
        return this;
    }

    public ShopCartBuilder changeToNumber(Integer number) {
        this.shopCart.buyNumber = number;
        if(this.shopCart.buyNumber <= 0) {
            deleted();
        }
        return this;
    }

    public ShopCartBuilder deleted() {
        this.shopCart.deleted = DeletedStatus.DELETED;
        return this;
    }

    /**
     *  进行后继的生成.
     */
    public void build() {
        this.shopCart.save();
    }
}
