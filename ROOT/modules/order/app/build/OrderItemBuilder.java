package build;

import enums.OrderItemType;
import goods.Goods;
import models.constants.DeletedStatus;
import order.OrderItem;
import product.Product;
import product.merchant.ProductSpecMerchant;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 订单条目Builder.
 */
public class OrderItemBuilder {

    OrderBuilder orderBuilder;

    OrderItem orderItem;

    Goods goods;



    List<String> errorMessages;


    public OrderItemBuilder(OrderBuilder orderBuilder, ProductSpecMerchant product) {
        this(orderBuilder, product.findGoods(orderBuilder.getOrder().webUser));
    }


    public OrderItemBuilder(OrderBuilder orderBuilder, Product product) {
        this(orderBuilder, product.findGoods());
    }



    public OrderItemBuilder(OrderBuilder orderBuilder, Goods goods) {
        this.orderBuilder = orderBuilder;
        this.orderItem = new OrderItem();
        this.orderItem.order = orderBuilder.order;
        this.goods = goods;
        this.orderItem.goods = this.goods;
        this.orderItem.createdAt = new Date();
        //TODO 价格需要重新定义
//        this.orderItem.facePrice = goods.salePrice;
        this.orderItem.deleted = DeletedStatus.UN_DELETED;
        this.orderItem.type = OrderItemType.BUY;

        if (this.orderBuilder.order.orderItems == null) {
            this.orderBuilder.order.orderItems = new ArrayList<>();
        }
        this.orderBuilder.order.orderItems.add(this.orderItem);
    }



    public OrderItemBuilder originalPrice(Double originalPrice) {
        this.orderItem.originalPrice = originalPrice;
        return this;
    }

    public OrderItemBuilder amount(Double amount) {
        this.orderItem.amount = amount;
        return this;
    }

    public OrderItemBuilder salePrice(Double salePrice) {
        this.orderItem.salePrice = salePrice;
        return this;
    }

    public OrderItemBuilder partnerPrice(Double partnerPrice) {
        this.orderItem.partnerPrice = partnerPrice;
        return this;
    }



    public OrderItemBuilder buyNumber(Integer number) {
        this.orderItem.buyNumber = number;
        return this;
    }

    public OrderItemBuilder productName(String goodsName){
//        this.orderItem.goodsName=goodsName;
        return  this;
    }


    /**
     * 完成OrderItem的生成，并返回对应的OrderBuilder，以进行后继的生成.
     */
    public OrderBuilder build() {
        this.orderItem.save();
        this.orderBuilder.recalcOrderItem();
        this.orderBuilder.orderItem = this.orderItem;
        this.orderBuilder.order.save();
        return orderBuilder;
    }
}
