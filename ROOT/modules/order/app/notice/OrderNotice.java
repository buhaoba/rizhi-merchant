package notice;

import enums.OrderStatus;
import models.constants.DeletedStatus;
import models.weixin.WebUser;
import order.Order;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 订单通知表
 * Created by buhaoba on 2017/5/11.
 */
@Table(name = "order_notices")
@Entity
public class OrderNotice extends Model {


    /**
     * 关联的订单
     */
    @JoinColumn(name = "order_id", nullable = true)
    @ManyToOne
    public Order order;

    /**
     * 前端用户
     */
    @JoinColumn(name = "web_user_id", nullable = true)
    @ManyToOne
    public WebUser webUser;


    /**
     * 标题
     */
    public String title;

    /**
     * 是否阅读
     */
    @Column(name = "is_read")
    public Boolean isRead;


    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 订单创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;



    public OrderNotice() {
        super();
    }

    public OrderNotice(Order order , WebUser webUser , String title ) {
        this.order = order;
        this.webUser = webUser;
        this.title = title;
        this.isRead = Boolean.FALSE;
        this.deleted = DeletedStatus.UN_DELETED;
        this.createdAt = new Date();
    }


    public static List<OrderNotice> findAnReadByWebUser(WebUser webUser) {
        return OrderNotice.find("webUser = ? and isRead = ? and order.status = ? and deleted = ? order by id desc" , webUser , Boolean.FALSE , OrderStatus.UNPAID , DeletedStatus.UN_DELETED).fetch();
    }

}
