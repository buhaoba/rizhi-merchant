package bill;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.constants.SuccessStatus;
import models.weixin.WebUser;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import javax.persistence.*;
import java.util.Date;

/**
 * 用户提现表
 */
@Entity
@Table(name = "withdrawals_user_bills")
public class WithdrawalsUserBill extends BaseModel {

    /**
     * 提现编号
     */
    @Column(name = "bill_code")
   public String billCode;

    /**
     * 关联商户
     */
    @JoinColumn(name = "user_id")
    @ManyToOne
    public WebUser webUser;


    /**
     * 金额
     */
    @Column(name = "amount")
    public Integer amount;


    /**
     * 是否成功,0:成功，1:失败
     */
    @Enumerated(EnumType.ORDINAL)
    public SuccessStatus successStatus;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 备注信息
     */
    @Column(name = "remark")
    public String remark;


    public WithdrawalsUserBill() {
        super();
    }

    public WithdrawalsUserBill(WebUser webUser , Integer amount) {
        this.billCode = DateFormatUtils.format(new Date(), "yyMMdd") + RandomStringUtils.randomNumeric(9);
        this.webUser = webUser;
        this.amount = amount;
        this.successStatus = SuccessStatus.WAIT;
        this.createAt = new Date();
        this.deleted = DeletedStatus.UN_DELETED;
    }

    /**
     * 根据编号查询交易记录
     * @param orderNumber
     * @return
     */
    public static WithdrawalsUserBill findByOrderNumber(String orderNumber) {
        return WithdrawalsUserBill.find("deleted = ? and billCode = ?" , DeletedStatus.UN_DELETED , orderNumber).first();
    }







}
