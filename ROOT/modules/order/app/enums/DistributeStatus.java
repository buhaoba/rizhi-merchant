package enums;

/**
 * 订单派发状态
 */
public enum DistributeStatus {
    UN_DISTRIBUTE, // 未接单

    DISTRIBUTE,   // 已派单

    ROB_DISTRIBUTE,  // 已接单

    SEND_DISTRIBUTE,  // 已送达

    USER_GET;  // 已送达

    public static String getdistributeStatus (DistributeStatus distributeStatus){
        String distributeStatusStr = "";
        switch (distributeStatus){
            case UN_DISTRIBUTE: distributeStatusStr = "未接单"; break;
            case DISTRIBUTE: distributeStatusStr = "已派单"; break;
            case ROB_DISTRIBUTE: distributeStatusStr = "配送中"; break;
            case SEND_DISTRIBUTE: distributeStatusStr = "已送达"; break;
            case USER_GET: distributeStatusStr = "已送达"; break;
            default: distributeStatusStr = ""; break;
        }
        return  distributeStatusStr ;
    }

}
