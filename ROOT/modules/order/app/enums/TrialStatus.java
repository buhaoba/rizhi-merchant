package enums;

/**
 * 投诉枚举类.
 * <p/>
 * User: sujie
 * Date: 2/27/12
 * Time: 9:50 AM
 */
public enum TrialStatus {
    UN_TRIAL(0), TRIALED(1);

    private int value;

    TrialStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
