package enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 订单来源
 */
public enum OrderType {
    PC,// PC网站
    PHONE, //手机端
    WEIXIN, // 微信
    WEBSITE_CARD, //网上会员卡预订
    DISABLED, // 屏蔽
    SOLD, //出售
    WEIXIN_SALE, // 微信公众号售出
    INSIDE_SALES,//线下销售
    QR_CODE; // 二维码扫码购买

    public static OrderType findByName(String name) {
        if (name.toUpperCase().equals(OrderType.PC.toString())) {
            return OrderType.PC;
        } else if (name.toUpperCase().equals(OrderType.PHONE.toString())) {
            return OrderType.PHONE;
        } else if (name.toUpperCase().equals(OrderType.WEIXIN.toString())) {
            return OrderType.WEIXIN;
        } else if (name.toUpperCase().equals(OrderType.DISABLED.toString())) {
            return OrderType.DISABLED;
        } else if (name.toUpperCase().equals(OrderType.SOLD.toString())) {
            return OrderType.SOLD;
        } else if (name.toUpperCase().equals(OrderType.WEBSITE_CARD.toString())) {
            return OrderType.WEBSITE_CARD;
        } else if (name.toUpperCase().equals(OrderType.WEIXIN_SALE.toString())) {
            return OrderType.WEIXIN_SALE;
        } else if (name.toUpperCase().equals(OrderType.INSIDE_SALES.toString())) {
            return OrderType.INSIDE_SALES;
        } else if (name.toUpperCase().equals(OrderType.QR_CODE.toString())) {
            return OrderType.QR_CODE;
        } else {
            return null;
        }
    }

    public static String findPayName(OrderType type) {
        if (type == null) return "";
        Map msg = new HashMap();
        msg.put(OrderType.PC.toString(), "PC网站");
        msg.put(OrderType.PHONE.toString(), "手机端");
        msg.put(OrderType.WEIXIN.toString(), "微信");
        msg.put(OrderType.WEBSITE_CARD.toString(), " = 网上会员卡预订");
        msg.put(OrderType.DISABLED.toString(), "屏蔽");
        msg.put(OrderType.SOLD.toString(), " 出售");
        msg.put(OrderType.WEIXIN_SALE.toString(), "微信公众号售出");
        msg.put(OrderType.INSIDE_SALES.toString(), "线下销售");
        msg.put(OrderType.QR_CODE.toString(), "二维码扫码购买");
        return msg.get(type.toString()).toString();
    }
}
