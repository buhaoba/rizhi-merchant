package enums;

/**
 * Created by shancheng on 16-6-21.
 */
public enum GoodsType {

    PRODUCT, // 产品  目前只有一种 水吧
    COACH, // 教练
    CURRICULUM, // 课程 健身
    FIELD,  // 场地  场地分多钟   健身场地 搏击场地等等 根据 FieldType 区分
    SPORTS_ROOMS, //竞技包房 电竞
    RECHARGE ,// 会员卡充值
    THIRDPARTY, // 第三方订单
    NO_PRODUCT ;//没有产品的水吧商品/订单


    public static String getGoodsType (GoodsType goodsType){
        String goodsTypeStr = "";
        switch (goodsType){
            case PRODUCT: goodsTypeStr = "水吧商品"; break;
            case COACH: goodsTypeStr = "教练"; break;
            case CURRICULUM: goodsTypeStr = "健身课程"; break;
            case FIELD: goodsTypeStr = "场地"; break;
            case SPORTS_ROOMS: goodsTypeStr = "竞技包房"; break;
            case RECHARGE: goodsTypeStr = "会员卡充值"; break;
            case THIRDPARTY: goodsTypeStr = "第三方订单"; break;
            case NO_PRODUCT: goodsTypeStr = "水吧订单"; break;
            default: goodsTypeStr = ""; break;
        }
        return  goodsTypeStr ;
    }



}
