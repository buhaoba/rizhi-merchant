package enums;

/**
 * Created by liming on 16/7/18.
 */
public enum OrderItemStatus {
    //正常交易
    NORMAL,
    //已拆单
    SPLIT,
    //已退款
    SALES_RETURN
}
