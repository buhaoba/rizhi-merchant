package enums;

/**
 * Created by upshan on 2016/06/06
 */
public enum OrderItemType {

    BUY, //购买
    LUCK, //抽奖
    GIVE,//赠送
    VOTE//投票
}
