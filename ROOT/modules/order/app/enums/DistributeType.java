package enums;

/**
 * Created by Administrator on 2017/10/9.
 */
public enum DistributeType {
    OWN, // 商家自送

    OTHERS;   // 派送



    public static String getdistributeType (DistributeType distributeType){
        String distributeTypeStr = "";
        switch (distributeType){
            case OWN: distributeTypeStr = "自送"; break;
            case OTHERS: distributeTypeStr = "派送"; break;
            default: distributeTypeStr = ""; break;
        }
        return  distributeTypeStr ;
    }
}
