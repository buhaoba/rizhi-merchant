package enums;

/**
 * Created by buhaoba on 16/7/15.
 */
public enum  PaymentType {

    WEIXIN, //微信原生支付
    WEIXIN_PUB, //微信公众号支付
    WEIXIN_PUB_QR, // 微信扫码支付
    ECOUPON, //优惠券
    INTEGRAL, //积分
    ECASH, //代金券
    ALI_PC_DIRECT, //支付宝
    CASH, //现金
    ALI_QR, //支付宝
    DEDUCTIBLE_COUPON  //抵扣券抵扣


}
