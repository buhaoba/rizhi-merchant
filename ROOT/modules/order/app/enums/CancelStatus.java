package enums;

/**
 * Created by Administrator on 2018/1/12.
 * 订单明细取消状态
 */
public enum CancelStatus {
    UN_TRIAL(0), CANCEL(1);

    private int value;

    CancelStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
