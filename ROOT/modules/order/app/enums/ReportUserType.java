package enums;

/**
 * 标记删除的枚举类型.
 * <p/>
 * User: sujie
 * Date: 2/27/12
 * Time: 9:50 AM
 */
public enum ReportUserType {
    MERCHANT, //商户
    USER    // 用户
}
