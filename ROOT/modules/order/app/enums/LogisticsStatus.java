package enums;

/**
 * Created by liming on 16/7/18.
 * 物流状态
 */
public enum LogisticsStatus {
    //备货中
    PREPARING,
    //已配货
    PACKAGE,
    //未送达退回
    RETURN,
    //物流扫描
    SCAN,
    //已发货
    GO,
    //已送达
    ARRIVE

}
