package enums;

/**
 * Created by Administrator on 2017/9/22.
 */
public enum RefundStatus {
    SELLER_AGREES,   //卖家同意退款
    ADMIN_AGREES    //管理员同意退款
}
