package enums;

/**
 * 场馆状态
 */
public enum ShopCartStatus {
    NORMAL,//正常
    DELETED,//删除
    BUY //购买
}
