package coupon;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.weixin.WebUser;
import util.common.DateUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by buhaoba on 2016/10/19.
 */
@Entity
@Table(name = "discount_coupon_detail")
public class DiscountCouponDetail extends BaseModel {


    /**
     * 用户信息
     */
    @JoinColumn(name = "web_user_id")
    @ManyToOne
    public WebUser webUser;


    /**
     * 折扣券信息
     */
    @JoinColumn(name = "discount_coupon_id")
    @ManyToOne
    public DiscountCoupon discountCoupon;

    /**
     * 可用金额
     */
    @Column(name = "amount")
    public Double amount = 0d;

    /**
     * 有效期截止时间
     */
    @Transient
    public String endDate;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark ;



    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    public DiscountCouponDetail() {
        super();
    }

    public DiscountCouponDetail(WebUser webUser , DiscountCoupon discountCoupon , Double amount) {
        this.webUser = webUser;
        this.discountCoupon = discountCoupon;
        this.amount = amount;
        this.deleted = DeletedStatus.UN_DELETED;
        this.createAt = new Date();
        this.modifyAt = new Date();
    }

    public static DiscountCouponDetail findByWebUserAndCoupon(WebUser webUser , DiscountCoupon discountCoupon) {
        return DiscountCouponDetail.find("webUser = ?1 and discountCoupon = ?2 and deleted = ?3" , webUser , discountCoupon , DeletedStatus.UN_DELETED).first();
    }

    public static List<DiscountCouponDetail> findByWebUser(WebUser webUser) {
        Date today = new Date();
        String nowTime = DateUtil.dateToString(new Date(),"HH:mm");
//        return DiscountCouponDetail.find("webUser = ? and deleted = ?  order by discountCoupon.priority asc" , webUser , DeletedStatus.UN_DELETED ).fetch();
        return DiscountCouponDetail.find("webUser = ?1 and deleted = ?2 and discountCoupon.endAt >= ?3 and discountCoupon.endTimes > ?4  order by discountCoupon.priority asc" , webUser , DeletedStatus.UN_DELETED , today ,nowTime).fetch();
    }


    public static DiscountCouponDetail createOrUpdateDetail(WebUser webUser , DiscountCoupon discountCoupon , Double amount) {
        DiscountCouponDetail detail = findByWebUserAndCoupon(webUser , discountCoupon);
        if(detail == null) {
            new DiscountCouponDetail(webUser , discountCoupon , amount).save();
        } else {
            detail.amount += amount;
            detail.modifyAt = new Date();
            detail.save();
        }
        return detail;
    }

    public static DiscountCouponDetail findByDetailId(long detailId){
        return DiscountCouponDetail.find("id = ?1 and deleted = ?2" , detailId , DeletedStatus.UN_DELETED).first();
    }

    /**
     * 检查用户抵扣券是否存在
     * @param discountCoupon
     * @param webUser
     * @return
     */
    public static Boolean checkByCouponAndUser(DiscountCoupon discountCoupon ,WebUser webUser){
        return DiscountCouponDetail.count("discountCoupon = ?1 and webUser = ?2 and deleted = ?3" , discountCoupon ,webUser ,DeletedStatus.UN_DELETED) > 0 ;
    }

    /**
     * 检索临期卡券
     * 离到期还有14天/7天/小于等于3天
     * @return
     */

    public static List<DiscountCouponDetail> findPeriodCouponList(){
        Date today = new Date();
        Date fourteen = DateUtil.getAfterDate(DateUtil.getBeginOfDay(today) , 14);
        Date seven = DateUtil.getAfterDate(DateUtil.getBeginOfDay(today) , 7);
        Date three = DateUtil.getAfterDate(DateUtil.getBeginOfDay(today),3);

        return  DiscountCouponDetail.find(" deleted = ?1 and( discountCoupon.endAt = ?2 or discountCoupon.endAt = ?3 or (discountCoupon.endAt <= ?4 and discountCoupon.endAt > ?5 ) and amount > 0 )" , DeletedStatus.UN_DELETED , fourteen ,seven ,three,today).fetch();
    }

    /**
     * 检索到期卡券
     *
     * @return
     */

    public static List<DiscountCouponDetail> findExpireCouponList(){
        Date now = new Date();
        Date today = DateUtil.getBeginOfDay();
        String endTimes = DateUtil.dateToString(now , "HH")+":00";

        return  DiscountCouponDetail.find(" deleted = ?1 and discountCoupon.endAt = ?2 and discountCoupon.endTimes = ?3  and amount > 0 )" , DeletedStatus.UN_DELETED ,today , endTimes).fetch();
    }



}
