package coupon;

import models.BaseModel;
import models.constants.DeletedStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by buhaoba on 2016/10/19.
 */
@Entity
@Table(name = "discount_coupon_detail_history")
public class DiscountCouponDetailHistory extends BaseModel {


    /**
     * 卡券信息
     */
    @JoinColumn(name = "discount_coupon_detail_id")
    @ManyToOne
    public DiscountCouponDetail discountCouponDetail;

    /**
     * 可用金额
     */
    @Column(name = "amount")
    public Double amount = 0d;

    /**
     * 备注信息
     */
    @Column(name = "remark")
    public String remark;

    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    public DiscountCouponDetailHistory() {
        super();
    }

    public DiscountCouponDetailHistory(DiscountCouponDetail discountCouponDetail , Double amount , String remark) {
        this.discountCouponDetail = discountCouponDetail;
        this.amount = amount;
        this.remark = remark;
        this.deleted = DeletedStatus.UN_DELETED;
        this.createAt = new Date();
        this.modifyAt = new Date();
    }

    public static List<DiscountCouponDetailHistory> findByDetailId(long detailId){
        return DiscountCouponDetailHistory.find("discountCouponDetail.id = ? and deleted = ? order by createAt desc " , detailId , DeletedStatus.UN_DELETED).fetch();
    }

}
