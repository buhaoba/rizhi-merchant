package coupon;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import product.merchant.ProductSpecMerchant;
import util.common.UStringUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 抵扣券可抵扣商品
 * Created by Yangshancheng
 * on 2017/1/6.
 * Time : 下午12:31
 */
@Entity
@Table(name = "deductible_coupon_type_spec_products")
public class DeductibleCouponSpecProduct extends BaseModel {

    /**
     * 抵扣券类型信息
     */
    @JoinColumn(name = "deductible_coupon_type_id")
    @ManyToOne
    public DeductibleCouponType deductibleCouponType;


    /**
     * 可抵扣商品
     */
    @JoinColumn(name = "product_spec_merchant")
    @ManyToOne
    public ProductSpecMerchant productSpecMerchant;


    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;
    //加载抵扣券类型的商品
    public  static List<DeductibleCouponSpecProduct> loadDeCouponSpecPro(long id){
        return DeductibleCouponSpecProduct.find("deductibleCouponType.id=? and deductibleCouponType.deleted=? and deleted=?",id,DeletedStatus.UN_DELETED,DeletedStatus.UN_DELETED).fetch();

    }
    /**
     * 删除Item
     */
    public static void deleteItem(Integer[] delItemIdArray) {
//        String delIds = UStringUtil.concatStr(",", delItemIdArray);
//        CustomActivityItem.delete("id in (" + delIds + ")");
        String sql = "update deductible_coupon_type_spec_products set deleted = ? where id in (" + UStringUtil.concatStr(",", delItemIdArray) + ")";
        em().createNativeQuery(sql).setParameter(1, DeletedStatus.DELETED.getValue()).executeUpdate();
    }

}
