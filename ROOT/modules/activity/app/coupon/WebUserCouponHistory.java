package coupon;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.weixin.WebUser;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by buhaoba on 2016/10/20.
 */
@Entity
@Table(name = "webuser_coupon_historys")
public class WebUserCouponHistory extends BaseModel {

    /**
     *  用户代金券
     */
    @JoinColumn(name = "web_user_coupon_id")
    @ManyToOne
    public WebUserCoupon webUserCoupon;

    /**
     * 使用描述
     */
    public String content;


    /**
     * 变动金额
     */
    public Double amount;

    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;


    public WebUserCouponHistory() {
        super();
    }

    public WebUserCouponHistory(WebUserCoupon webUserCoupon, String content , Double amount) {
        this.webUserCoupon = webUserCoupon;
        this.content = content;
        this.amount = amount;
        this.deleted = DeletedStatus.UN_DELETED;
        this.createAt = new Date();
    }


    public static WebUserCouponHistory findByWebUser(WebUser webUser) {
        return DiscountCoupon.find("webUser = ? and deleted = ?" , webUser , DeletedStatus.UN_DELETED).first();
    }
}
