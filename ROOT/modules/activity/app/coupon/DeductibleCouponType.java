package coupon;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import order.Order;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 抵扣券
 * Created by Yangshancheng
 * on 2017/1/6.
 * Time : 下午12:31
 */
@Entity
@Table(name = "deductible_coupon_types")
public class DeductibleCouponType extends BaseModel {

    /**
     * 商户信息
     */
    @JoinColumn(name = "merchant_id")
    @ManyToOne
    public Merchant merchant;

    /**
     * 活动标题
     */
    @Column(name = "title")
    public String title;

    /**
     * 开始日期
     */
    @Column(name = "begin_at")
    public Date beginAt;

    /**
     * 开始时间
     */
    @Column(name = "begin_times")
    public String beginTimes;

    /**
     * 结束日期
     */
    @Column(name = "end_at")
    public Date endAt;


    /**
     * 结束时间
     */
    @Column(name = "end_times")
    public String endTimes;


    /**
     * 赠送金额
     */
    @Column(name = "amount")
    public Double amount;


    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;
    //根据id查询出抵扣券
    public  static DeductibleCouponType findByDeductibleCouponTypeId(long id){
        return DeductibleCouponType.find("id =? and deleted=?",id ,DeletedStatus.UN_DELETED).first();

    }

    public static List<DeductibleCouponType>  loadAllCouponTypeList(){
        return DeductibleCouponType.find("deleted = ? order by id desc" , DeletedStatus.UN_DELETED).fetch();
    }
}
