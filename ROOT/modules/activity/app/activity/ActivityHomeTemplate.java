package activity;

import enums.TempType;
import helper.CacheCallBack;
import helper.CacheHelper;
import models.BaseModel;
import models.constants.AvailableStatus;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import util.common.UStringUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by liming on 16/7/7.
 */

@Entity
@Table(name = "activity_home_template")
public class ActivityHomeTemplate extends BaseModel {

    public static final String CACHE_ACTIVITY_HOME_MERCHANTID = "ULC_CACHE_ACTIVITY_HOME_MERCHANTID_";
    public static final String CACHE_ACTIVITY_HOME_TEMPLATE_ID = "ULC_CACHE_ACTIVITY_HOME_TEMPLATE_ID_";

    @Column(name = "name")
    public  String name;
    
    @ManyToOne
    @JoinColumn(name = "merchant_id")
    public Merchant merchant;



    @Column
    public long merchantId;


    /**
     * 当前状态  禁用\可用
     */
    @Enumerated(EnumType.STRING)
    public AvailableStatus status;
    /**
     * 模板类型
     */
    @Enumerated(EnumType.STRING)
    public TempType type  = TempType.ORDINARY;

    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 根据ID删除操作明细
     *
     * @param delItemArray
     */
    public static void deleteByIds(Integer[] delItemArray) {
        String delItemStr = UStringUtil.concatStr(",", delItemArray);
        List<ActivityHomeTemplate> activityHomeTemplates = ActivityHomeTemplate.find("id in (" + delItemStr + ")").fetch();
        for(ActivityHomeTemplate activityHomeTemplate :activityHomeTemplates){
            activityHomeTemplate.deleted = DeletedStatus.DELETED;
            activityHomeTemplate.save();
        }
    }
    public static List<ActivityHomeTemplate> findTemplateAvailable(Merchant merchant){
        return ActivityHomeTemplate.find(" deleted = ? and status = ? and merchant.id = ? ",DeletedStatus.UN_DELETED,AvailableStatus.AVAILABLE,merchant.id).fetch();
    }

    /**
     * 根据 merchantId 从缓存中获取  商户模板信息,
     * 如果或许不到查询数据库
     * @param merchantId
     * @return
     */
    public static ActivityHomeTemplate findByMerchantId(Long merchantId) {
        return ActivityHomeTemplate.find("deleted = ? and status = ? and merchant.id = ?" , DeletedStatus.UN_DELETED ,AvailableStatus.AVAILABLE , merchantId).first();
//        return CacheHelper.getCache(CACHE_ACTIVITY_HOME_MERCHANTID + merchantId, new CacheCallBack<ActivityHomeTemplate>() {
//            @Override
//            public ActivityHomeTemplate loadData() {
//                return ActivityHomeTemplate.find("deleted = ? and status = ? and merchant.id = ?" , DeletedStatus.UN_DELETED ,AvailableStatus.AVAILABLE , merchantId).first();
//            }
//        });
    }

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    public static ActivityHomeTemplate findByTemplateId(long id){
        return ActivityHomeTemplate.find(" id = ? and deleted = ?" , id , DeletedStatus.UN_DELETED).first();
    }


}
