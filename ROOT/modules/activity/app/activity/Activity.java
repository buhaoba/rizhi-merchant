package activity;

import activity.marketing.ActivityClassify;
import enums.ActivityType;
import enums.ShowPosition;
import helper.CacheHelper;
import models.BaseModel;
import models.constants.DeletedStatus;
import models.merchant.Merchant;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by shancheng on 16-6-27.
 */
@Entity
@Table(name = "activity")
public class Activity extends BaseModel {

    public static final String CACHE_ACTIVITY_BY_TEMPID = "ULC_CACHE_ACTIVITY_BY_TEMPID_";
    public static final String CACHE_ACTIVITY_ID = "ULC_CACHE_ACTIVITY_";
    public static final String CACHE_ACTIVITY_PRODUCT_ACTIVITYID = "ULC_CACHE_ACTIVITY_PRODUCT_ACTIVITYID_";

    /**
     * 活动标题
     */
    @Column(name = "title")
    public String title;


    /**
     * 副标题
     */
    @Column(name = "subtitle")
    public String subtitle;

    /**
     * 活动简介
     */
    @Column(name = "content")
    public String content;
//
//    @ManyToOne
//    @JoinColumn(name = "activity_home_template_id")
//    public ActivityHomeTemplate activityHomeTemplate;


    @ManyToOne
    @JoinColumn(name = "merchant_id")
    public Merchant merchant;


    /**
     * 分类活动
     */
    @ManyToOne
    @JoinColumn(name = "activity_classify_id")
    public ActivityClassify activityClassify;


    /**
     * 活动主图
     */
    @Column(name = "main_img")
    public String mainImg;


    /**
     * 活动类型
     */
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    public ActivityType type;

    /**
     * 活动类型(所在位置)
     */
    @Column(name = "show_position")
    @Enumerated(EnumType.STRING)
    public ShowPosition showPosition;

    /**
     * 排序号
     */
    @Column(name = "show_order")
    public Long showOrder;

    /**
     * 活动链接
     */
    @Column(name = "url")
    public String url;

    /**
     * 开始时间
     */
    @Column(name = "begin_at")
    @Temporal(TemporalType.TIMESTAMP)
    public Date beginAt;

    /**
     * 结束时间
     */
    @Column(name = "end_at")
    @Temporal(TemporalType.TIMESTAMP)
    public Date endAt;


    /**
     * 开始时间
     */
    @Transient
    public String beginAtStr;

    /**
     * 结束时间
     */
    @Transient
    public String endAtStr;

    /**
     * 开始时间
     */
    @Column(name = "begin_times")
    public String beginTimes;


    /**
     * 活动结束时间
     */
    @Column(name = "end_times")
    public String endTimes;


    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;


    /**
     * 标题颜色
     */
    @Column(name = "title_colour")
    public String titleColour;

    /**
     * 副标题颜色
     */
    @Column(name = "subtitle_colour")
    public String subtitleColour;


//
//    /**
//     * 购物篮创建时间
//     */
//    @Column(name = "created_at")
//    public Date createdAt;


    public static void updateCache(String key, Object value) {
        CacheHelper.objectToCache(key, value);
    }


    /**
     * 删除图片
     *
     * @param id
     */
    public static void deleteImg(long id) {
        Activity activity = Activity.findById(id);
        if (activity != null) {
            activity.mainImg = null;
            activity.save();
        }
    }

    /**
     * 加载商户所有活动
     * @param merchantId
     * @return
     */
    public static List<Activity> loadActivityByMerchant(long merchantId){
        if(merchantId == 0){
            return Activity.find("deleted = ?1 and merchant.id is null order by id asc" , DeletedStatus.UN_DELETED ).fetch();
        }else{
            return Activity.find("deleted = ?1 and merchant.id = ?2 order by id asc " , DeletedStatus.UN_DELETED , merchantId).fetch();
        }
    }


    /**
     * 根据活动 拿到活动商品
     *
     * @return
     */
    public List<ActivityProduct> getActivityProducts() {
        return ActivityProduct.find("deleted = ?1 and activity.id = ?2 order by  id ", DeletedStatus.UN_DELETED, this.id).fetch();
    }

    public static Activity findByActivityId(long id){
        return Activity.find("id = ?1 and deleted = ?2 " ,id  , DeletedStatus.UN_DELETED).first();
    }

    /**
     *首页设置获取活动,商户Id为空
     * @param showPosition
     *
     * @return
     */
    public static Activity findByPosition(ShowPosition showPosition) {
        return Activity.find(" showPosition = ?1 and deleted = ?2 and merchant is null", showPosition, DeletedStatus.UN_DELETED).first();
    }

    public static List<Activity> findByPositions(ShowPosition showPosition, long ahtId) {
        return Activity.find(" showPosition = ?1 and activityHomeTemplate.id = ?2 and deleted = ?3 order by showOrder asc", showPosition, ahtId, DeletedStatus.UN_DELETED).fetch();
    }

    public static Activity findByNum(ShowPosition showPosition, int i) {
        return (Activity) Activity.find("showPosition = ?1 and deleted = ?2 order by showOrder asc ").fetch().get(i - 1);
    }

    public static List<Activity> findByTemplateWithDataBase(long id) {
        return Activity.find(" deleted = ?1 and activityHomeTemplate.id = ?2 ", DeletedStatus.UN_DELETED, id).fetch();
    }

    /**
     *
     * @param showPosition
     *
     * @return
     */
    public static Activity findByPositionAndMer(ShowPosition showPosition,long merchantId) {
        return Activity.find(" showPosition = ?1 and deleted = ?2 and merchant.id = ?3 ", showPosition, DeletedStatus.UN_DELETED,merchantId).first();
    }


}
