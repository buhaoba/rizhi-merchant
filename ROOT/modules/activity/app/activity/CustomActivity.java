package activity;

import enums.CustomActivityStatus;
import enums.CustomActivityType;
import models.BaseModel;
import models.admin.AdminUser;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by liming on 16/7/26.
 * 自定义活动(秒杀/促销)
 */
@Entity
@Table(name = "custom_activity")
public class CustomActivity extends BaseModel {

    /**
     * 活动编号
     */
    @Column(name = "code")
    public String code;

    /**
     * 商户
     */
    @ManyToOne
    @JoinColumn(name = "merchant_id")
    public Merchant merchant;

    /**
     * 活动类型 促销/秒杀
     */
    @Column(name = "custom_activity_type")
    @Enumerated(EnumType.STRING)
    public CustomActivityType customActivityType;

    /**
     * 排序号
     */
    @Column(name = "show_order")
    public Long showOrder;


    /**
     * 活动开始时间
     */
    @Column(name = "begin_date")
    @Temporal(TemporalType.TIMESTAMP)
    public Date beginDate;

    @Transient
    public String beginDateStr;


    /**
     * 活动结束时间
     */
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    public Date endDate;

    @Transient
    public String endDateStr;

    /**
     * 开始时间
     */
    @Column(name = "begin_times")
    public String beginTime;

    /**
     * 活动结束时间
     */
    @Column(name = "end_times")
    public String endTime;

    /**
     * 是否修改过价格
     */
    @Column(name = "is_change_price")
    public Boolean isChangePrice;

    /**
     * 活动状态
     */
    @Column(name = "custom_activity_status")
    @Enumerated(EnumType.STRING)
    public CustomActivityStatus customActivityStatus;

    /**
     * 活动开启人
     */
    @ManyToOne
    @JoinColumn(name = "start_user_id")
    public AdminUser startUser;

    /**
     * 开启时间
     */
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    public Date startDate;

    @Transient
    public String startDateStr;

    /**
     * 强制终止人
     */
    @ManyToOne
    @JoinColumn(name = "abort_user_id")
    public AdminUser abortUser;

    /**
     * 强制终止时间
     */
    @Column(name = "abort_date")
    @Temporal(TemporalType.TIMESTAMP)
    public Date abortDate;
    @Transient
    public String abortDateStr;

    /**
     * 版本号
     */
    @Version
    public Integer version;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;

    /**
     * 删除状态
     */
    @Column(name = "deleted_status")
    public DeletedStatus deletedStatus;

    @Transient
    public List<HashMap<String,Object>> customActivityItems;

    /**
     * 根据id查询
     */
    public static CustomActivity findByCustomActivityId(long id) {
        return CustomActivity.find("id =? and deletedStatus=?", id, DeletedStatus.UN_DELETED).first();
    }
    /**
     * 根据商户及活动类型查询
     * @param merchant
     * @param type
     * @return
     */
    public static List<CustomActivity> findByMerchantAndType(Merchant merchant, CustomActivityType type,int pageNum){
        return CustomActivity.find("merchant = ? and customActivityType = ? and deletedStatus = ? and customActivityStatus = ? and day(end_date)-day(now()) >= 0 order by beginDate ,beginTime asc ",merchant,type,DeletedStatus.UN_DELETED,CustomActivityStatus.START).fetch(pageNum,10);
    }
    /**
     * 根据活动 拿到活动商品
     * @return
     */
    public List<CustomActivityItem> getCustomActivityItems() {
        final Long _id = this.id;
        List<CustomActivityItem> customActivityItems = CustomActivityItem.find("customActivity.id = ?" , _id).fetch();
        return  customActivityItems;
//        return CacheHelper.getCache(CustomActivityItem.CACHE_CUSTOM_ACTIVITYITEM_BY_CUSTOMACTIVITY_ID + this.id, new CacheCallBack<List<CustomActivityItem>>() {
//            @Override
//            public List<CustomActivityItem> loadData() {
//                return CustomActivityItem.find("deletedStatus = ? and customActivity.id = ?" , DeletedStatus.UN_DELETED , _id).fetch();
//            }
//        });
    }
    public static  List<HashMap<String,Object>> findByCustomActivity(long id ){
        Query query = em().createNativeQuery("SELECT m.id AS productSpecMerchant, ps.main_image AS mainImage, p.name AS name, p.selling_point AS sellingPoint, m.merchant_price AS merchantPrice , ps.spec as spec , ps.unit as unit , i.price as activityPrice , i.old_price as oldPrice FROM custom_activity_item i LEFT JOIN product_spec_merchants m ON i.product_spec_merchant_id = m.id LEFT JOIN product_specs ps ON m.product_spec_id = ps.id LEFT JOIN products p ON p.id = ps.product_id WHERE i.deleted = 0 AND i.custom_activity_id = "+id);
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        return query.getResultList();
    }
}
