package activity.give;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.merchant.Merchant;

import javax.persistence.*;

/**
 * 赠送活动
 * Created by Yangshancheng
 * on 2016/11/22.
 * Time : 下午5:30
 */
@Entity
@Table(name = "give_activitys")
public class GiveActivity extends BaseModel {

    /**
     * 活动名字
     */
    @Column(name = "name")
    public String name;

    /**
     * 商户
     */
    @ManyToOne
    @JoinColumn(name = "merchant_id")
    public Merchant merchant;

    /**
     * 订单最低金额
     */
    @Column(name = "min_amount")
    public Double minAmount;

    /**
     * 最高金额
     */
    @Column(name = "max_amount")
    public Double maxAmount;

    /**
     * 排序
     */
    @Column(name = "sort")
    public String sort;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;

    /**
     * 版本号
     */
    @Version
    public Integer version;


    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public static GiveActivity getById(long id){
        return GiveActivity.find("id = ? and deleted = ? ",id,DeletedStatus.UN_DELETED).first();
    }





}
