package activity.give;

import enums.SalesmanType;
import models.BaseModel;
import models.constants.DeletedStatus;
import models.weixin.WebUser;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Yangshancheng
 * on 2016/11/24.
 * Time : 上午10:02
 */
@Entity(name = "give_web_user_auths")
public class GiveWebUserAuth extends BaseModel {

    /**
     * 授权用户
     */
    @ManyToOne
    @JoinColumn(name = "web_user_id")
    public WebUser webUser;
    /**
     * 业务员类型
     */

    @Enumerated(EnumType.STRING)
    public SalesmanType salesmanType;
    /**
     * 业务员名字
     */
    @Column(name = "web_user_name")
    public String userName;



    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    public static GiveWebUserAuth findByWebuser(long id){
        return GiveWebUserAuth.find(" deleted =? and webUser.id=? ",DeletedStatus.UN_DELETED,id).first();

    }
    public  static GiveWebUserAuth findByGiveWebUserAuth(long id){
        return  GiveWebUserAuth.find(" deleted =? and id=?",DeletedStatus.UN_DELETED,id).first();
    }
    public  static List<GiveWebUserAuth> loadByGiveWebUserAuth(){
        return GiveWebUserAuth.find("deleted =? and salesmanType= ?" ,DeletedStatus.UN_DELETED,SalesmanType.REDPAPER).fetch();
    }

    public static List<GiveWebUserAuth> loadAllGiveWebUserAuth() {
        return GiveWebUserAuth.find("deleted =?", DeletedStatus.UN_DELETED).fetch();
    }
}
