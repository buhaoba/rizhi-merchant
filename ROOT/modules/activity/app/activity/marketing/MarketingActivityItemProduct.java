package activity.marketing;

import models.BaseModel;
import models.constants.DeletedStatus;
import play.db.jpa.JPA;
import play.db.jpa.Model;
import product.merchant.ProductSpecMerchant;
import util.common.UStringUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by buhaoba on 2016/11/8.
 */
@Entity
@Table(name = "maketint_activity_item_products")
public class MarketingActivityItemProduct extends BaseModel{

    /**
     * 商品条目信息
     */
    @ManyToOne
    @JoinColumn(name = "marketing_activity_item_id")
    public MarketingActivityItem marketingActivityItem;

    /**
     * 商品信息
     */
    @ManyToOne
    @JoinColumn(name = "product_spec_merchant_id")
    public ProductSpecMerchant productSpecMerchant;

    /**
     * 排序
     */
    @Column(name = "sort")
    public Integer sort;


    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;


    /**
     *
     * @param item
     * @return
     */
    public static List<MarketingActivityItemProduct> findByItem(MarketingActivityItem item) {
        return MarketingActivityItemProduct.find("deleted = ? and marketingActivityItem = ? order by sort" , DeletedStatus.UN_DELETED , item).fetch();
    }

    /**
     *根据活动类目删除活动商品
     * @param itemIds
     */
    public static void deleteMarketingActivityItemProducts(Integer[] itemIds){
        String ids = UStringUtil.concatStr(",",itemIds);
        String sqlUpdate = "update maketint_activity_item_products set deleted = 1 where  marketing_activity_item_id in ("+ ids+")";
        JPA.em().createNativeQuery(sqlUpdate).executeUpdate();
    }

    /**
     * 根据活动商品删除商品
     * @param itemProductIds
     */
    public static void deleteItemProductd(Integer[] itemProductIds){
        String strIds = UStringUtil.concatStr(",",itemProductIds);
        String sqlUpdate = "update maketint_activity_item_products set deleted = 1 where  id in ("+ strIds+")";
        JPA.em().createNativeQuery(sqlUpdate).executeUpdate();
    }

}
