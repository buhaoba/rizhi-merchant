package activity.marketing;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by buhaoba on 2016/11/8.
 */
@Entity
@Table(name = "maketint_activitys")
public class MarketingActivity extends BaseModel{

    /**
     * 所属商户
     */
    @ManyToOne
    @JoinColumn(name = "merchant_id")
    public Merchant merchant;

    /**
     * 活动编号
     */
    @Column(name = "code")
    public String code;


    /**
     * 活动标题
     */
    @Column(name = "title")
    public String title;

    @Lob
    @Column(name = "content")
    public String content;

    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;


    /**
     *
     * @param code
     * @return
     */
    public static MarketingActivity findByCode(String code) {
        return MarketingActivity.find("deleted = ? and code = ?" , DeletedStatus.UN_DELETED , code).first();
    }

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    public static MarketingActivity findByMarketingActivityId(long id){
        return MarketingActivity.find("id = ? and deleted = ?",id,DeletedStatus.UN_DELETED).first();
    }

    public static Boolean checkItems(long id){
        return MarketingActivityItem.count("marketingActivity.id = ? and deleted = ?",id,DeletedStatus.UN_DELETED)>0;
    }


}
