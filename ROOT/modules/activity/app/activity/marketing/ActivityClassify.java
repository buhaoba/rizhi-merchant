package activity.marketing;

import activity.ActivityHomeTemplate;
import models.BaseModel;
import models.constants.DeletedStatus;

import javax.persistence.*;
import java.util.List;

/**
 * Created by youliangcheng on 17/3/30.
 */

@Entity
@Table(name = "activity_classifies")
public class ActivityClassify extends BaseModel {

    /**
     * 类别名称
     */
    @Column(name = "name")
    public String name;


    /**
     * 所属模板
     */
    @ManyToOne
    @JoinColumn(name = "activity_home_template_id")
    public ActivityHomeTemplate activityHomeTemplate;

    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;


    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public static ActivityClassify findByClassifyId(long id){
        return  ActivityClassify.find("id = ? and deleted = ?" ,id , DeletedStatus.UN_DELETED).first();
    }

    /**
     * 根据模板查询模板下的分类
     * @param templateId
     * @return
     */
    public static List<ActivityClassify>  loadByTemplateId(long templateId){
        return ActivityClassify.find("activityHomeTemplate.id = ? and deleted = ? ", templateId , DeletedStatus.UN_DELETED).fetch();
    }


}
