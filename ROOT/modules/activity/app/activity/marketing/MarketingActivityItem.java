package activity.marketing;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import play.db.jpa.JPA;
import play.db.jpa.Model;
import util.common.UStringUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by buhaoba on 2016/11/8.
 */
@Entity
@Table(name = "maketint_activity_items")
public class MarketingActivityItem extends BaseModel{

    @ManyToOne
    @JoinColumn(name = "marketing_activity_id")
    public MarketingActivity marketingActivity;

    /**
     * 活动条目名称
     */
    @Column(name = "name")
    public String name;

    /**
     * 链接地址
     */
    @Column(name = "link_url")
    public String linkUrl;

    /**
     * 排序
     */
    @Column(name = "sort")
    public Integer sort;


    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;


    @Transient
    public List<MarketingActivityItemProduct> productList;


    /**
     *
     * @param activity
     * @return
     */
    public static List<MarketingActivityItem> findByActivity(MarketingActivity activity) {
        return MarketingActivityItem.find("deleted = ? and marketingActivity = ? order by sort" , DeletedStatus.UN_DELETED , activity).fetch();
    }

    /**
     *
     * @param id
     * @return
     */
    public static MarketingActivityItem findByItemId(long id){
        return MarketingActivityItem.find("id = ? and deleted = ?",id,DeletedStatus.UN_DELETED).first();
    }

    /**
     *
     * @param ids
     */
    public static void deleteMarketingActivityItem(Integer[] ids){
        MarketingActivityItemProduct.deleteMarketingActivityItemProducts(ids);
        String sqlUpdate = "update maketint_activity_items set deleted = 1 where id in ("+ UStringUtil.concatStr(",",ids)+")";
        JPA.em().createNativeQuery(sqlUpdate).executeUpdate();

    }

}
