package activity.knowledge;

import models.constants.DeletedStatus;
import models.merchant.Merchant;
import models.weixin.WebUser;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by buhaoba on 2016/10/12.
 */
public class WebUserKnowledge extends Model {


    /**
     * 知识点
     */
    @ManyToOne
    @JoinColumn(name = "knowledge_id")
    public Knowledge knowledge;


    /**
     * 微信用户阅读信息
     */
    @ManyToOne
    @JoinColumn(name = "web_user_id")
    public WebUser webUser;




    /**
     * 新闻标题
     */
    @Column(name = "title")
    public String title;

    /**
     * 文字内容
     */
    @Lob
    @Column(name = "content")
    public String content;


    /**
     * 语音路径
     */
    @Column(name = "voice")
    public String voice;

    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 购物篮创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

}
