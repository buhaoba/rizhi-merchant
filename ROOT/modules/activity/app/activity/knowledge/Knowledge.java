package activity.knowledge;

import models.constants.DeletedStatus;
import play.db.jpa.Model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import java.util.Date;

/**
 * Created by buhaoba on 2016/10/12.
 */
public class Knowledge extends Model {

    /**
     * 新闻标题
     */
    @Column(name = "title")
    public String title;

    /**
     * 文字内容
     */
    @Lob
    @Column(name = "content")
    public String content;


    /**
     * 语音路径
     */
    @Column(name = "voice")
    public String voice;

    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 购物篮创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

}
