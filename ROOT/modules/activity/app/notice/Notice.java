package notice;

import enums.ShowPosition;
import models.BaseModel;
import models.constants.DeletedStatus;

import javax.persistence.*;
import java.util.List;

/**
 * 公告
 * Created by shancheng on 16-6-27.
 */
@Entity
@Table(name = "activity_notices")
public class Notice extends BaseModel {


    /**
     * 公告标题
     */
    @Column(name = "title")
    public String title;

    /**
     * 公告内容
     */
    @Lob
    @Column(name = "content")
    public String content;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;


    /**
     * 图标
     */
    @Column(name = "icon")
    public String icon;

    /**
     * 公告类型
     */
    @Column(name = "show_position")
    @Enumerated(EnumType.STRING)
    public ShowPosition showPosition;


    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;



    public static OrderNotice findByNoticeId(long id){
        return OrderNotice.find("id = ? and deleted = ? ",id , DeletedStatus.UN_DELETED).first();
    }

    public static List<OrderNotice>  loadNoticeTop5 (){
        return OrderNotice.find("deleted = ? order by id desc limit 0,5",DeletedStatus.UN_DELETED).fetch();
    }

}
