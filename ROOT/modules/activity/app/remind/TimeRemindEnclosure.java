package remind;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.weixin.WebUser;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Administrator on 2016/10/22.
 */
@Entity
@Table(name = "time_remind_enclosures")
public class TimeRemindEnclosure extends BaseModel {

    /**
     *  用户
     */
    @JoinColumn(name = "time_remind_id")
    @ManyToOne
    public TimeRemind timeRemind;

    /**
     * 文件名字
     */
    @Column(name = "name")
    public String name;

   /**
     * 文件路径
     */
    @Column(name = "path")
    public String path;


    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;

    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    public static List<TimeRemindEnclosure> loadByTimeRemindId(long timeRemindId){
        return TimeRemindEnclosure.find("timeRemind.id=? and deleted=?",timeRemindId,DeletedStatus.UN_DELETED).fetch();

    }
    public  static TimeRemindEnclosure findByTimeRemindEnclosureId(long id){
        return TimeRemindEnclosure.find("id=? and deleted=?",id,DeletedStatus.UN_DELETED).first();

    }
    public  static List<TimeRemindEnclosure> findByTimeRemindEn(long id){
        return TimeRemindEnclosure.find("timeRemind.id=? and deleted=?",id,DeletedStatus.UN_DELETED).fetch();
    }



}
