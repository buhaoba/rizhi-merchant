package remind;

import models.BaseModel;
import models.constants.AvailableStatus;
import models.constants.DeletedStatus;
import models.enums.TreeNodeStatus;
import play.db.jpa.JPA;
import util.common.ConvertUtil;
import util.common.UStringUtil;

import javax.persistence.*;
import java.util.List;

/**
 * Created by youliangcheng on 2016/11/17.
 */
@Entity
@Table(name = "time_remind_type")
public class TimeRemindType extends BaseModel {

    /**
     * 类别编号
     */
    @Column(name = "code")
    public String code;

    /**
     * 类别名称
     */
    @Column(name = "name")
    public String name;

    /**
     * 末端节点
     */
    @Column(name = "is_leaf")
    public Boolean isLeaf;
    /**
     * 是否为叶子节点 open:叶子节点 close:非叶子节点
     */
    @Transient
    public TreeNodeStatus state;
    /**
     * 等级
     */
    @Column(name = "level")
    public Integer level;
    /**
     * 所属类别
     */
    @Column(name = "parent_type_id")
    public Long parentTypeId;

    @Transient
    public Integer isModify;

    @Transient
    public Integer isCheck;

    /**
     * 节点全路径
     */
    @Column(name = "node_id_path")
    public String nodeIdPath;

    /**
     * 排序号
     */
    @Column(name = "show_order")
    public Integer showOrder = 0;

    /**
     * 当前状态  禁用\可用
     */
    @Enumerated(EnumType.STRING)
    public AvailableStatus status;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;

    /**
     * 逻辑删除状态
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 查询根节点的所有 孩子节点
     * @param id
     * @return
     */
    public static List<TimeRemindType> loadTypeChildNodeList(long id){

        List<TimeRemindType> typeList = TimeRemindType.find("parentTypeId = ? and deleted = ? order by showOrder,id desc ",id,DeletedStatus.UN_DELETED).fetch();
        for(TimeRemindType type :typeList){
            type.state = type.isLeaf !=null && type.isLeaf ? TreeNodeStatus.open : TreeNodeStatus.closed;
        }
        return typeList;
    }

    /**
     * 判断该数据是否被使用
     * @param id
     * @return
     */
    public static Boolean checkIsUsed(long id){
        return TimeRemind.count("timeRemindType.id = ? and deleted = ?",id,DeletedStatus.UN_DELETED) > 0 ;
    }

    /**
     * 生成编号
     * @param parentType
     * @return
     */
    public static String createCode(TimeRemindType parentType){

        Long number = TimeRemindType.count("parentTypeId = ? ",parentType.id);
        return  number >= 100 ? ConvertUtil.toString(parentType.code)+number:(ConvertUtil.toString(parentType.code)+(ConvertUtil.toJson(number+100).substring(1)));

    }

    public static String createRootCode(TimeRemindType remindType){
        Long number = TimeRemindType.count("parentTypeId = 0 ");
        if(number < 10) {
            return "0" + number;
        }else {
            return ConvertUtil.toString(number);
        }
    }

    /**
     * 删除
     * @param delIds
     */
    public static void deleteTypes(Long[] delIds){
        for(Long delId : delIds){
            TimeRemindType timeRemindType = TimeRemindType.findById(delId);
            timeRemindType.deleted = DeletedStatus.DELETED;
            timeRemindType.save();
            if(!checkBrotherNode(timeRemindType.parentTypeId)){
                TimeRemindType parentType = TimeRemindType.findById(timeRemindType.parentTypeId);
                parentType.isLeaf = true;
                parentType.save();
            }
        }

    }

    public static Boolean checkBrotherNode(long parentTypeId){
        return TimeRemindType.count(" parentTypeId = ? and deleted = ?",parentTypeId,DeletedStatus.UN_DELETED)>0;
    }


}
