package enums;

/**
 * Created by liming on 16/7/27.
 * 自定义秒杀活动状态
 */
public enum  CustomActivityStatus {
    PLAN,//计划
    START,//开始
    END,//自然结束
    ABORT//强制终止
}
