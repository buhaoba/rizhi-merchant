package enums;

/**
 * Created by liming on 16/7/26.
 * 自定义活动类型
 */
public enum CustomActivityType {
    SECKILL, //秒杀
    PROMOTION, //促销
    ADVANCESALE //预售
}
