package enums;

/**
 * 商户首页模板类型
 * Created by buhaoba on 2016/10/10.
 */
public enum TempType {

    ORDINARY,  // 模板一
    LIANGFENWANG, // 凉粉网
    TEMPLATE_TWO // 模板二


}
