package enums;

/**
 * Created by buhaoba on 2016/10/19.
 */
public enum CouponType {

    FIRST_ORDER, //首次下单赠送
    NO_BODY, //如果没有 就赠送
    DIRECTIONAL, //定向赠送
    NEW_USER, //新用户赠送
    SHARE_FRIENDS_NO_BODY  //本来什么也没有 并且分享朋友圈
}
