package enums;

/**
 * Created by shancheng on 16-6-27.
 */
public enum  ShowPosition {

    INDEX_BLOCK , // 首页轮转图
    INDEX_NOTICE, // 首页头条
    INDEX_SECKILL, // 首页秒杀
    INDEX_NOTICE_ZS, //首页通知活动 左上角
    INDEX_NOTICE_YS, // 首页通知活动 右上角
    INDEX_NOTICE_ZX, // 首页通知活动 右下角
    INDEX_NOTICE_YX, // 首页公告活动 右下角
    INDEX_PROMOTION, // 首页促销产品
    INDEX_PRODUCT_RECOMMEND,  //首页产品推荐
    PROMOTION , // 促销页面
    SECKILL, //秒杀页面
    CLASSIFY ,// 分类
    MERCHANT_RECOMMEND //首页推荐商户
}
