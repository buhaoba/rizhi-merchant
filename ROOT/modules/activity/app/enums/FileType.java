package enums;

/**
 * 商户首页模板类型
 * Created by buhaoba on 2016/10/10.
 */
public enum FileType {

    FINANCE,  // Finance 财务
    PURCHASE, // Purchase 采购
    RECRUIT   //recruit 招聘

}
