package enums;

/**
 * Created by shancheng on 16-6-27.
 */
public enum ActivityType {

    ZHUANPAN, //转盘
    CHOUJIANG,  // 抽奖
    MIAOSHA,  // 秒杀
    NOTICE, // 公告活动
    PROMOTION // 促销
    // 。。。。
}
