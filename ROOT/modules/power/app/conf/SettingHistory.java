package conf;

import play.db.jpa.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by buhaoba on 16/3/29.
 */
@Entity
@Table(name = "setting_historys")
public class SettingHistory extends Model {

    /**
     * 优惠比例
     * 跟一切打折券无关
     */
    @Column(name = "discount_proportion")
    public Float discountProportion;

    /**
     * 优惠价格
     * 跟一切打折券无关
     */
    @Column(name = "discount_amount")
    public Double discountAmount;

    /**
     * 优惠券可使用比例
     * 跟系统优惠无关 可叠加
     */
    @Column(name = "coupon_proportion")
    public Float couponProportion;

    /**
     * 优惠券可使用金额
     * 跟系统优惠无关  可叠加
     */
    @Column(name = "coupon_amount")
    public Double couponAmount;

    /**
     * 大转盘活动开始时间
     */
    @Column(name = "start_wheelOfFortune")
    public Date startWheelOfFortune;
    /**
     * 大转盘活动结束时间
     */
    @Column(name = "end_wheelOfFortune")
    public Date endWheelOfFortune;
    /**
     * 分享得红包活动开始时间
     */
    @Column(name = "start_share")
    public Date startShare;
    /**
     * 分享得红包活动结束时间
     */
    @Column(name = "end_share")
    public Date endShare;
    /**
     * 修改时间
     */
    @Column(name = "update_at")
    public Date updateAt;

    /**
     * 创建时间
     */
    @Column(name = "create_at")
    public Date createAt;


    public SettingHistory() {
        super();
    }

    public SettingHistory(Float discountProportion , Double discountAmount , Float couponProportion , Double couponAmount,
                          Date startWheelOfFortune,Date endWheelOfFortune,Date startShare,Date endShare) {
        this.discountProportion = discountProportion;
        this.discountAmount = discountAmount;
        this.couponAmount = couponAmount;
        this.couponProportion = couponProportion;
        this.startWheelOfFortune = startWheelOfFortune;
        this.endWheelOfFortune = endWheelOfFortune;
        this.startShare = startShare;
        this.endShare = endShare;
        this.updateAt = new Date();
        this.createAt = new Date();
    }



}
