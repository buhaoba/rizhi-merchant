package conf;

import models.weixin.WebUser;
import models.weixin.WeixinUserWallet;
import order.Order;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Query;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by buhaoba on 16/3/29.
 */
@Entity
@Table(name = "settings")
public class Setting extends Model {

    public static final String CACHE_KEY = "SETTING_FIRST";

    /**
     * 优惠比例
     * 跟一切打折券无关
     */
    @Column(name = "discount_proportion")
    public Double discountProportion;

    /**
     * 优惠价格
     * 跟一切打折券无关
     */
    @Column(name = "discount_amount")
    public Double discountAmount;

    /**
     * 优惠券可使用比例
     * 跟系统优惠无关 可叠加
     */
    @Column(name = "coupon_proportion")
    public Double couponProportion;

    /**
     * 优惠券可使用金额
     * 跟系统优惠无关  可叠加
     */
    @Column(name = "coupon_amount")
    public Double couponAmount;

    /**
     * 是否开启活动
     */
    @Column(name = "is_open")
    public Boolean isOpen;


    /**
     * 赠送活动状态
     */
    @Column(name = "is_give")
    public Boolean isGive;

    /**
     * 赠送活动开始时间
     */
    @Column(name = "start_isgive")
    public Date startIsGive;
    /**
     * 赠送活动结束时间
     */
    @Column(name = "end_isgive")
    public Date endIsGive;
    /**
     * 大转盘活动开始时间
     */
    @Column(name = "start_wheelOfFortune")
    public Date startWheelOfFortune;
    /**
     * 大转盘活动结束时间
     */
    @Column(name = "end_wheelOfFortune")
    public Date endWheelOfFortune;
    /**
     * 分享得红包活动开始时间
     */
    @Column(name = "start_share")
    public Date startShare;
    /**
     * 分享得红包活动结束时间
     */
    @Column(name = "end_share")
    public Date endShare;
    /**
     * 投票活动开始时间
     */
    @Column(name = "start_vote")
    public Date startVote;
    /**
     * 投票活动结束时间
     */
    @Column(name = "end_vote")
    public Date endVote;
    /**
     * 活动开关
     */
    @Column(name = "is_start")
    public Boolean isStart;








    /**
     * 创建时间
     */
    @Column(name = "create_at")
    public Date createAt;

    public Setting() {
        super();
    }

    public Setting(Double discountProportion , Double discountAmount , Double couponProportion , Double couponAmount) {
        this.discountProportion = discountProportion;
        this.discountAmount = discountAmount;
        this.couponProportion = couponProportion;
        this.couponAmount = couponAmount;
        this.isOpen = true;
        this.createAt = new Date();
    }

    public static Double discountAmount(Double amount , WebUser wxUser) {
        Logger.info("Amount : %s | discountAmount WeixinUser : %s" , amount , wxUser);
        Setting setting = (Setting) Setting.all().first();
        if(setting == null) {
            return amount;
        } else {
            Double discountProportionAmount = 0d;
            Double couponProportionAmount = 0d;
            // 优惠比例 价格计算
            if (setting.discountProportion != null) {
                discountProportionAmount = amount * setting.discountProportion / 100;
            }
            // 优惠券比例 价格计算
            if (setting.couponProportion != null) {
                WeixinUserWallet wallet = WeixinUserWallet.findByWeixinUser(wxUser);
                Logger.info("Wallet : %s" , wallet);
                if (wallet != null && wallet.amount != null) {
                    Double _couponProportionAmount = amount * setting.couponProportion / 100 ; // amount.multiply(new BigDecimal(setting.couponProportion)).divide(new BigDecimal(100));
                    Logger.info("_couponProportionAmount : %s" , _couponProportionAmount);
                    couponProportionAmount = wallet.amount == _couponProportionAmount ? _couponProportionAmount : wallet.amount; // wallet.amount.compareTo(_couponProportionAmount) == 1 ? _couponProportionAmount : wallet.amount;
                    Logger.info("couponProportionAmount : %s" , couponProportionAmount);
                }
            }
            if (setting.discountAmount == null) {
                setting.discountAmount = 0d;
            }
            if (setting.couponAmount == null) {
                setting.couponAmount = 0d;
            }
            Logger.info("amount : %s |discountProportionAmount : %s | couponProportionAmount : %s | discountAmount : %s | couponAmount : %s" , amount, discountProportionAmount , couponProportionAmount , setting.discountAmount , setting.couponAmount);
            return amount - discountProportionAmount - couponProportionAmount - setting.discountAmount - setting.couponAmount;
        }
    }


    /**
     * 折扣后的价格
     * @param order
     * @return
     */
    public static Double discountAmount(Order order) {
        Setting setting = (Setting) Setting.all().first();
        if(setting == null) {
            return order.amount;
        } else {
            Double discountProportionAmount = 0d;
            Double couponProportionAmount = 0d;
            // 优惠比例 价格计算
            if (setting.discountProportion != null) {
                discountProportionAmount = order.amount * setting.discountProportion / 100 ; // order.amount.multiply(new BigDecimal(setting.discountProportion)).divide(new BigDecimal(100));
            }
            // 优惠券比例 价格计算
            if (setting.couponProportion != null) {
                WebUser wxUser = order.webUser;
                WeixinUserWallet wallet = WeixinUserWallet.findByWeixinUser(wxUser);
                if (wallet != null && wallet.amount != null) {
                    Double _couponProportionAmount = order.amount * setting.couponProportion / 100; // order.amount.multiply(new BigDecimal(setting.couponProportion)).divide(new BigDecimal(100));
                    Logger.info("Setting 卡圈比例优惠金额 : %s" , _couponProportionAmount);
                    couponProportionAmount = wallet.amount.compareTo(_couponProportionAmount) == 1 ? _couponProportionAmount : wallet.amount;
                    Logger.info("Setting 实际比对有 优惠金额");
                }
            }
            if (setting.discountAmount == null) {
                setting.discountAmount = 0d;
            }
            if (setting.couponAmount == null) {
                setting.couponAmount = 0d;
            }
            // 订单金额 - 系统折扣 - 系统优惠 - 优惠券折扣 - 优惠券优惠
            return order.amount - discountProportionAmount - couponProportionAmount - setting.discountAmount - setting.couponAmount;

                    //order.amount.subtract(discountProportionAmount).subtract(couponProportionAmount).subtract(setting.discountAmount).subtract(setting.couponAmount);
        }
    }
    public static void startActivity(Boolean isStart){
        String sql = "";
        Query query = null;
        if(isStart == true){
            sql = " update weixin_users w set w.is_prompt = 1 ";
        }else{
            sql = " update weixin_users w set w.is_prompt = 0 ";
        }

        query = JPA.em().createNativeQuery(sql);
        query.executeUpdate();

    }


}
