package models.enums;

/**
 * 微信企业号应用名.
 *
 * 在这里定义应用名称，并配置到不同接口中.
 */
public enum WxcpAppType {
    TRIP,  //出差管理

    NULL   // 什么都不是
}
