package models.enums;

/**
 * Created by buhaoba on 16/9/24.
 */
public enum IdentityType {

    USER,
    MERCHANT

}
