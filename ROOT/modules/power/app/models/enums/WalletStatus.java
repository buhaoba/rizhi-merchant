package models.enums;

/**
 * Created by buhaoba on 16/3/30.
 */
public enum WalletStatus {
    NORMAL, //可用
    FORZEN //冻结
}
