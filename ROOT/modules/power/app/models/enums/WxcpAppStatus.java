package models.enums;

/**
 * 企业号应用状态.
 */
public enum WxcpAppStatus {

    OPEN,  //可用
    FREEZE, //冻结

}
