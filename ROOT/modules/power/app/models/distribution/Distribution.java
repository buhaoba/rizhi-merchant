package models.distribution;

import models.constants.AvailableStatus;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import models.merchant.enums.MerchantType;
import org.omg.PortableInterceptor.DISCARDING;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * 分销商
 * Created by buhaoba on 16/6/16.
 */
@Entity
@Table(name = "distribution")
public class Distribution extends Model {

    /**
     * 来源商户
     */
    @ManyToOne
    @JoinColumn(name = "merchant_id")
    public Merchant merchant;


    /**
     * 分销商户
     */
    @ManyToOne
    @JoinColumn(name = "distribution_merchant_id")
    public Merchant distributionMerchant;



    /**
     * 可用状态
     */
    @Column(name = "available_status")
    @Enumerated(EnumType.STRING)
    public AvailableStatus availableStatus;


    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Column(name = "deleted")
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;


    /**
     * 判断 分享商户 是否已在 商户中注册
     * @param merchant
     * @param distributionMerchant
     * @return
     */
    public static Boolean checkDistribution(Merchant merchant , Merchant distributionMerchant) {
        return Distribution.count("merchant = ? and distributionMerchant = ? and availableStatus = ? and deleted = ?" , merchant , distributionMerchant , AvailableStatus.AVAILABLE , DeletedStatus.UN_DELETED) > 0 ? Boolean.TRUE : Boolean.FALSE;
    }

}
