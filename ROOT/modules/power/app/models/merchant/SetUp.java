package models.merchant;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.merchant.enums.BuyerType;
import models.merchant.enums.VisitorType;

import javax.persistence.*;
import java.util.List;

/**
 * Created by shancheng on 16-6-17.
 */
@Entity
@Table(name = "power_setups")
public class SetUp extends BaseModel {

    /**
     *
     */
    @Column(name = "name")
    public String name;

    /**
     *  订单最低金额
     */
    @Column(name = "min_amount")
    public Double minAmount;

    /**
     * 最高购买金额
     */
    @Column(name = "max_amount")
    public Double maxAmount;


    /**
     * 访客类型
     */
    @Column(name = "visitor_type")
    @Enumerated(EnumType.STRING)
    public VisitorType visitorType;



    /**
     * 购买者类型
     */
    @Column(name = "buyer_type")
    @Enumerated(EnumType.STRING)
    public BuyerType buyerType;

    /**
     * 访问是否需要授权
     */
    @Column(name = "visit_auth")
    public Boolean visitAuth = Boolean.FALSE;


    /**
     * 是否需要注册
     * 注册用户为 在  Distribution 中 有关联关系
     * Distribution.merchant   平台
     * Distribution.distributionMerchant  访问用户
     */
    @Column(name = "need_register")
    public Boolean needRegister;


    /**
     * 访客手机验证
     * true 必须验证
     * false 不需要验证
     */
    @Column(name = "visitor_phone")
    public Boolean visitorPhone;


    /**
     * 购买者 验证手机
     * true : 必须验证
     * false ： 不需要验证
     */
    @Column(name = "buyer_phone")
    public Boolean buyerPhone;


    /**
     * 必须设置收获地址
     */
    @Column(name = "need_address")
    public Boolean needAddress;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 根据ID查询
     */
    public static SetUp findBySetUpId(long id){
        return SetUp.find("id=? and deleted=?", id, DeletedStatus.UN_DELETED).first();
    }
    /**
     * 查询模版集合
     */

    public static List<SetUp> findALLSetUp(){
        return SetUp.find("deleted=?", DeletedStatus.UN_DELETED).fetch();
    }





}
