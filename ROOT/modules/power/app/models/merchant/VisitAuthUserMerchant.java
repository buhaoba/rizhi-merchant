package models.merchant;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.weixin.WebUser;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by buhaoba on 16/9/30.
 */
@Entity
@Table(name = "visit_auth_user_merchants")
public class VisitAuthUserMerchant extends BaseModel {

    @JoinColumn(name = "web_user_id")
    @ManyToOne
    public WebUser webUser;

    @JoinColumn(name = "merchant_id")
    @ManyToOne
    public Merchant merchant;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted = DeletedStatus.UN_DELETED;

//    /**
//     * 创建时间
//     */
//    @Column(name = "create_at")
//    public Date createAt;
//

    public VisitAuthUserMerchant() {
        super();
    }

    public VisitAuthUserMerchant(WebUser webUser , Merchant merchant) {
        this.webUser = webUser;
        this.merchant = merchant;
        this.deleted = DeletedStatus.UN_DELETED;
        this.createAt = new Date();
    }


    public static long countByWebUserAndMerchant(WebUser webUser , Merchant merchant) {
        return VisitAuthUserMerchant.count("webUser.id = ? and merchant.id = ? and deleted = ?", webUser.id, merchant.id, DeletedStatus.UN_DELETED);
    }

    public static List<VisitAuthUserMerchant> loadByUserId(long id){
        return VisitAuthUserMerchant.find(" webUser.id = ? and deleted = ? and merchant.deleted = ?" , id,DeletedStatus.UN_DELETED,DeletedStatus.UN_DELETED).fetch();
    }

}
