package models.merchant.enums;


/**
 * Created by buhaoba on 16/6/16.
 */
public enum MerchantType {

    STUDENT, // 学生
    GETI; // 个体商户

    public static String getMerchantType(MerchantType merchantType) {
        String merchantTypeStr = "";
        switch (merchantType) {
            case STUDENT:
                merchantTypeStr = "学生";
                break;
            case GETI:
                merchantTypeStr = "个体商户";
                break;
            default:
                merchantTypeStr = "";
                break;
        }
        return merchantTypeStr;
    }
 }

