package models.merchant.enums;

/**
 * Created by Administrator on 2017/9/22.
 */
public enum  VerifyState {

    WAIT, //等待审批
    VERIFY,  //提交审核资料
    PASS, // 审批通过
    NOTPASS, //审批未通过
    CLOSE //  关店

}
