package models.admin;

import jodd.bean.BeanCopy;
import models.BaseModel;
import models.admin.enums.OperateType;
import util.common.UStringUtil;

import javax.persistence.*;
import java.util.List;

/**
 * Created by liming on 16/6/29.
 */
@Entity
@Table(name = "admin_business_operate")
public class AdminBusinessOperate extends BaseModel {

    @ManyToOne
    @JoinColumn(name = "business_id")
    public AdminBusiness business;

    @Column(name = "name")
    public String name;

    @Column(name = "filterKey")
    public String filterKey;

    //操作类型 SYSTEM:系统默认 CUSTOM 自定义
    @Enumerated(EnumType.STRING)
    @Column(name = "operate_type")
    public OperateType operateType;

    @Column(name = "remark")
    public String remark;

    /**
     * 根据业务ID加载业务操作
     *
     * @param businessId
     * @return
     */
    public static List<AdminBusinessOperate> loadByBusinessId(long businessId) {
        return AdminBusinessOperate.find("business.id = ?1", businessId).fetch();
    }

    /**
     * 根据ID删除操作明细
     *
     * @param delItemArray
     */
    public static void deleteByIds(Integer[] delItemArray) {
        String delItemStr = UStringUtil.concatStr(",", delItemArray);
        AdminRoleOperate.delete("adminBusinessOperate.id in ("+ delItemStr + ")");
        AdminBusinessOperate.delete("id in (" + delItemStr + ")");
    }

    /**
     * 根据业务ID删除业务操作
     *
     * @param businessId
     */
    public static void deleteByBusinessId(long businessId) {
        AdminBusinessOperate.delete("business.id = ?1", businessId);
    }
    public static void  update(AdminBusinessOperate adminBusinessOperate){
        AdminBusinessOperate oldEntity =  AdminBusinessOperate.findById(adminBusinessOperate.id);
        BeanCopy.beans(adminBusinessOperate,oldEntity).ignoreNulls(true).copy();
        adminBusinessOperate.save();
    }
}
