package models.admin;

import jodd.bean.BeanCopy;
import models.BaseModel;
import models.admin.enums.BusinessType;
import models.constants.AvailableStatus;
import models.constants.DeletedStatus;
import play.modules.paginate.JPAExtPaginator;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

/**
 * Created by liming on 16/6/29.
 */
@Entity
@Table(name = "admin_business")
public class AdminBusiness extends BaseModel {
    //业务编号
    @Column(name = "code")
    public String code;

    //业务名称
    @Column(name = "name")
    public String name;

    //业务类型
    @Column(name = "business_type")
    @Enumerated(EnumType.STRING)
    public BusinessType businessType;

    @Column(name = "url")
    public String url;

    //业务控制层url
    @Column(name = "controller")
    public String controller;

    /**
     * 主对象
     */
    @Column(name = "main_entity")
    public String mainEntity;

    /**
     * 流水号前缀
     */
    @Column(name = "prefix")
    public String prefix;

    /**
     * 流水号最后日期
     */
    @Column(name = "code_date")
    public String codeDate;

    /**
     * 最大流水号
     */
    @Column(name = "code_num")
    public Integer codeNum;

    /**
     * 流水号长度
     */
    @Column(name = "code_length")
    public Integer codeLength;

    //排序号
    @Column(name = "show_order")
    public Integer showOrder = 0;


    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;


    /**
     * 业务流水号是否启用日期
     */
    @Column(name = "code_date_available")
    @Enumerated(EnumType.STRING)
    public AvailableStatus codeDateAvailable;

    @Column(name = "remark")
    public String remark;

    @Column(name = "version")
    public Integer version = 0;

    @Transient
    public List<Map<String, Object>> operateList;


    /**
     * Update
     * @param id
     * @param newObject
     */
//    public static void update(Long id, Product newObject) {
//        AdminBusiness adminBusiness = AdminBusiness.findById(id);
//        BeanCopy.beans(newObject, adminBusiness).ignoreNulls(true).copy();
//        adminBusiness.save();
//    }


    /**
     * 根据id 删除业务
     *
     * @param id
     */
    public static void deleteById(long id) {
        AdminBusiness.delete("id = ?1", id);
    }

    /**
     * 分页查询.
     */
    public static JPAExtPaginator<AdminBusiness> findByCondition(Map<String, Object> conditionMap, String orderByExpress, int pageNumber, int pageSize) {
        // 拼接sql
        StringBuilder xsqlBuilder = new StringBuilder("t.deleted=models.constants.DeletedStatus.UN_DELETED")
                .append("/~ and t.id = {id} ~/")
                .append("/~ and t.code like {code} ~/") //编号
                .append("/~ and t.name like {name} ~/") //名称
                .append("/~ and t.businessType = {businessType} ~/") //业务类型
                .append("/~ and t.createAt >= {beganDate} ~/") //创建时间
                .append("/~ and t.createAt < {endDate}  ~/"); //创建时间

        util.xsql.XsqlBuilder.XsqlFilterResult result = new util.xsql.XsqlBuilder().generateHql(xsqlBuilder.toString(), conditionMap);
        JPAExtPaginator<AdminBusiness> orderPage = new JPAExtPaginator<>("AdminBusiness t", "t", AdminBusiness.class,
                result.getXsql(), conditionMap).orderBy(orderByExpress);
        orderPage.setPageNumber(pageNumber);
        orderPage.setPageSize(pageSize);
        orderPage.setBoundaryControlsEnabled(false);
        return orderPage;
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public static AdminBusiness findByBusinessId(long id){
        return AdminBusiness.find("id = ?1 and deleted = ?2" ,id , DeletedStatus.UN_DELETED).first();
    }

    public static List<AdminBusiness> findListByStatus() {
        return AdminBusiness.find("deleted = ?1 ",DeletedStatus.UN_DELETED).fetch();
    }

    /**
     * 每个实体类 增加一个 update方法
     * @param adminBusiness
     */
    public static void update(AdminBusiness adminBusiness) {
        AdminBusiness oldEntity = AdminBusiness.findById(adminBusiness.id);
        BeanCopy.beans(adminBusiness, oldEntity).ignoreNulls(true).copy();
        adminBusiness.save();
    }

    /**
     * 根据code地质查询业务
     * @param code
     * @return
     */
    public static AdminBusiness findByCode(String code){
        return AdminBusiness.find("code = ?1 and deleted = ?2" , code , DeletedStatus.UN_DELETED).first();
    }

}
