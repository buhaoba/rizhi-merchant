package models.admin;

import jodd.bean.BeanCopy;
import models.BaseModel;
import models.constants.DeletedStatus;
import models.enums.TreeNodeStatus;
import models.weixin.crop.Corp;

import javax.persistence.*;
import java.util.List;

/**
 * Created by liming on 16/6/29.
 */
@Entity
@Table(name = "admin_dept")
public class AdminDept extends BaseModel {

    /**
     * 所属企业号公司.
     */
    @ManyToOne
    @JoinColumn(name = "corp_id")
    public Corp corp;

    /**
     * 微信企业号维护的部门ID.
     *
     * 如果是根部门，wxDeptId是1
     */
    @Column(name = "wx_dept_id")
    public Integer wxDeptId;

    /**
     * 部门编号
     */
    @Column(name = "code")
    public String code;

    /**
     * 部门名称
     */
    @Column(name = "name")
    public String name;

    /**
     * 父节点ID
     */
    @Column(name = "parent_id")
    public Long parentId;

    /**
     * 是否为叶子节点
     */
    @Column(name = "is_leaf")
    public Boolean isLeaf;

    /**
     * 是否为叶子节点 open:叶子节点 close:非叶子节点
     */
    @Transient
    @Enumerated(EnumType.STRING)
    public TreeNodeStatus state;

    /**
     * 节点全路径
     */
    @Column(name = "node_id_path")
    public String nodeIdPath;

    /**
     * 排序号
     */
    @Column(name = "show_order")
    public Integer showOrder = 0;

    @Column(name = "remark")
    public String remark;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 创建人ID
     */
    @Column(name = "ct_user_id")
    public Long ctUserId;


    /**
     * 加载孩子节点
     *
     * @param id
     * @return
     */
    public static List<AdminDept> loadChildAdmnDept(long id) {
        List<AdminDept> adminDeptList = AdminDept.find("parentId = ?1 and deleted = ?2 order by showOrder,id ", id, DeletedStatus.UN_DELETED).fetch();
        for (AdminDept adminDept : adminDeptList) {
            adminDept.state = adminDept.isLeaf != null && adminDept.isLeaf ? TreeNodeStatus.open : TreeNodeStatus.closed;
        }
        return adminDeptList;
    }

    /**
     * 删除部门
     *
     * @param adminDeptId
     */
    public static void deleteAdminDept(Long adminDeptId) {
        AdminDept adminDept = AdminDept.findById(adminDeptId);
        adminDept.deleted = DeletedStatus.DELETED;
        adminDept.save();
        Long brotherQty = AdminDept.loadBrotherQty(adminDept);
        if (brotherQty == 0) {
            AdminDept parentAdminDept = AdminDept.findById(adminDept.parentId);
            parentAdminDept.isLeaf = true;
            parentAdminDept.save();
        }

    }

    /**
     * 判断部门是否被使用
     */
    public static Boolean checkIsUsed(long id) {
        return AdminUser.count("adminDept.id=?1", id) > 0;
    }

    /**
     * 获取部门兄弟节点数量
     *
     * @param adminDept
     * @return
     */
    public static long loadBrotherQty(AdminDept adminDept) {
        return AdminDept.count("parentId = ?1 and deleted = ?2", adminDept.parentId, DeletedStatus.UN_DELETED);
    }

    /**
     * 加载所有部门
     *
     * @return
     */
    public static List<AdminDept> loadAllDept() {
        List<AdminDept> deptList = AdminDept.find("deleted = ?1 order by nodeIdPath ", DeletedStatus.UN_DELETED).fetch();
        return deptList;
    }

    public static AdminDept findByWxDeptId(Corp corp, Integer wxDeptId) {
        return AdminDept.find("corp=? and wxDeptId=?1", corp, wxDeptId).first();
    }

    /**
     * 根据ID查询部门信息
     * @param id
     * @return
     */
    public static AdminDept findBydeptId(long id){
        return AdminDept.find("id = ?1 and deleted = ?2 ",id , DeletedStatus.UN_DELETED).first();
    }

    public static void  update(AdminDept adminDept){
        AdminDept oldEntity =  AdminDept.findById(adminDept.id);
        BeanCopy.beans(adminDept,oldEntity).ignoreNulls(true).copy();
        adminDept.save();
    }


}
