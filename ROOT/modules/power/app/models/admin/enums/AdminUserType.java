package models.admin.enums;

/**
 * Created by liming on 16/8/4.
 * 用户类型
 */
public enum AdminUserType {
    NORMAL("普通职员"),//普通职员
    LOGISTICS("物流人员") ,//物流
    ACCOUNTANT("财务人员") ;//财务人员
    private String code;

    private AdminUserType(String value) {
        code = value;
    }

    @Override
    public String toString() {
        return code;
    }

}
