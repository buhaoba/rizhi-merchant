package models.admin.enums;

/**
 * Created by liming on 16/6/22.
 */
public enum BusinessType {
    //普通业务
    COMM("普通业务"),
    //报表业务
    REPORT("报表业务");

    private String code;

    private BusinessType(String value) {
        code = value;
    }

    @Override
    public String toString() {
        return code;
    }
}
