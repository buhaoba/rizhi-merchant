package models.admin.enums;

/**
 * Created by youliangcheng on 17/7/3.
 */
public enum AdminIcon {
    cog//指示板 linecons-cog
    ,desktop //  桌面 linecons-desktop
    ,note // 注意linecons-note
    , star// 星标 linecons-star
    , mail // 邮件linecons-mail
    , database // 数据 linecons-database
    , params // linecons-params
    , beaker // 烧杯 linecons-beaker
    , globe // 全球linecons-globe
    , cloud // 云 linecons-cloud
}
