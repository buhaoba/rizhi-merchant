package models.admin.enums;

/**
 * Created by liming on 16/8/4.
 * 用户权限类型
 */
public enum AdminUserAuth {
    //部门权限
    DEPT("部门权限"),
    //个人权限
    PERSONAL("个人权限");

    private String code;

    private AdminUserAuth(String value) {
        code = value;
    }

    @Override
    public String toString() {
        return code;
    }
}
