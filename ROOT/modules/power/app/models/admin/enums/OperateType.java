package models.admin.enums;

/**
 * Created by liming on 16/6/30.
 */
public enum OperateType {
    //系统默认
    SYSTEM("系统"),//系统
    //自定义
    CUSTOM ("自定义");
    private String code;

    private OperateType(String value) {
        code = value;
    }

    @Override
    public String toString() {
        return code;
    }
}
