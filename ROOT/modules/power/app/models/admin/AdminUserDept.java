package models.admin;

import jodd.bean.BeanCopy;
import models.BaseModel;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by liming on 16/7/2.
 */
@Entity
@Table(name = "admin_user_dept")
public class AdminUserDept extends BaseModel {
    @ManyToOne
    @JoinColumn(name = "admin_user_id")
    public AdminUser adminUser;

    @ManyToOne
    @JoinColumn(name = "admin_dept_id")
    public AdminDept adminDept;

    /**
     * 删除用户管理部门
     * @param userId
     */
    public static void deleteByUserId(long userId){
        AdminUserDept.delete("adminUser.id=?1", userId);
    }

    /**
     * 查看用户管理部门
     */
    public static List<AdminUserDept> findByUserId(long userId){
        return AdminUserDept.find("adminUser.id=?1", userId).fetch();
    }
    public static void  update(AdminUserDept adminUserDept){
        AdminUserDept oldEntity =  AdminUserDept.findById(adminUserDept.id);
        BeanCopy.beans(adminUserDept,oldEntity).ignoreNulls(true).copy();
        adminUserDept.save();
    }

}
