package models.admin;

import jodd.bean.BeanCopy;
import play.db.jpa.Model;
import play.modules.paginate.JPAExtPaginator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Map;

/**
 * Created by liming on 16/6/29.
 */
@Entity
@Table(name = "admin_operate")
public class AdminOperate extends Model {

    @Column(name = "name")
    public String name;

    @Column(name = "filterKey")
    public String filterKey;

    @Column(name = "remark")
    public String remark;



    /**
     * 分页查询.
     */
    public static JPAExtPaginator<AdminOperate> findByCondition(Map<String, Object> conditionMap, String orderByExpress, int pageNumber, int pageSize) {
        // 拼接sql
        StringBuilder xsqlBuilder = new StringBuilder("1=1")
                .append("/~ and t.id = {id} ~/")
                .append("/~ and t.filterKey like {filterKey} ~/") //过滤关键字
                .append("/~ and t.name like {name} ~/"); //名称

        util.xsql.XsqlBuilder.XsqlFilterResult result = new util.xsql.XsqlBuilder().generateHql(xsqlBuilder.toString(), conditionMap);
        JPAExtPaginator<AdminOperate> orderPage = new JPAExtPaginator<>("AdminOperate t", "t", AdminOperate.class,
                result.getXsql(), conditionMap).orderBy(orderByExpress);
        orderPage.setPageNumber(pageNumber);
        orderPage.setPageSize(pageSize);
        orderPage.setBoundaryControlsEnabled(false);
        return orderPage;
    }

    public static void  update(AdminOperate adminOperate){
        AdminOperate oldEntity =  AdminOperate.findById(adminOperate.id);
        BeanCopy.beans(adminOperate,oldEntity).ignoreNulls(true).copy();
        adminOperate.save();
    }
}
