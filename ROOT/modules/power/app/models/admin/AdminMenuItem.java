package models.admin;

import jodd.bean.BeanCopy;
import models.BaseModel;
import models.enums.TreeNodeStatus;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liming on 16/7/1.
 */
@Entity
@Table(name = "admin_menu_item")
public class AdminMenuItem extends BaseModel {
    /**
     * 菜单名称
     */
    @Column(name = "name")
    public String name;

    /**
     * 父级节点
     */
    @Column(name = "parent_id")
    public Long parentId;

    /**
     * 父亲节点对象
     */
    @Transient
    public AdminMenuItem parent;

    /**
     * 业务
     */
    @ManyToOne
    @JoinColumn(name = "business_id")
    public AdminBusiness business;

    /**
     * 关联业务主Id
     */
    @Column(name = "menu_id")
    public Long menuId;

    /**
     * 主业务菜单对象
     */
    @Transient
    public AdminMenu menu;

    /**
     * 是否叶子节点
     */
    @Column(name = "is_leaf")
    public Boolean isLeaf;

    /**
     * 为了控件展示提供的标示叶子节点的字段
     */
    @Transient
    public TreeNodeStatus state;

    /**
     * 排序号
     */
    @Column(name = "show_order")
    public Integer showOrder = 0;

    /**
     * 子节点
     */
    @Transient
    public List<AdminMenuItem> childrenList;

    /**
     * 加载孩子节点
     *
     * @param menuItemId
     * @return
     */
    public static List<AdminMenuItem> loadChildMenuItem(long menuId, long menuItemId) {
        List<AdminMenuItem> menuList = AdminMenuItem.find("menuId = ?1 and parentId = ?2 order by showOrder,id desc", menuId, menuItemId).fetch();
        for (AdminMenuItem menu : menuList)
            menu.state = menu.isLeaf != null && menu.isLeaf ? TreeNodeStatus.open : TreeNodeStatus.closed;
        return menuList;
    }

    /**
     * 删除菜单
     *
     * @param
     */
    public static void deleteMenuItems(Long[] deleteMenuItemList) {
        for (Long menuItemId : deleteMenuItemList) {
            AdminMenuItem menuItem = AdminMenuItem.findById(menuItemId);
            long parentId = menuItem.parentId;
            menuItem.delete();
            Long brotherQty = AdminMenuItem.loadBrotherQty(parentId);
            if (brotherQty == 0) {
                AdminMenuItem parent = AdminMenuItem.findById(parentId);
                if(parent!=null){
                    parent.isLeaf = true;
                    parent.save();
                }
            }
        }

    }

    /**
     * 获取兄弟节点个数
     *
     * @param parentId
     * @return
     */
    public static long loadBrotherQty(long parentId) {
        return AdminMenuItem.count("parentId = ?1", parentId);
    }

    /**
     * 获取树形菜单数据
     * @param menuId
     * @return
     */
    public static List<Map<String, Object>> findMenuTreeByMenuId(long menuId) {
        List<AdminMenuItem> menuItemList = new ArrayList<AdminMenuItem>();
        List<Map<String,Object>> menuTree = new ArrayList<>();
        Map<Long,Object> menuMap = new HashMap<Long, Object>();
        try {
            menuItemList = AdminMenuItem.find("menuId = ?1 order by showOrder , id ",menuId).fetch();
            for(AdminMenuItem adminMenuItem: menuItemList) {
                Map<String, Object> newMenuItem = new HashMap<String, Object>();
                newMenuItem.put("menu_item_id",adminMenuItem.id);
                newMenuItem.put("parent_item_id",adminMenuItem.parentId);
                newMenuItem.put("text",adminMenuItem.name);
                newMenuItem.put("showOrder",adminMenuItem.showOrder);
                newMenuItem.put("businessId",adminMenuItem.business!=null ?adminMenuItem.business.id : null );

                if(!adminMenuItem.isLeaf) {
                    newMenuItem.put("nodes",new ArrayList<Map<String, Object>>());
                }
                if(menuMap.containsKey(adminMenuItem.parentId)) {
                    Map<String, Object> parentNode = (Map<String, Object>) menuMap.get(adminMenuItem.parentId);
                    List<Map<String, Object>> ndoes = (List<Map<String, Object>>) parentNode.get("nodes");
                    if (ndoes != null)
                        ndoes.add(newMenuItem);
                }else {
                    menuTree.add(newMenuItem);
                }
                menuMap.put(adminMenuItem.id,newMenuItem);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return menuTree;
    }

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    public static AdminMenuItem finbByMenuItemId(long id){
        return AdminMenuItem.find("id = ?1" , id).first();
    }

    public static void  update(AdminMenuItem adminMenuItem){
        AdminMenuItem oldEntity =  AdminMenuItem.findById(adminMenuItem.id);
        BeanCopy.beans(adminMenuItem,oldEntity).ignoreNulls(true).copy();
        adminMenuItem.save();
    }
}
