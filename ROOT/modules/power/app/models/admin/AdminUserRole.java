package models.admin;

import jodd.bean.BeanCopy;
import models.BaseModel;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by liming on 16/7/5.
 */
@Entity
@Table(name = "admin_user_role")
public class AdminUserRole extends BaseModel {

    /**
     * 用户
     */
    @ManyToOne
    @JoinColumn(name = "admin_user_id")
    public AdminUser adminUser;

    /**
     * 角色
     */
    @ManyToOne
    @JoinColumn(name = "admin_role_id")
    public AdminRole adminRole;

    /**
     * 删除用户角色
     * @param userId
     */
    public static void deleteByUserId(long userId){
        AdminUserRole.delete("adminUser.id=?1", userId);
    }

    /**
     * 查询用户角色
     * @param userId
     * @return
     */
    public static List<AdminUserRole> findByUserId(long userId){
        return AdminUserRole.find("adminUser.id=?1", userId).fetch();
    }


    /**
     * 查询用户角色
     * @param userId
     * @return
     */
    public static AdminUserRole findUserById(long userId){
        return AdminUserRole.find("adminUser.id=?1", userId).first();
    }

    public static void  update(AdminUserRole adminUserRole){
        AdminUserRole oldEntity =  AdminUserRole.findById(adminUserRole.id);
        BeanCopy.beans(adminUserRole,oldEntity).ignoreNulls(true).copy();
        adminUserRole.save();
    }

}
