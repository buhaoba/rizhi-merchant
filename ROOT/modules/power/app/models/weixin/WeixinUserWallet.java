package models.weixin;

import models.constants.DeletedStatus;
import models.enums.WalletStatus;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * 微信用户钱包
 * Created by buhaoba on 16/3/10.
 */
@Entity
@Table(name = "weixin_user_wallets")
public class WeixinUserWallet extends Model {

    /**
     * 关联的微信用户.
     */
    @ManyToOne
    @JoinColumn(name = "weixin_user_id")
    public WebUser webUser;


    /**
     * 收入金额
     */
    @Column(name = "amount")
    public Double amount;


    /**
     * 当前状态
     * 可用
     * 冻结
     */
    @Enumerated(EnumType.STRING)
    public WalletStatus status;

    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;


    public WeixinUserWallet() {
        super();
    }

    public WeixinUserWallet(WebUser webUser , Double amount) {
        this.webUser = webUser;
        this.amount = amount;
        this.status = WalletStatus.NORMAL;
        this.createdAt = new Date();
        this.deleted = DeletedStatus.UN_DELETED;
    }


    /**
     * 判断登录用户 是否有代金券
     * @param webUser
     * @return
     */
    public static WeixinUserWallet findByWeixinUser(WebUser webUser) {
        return WeixinUserWallet.find("webUser = ? and status = ? and deleted = ?" , webUser , WalletStatus.NORMAL , DeletedStatus.UN_DELETED).first();
    }


}
