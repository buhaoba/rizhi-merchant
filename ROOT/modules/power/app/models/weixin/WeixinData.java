package models.weixin;

import jodd.bean.BeanCopy;
import models.BaseModel;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import play.modules.paginate.JPAExtPaginator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by shancheng on 16-6-27.
 */
@Entity
@Table(name = "weixin_datas")
public class WeixinData extends BaseModel {

    /**
     * name
     * 名称
     */
    @Column(name = "name")
    public String name;
    /**
     * AppId
     */
    @Column(name = "app_id")
    public String appId;

    /**
     * 密钥
     */
    @Column(name = "app_secure")
    public String appSecure;

    /**
     * Token
     */
    @Column(name = "app_token")
    public String appToken;

    /**
     * appAesKey
     */
    @Column(name = "app_aes_key")
    public String appAesKey;


    /**
     * 商户支付ID
     */
    @Column(name = "mch_id")
    public String mchId;

    /**
     * 商户支付秘钥
     */
    @Column(name = "pay_key")
    public String payKey;

    /**
     * 商户支付秘钥
     */
    @Column(name = "pay_path")
    public String payPath;

    /**
     * 公众号过期时间
     */
    @Column(name = "expiration_date")
    @Temporal(TemporalType.DATE)
    public Date  expirationDate;

    /**
     * ping++ API Key
     */
    @Column(name = "pingpp_api_key")
    public String pingppApiKey;

    /**
     * ping++ Api Id
     */
    @Column(name = "pingpp_api_id")
    public String pingppApiId;

    /**
     * 用户注册时间.
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "deleted")
    public DeletedStatus deleted = DeletedStatus.UN_DELETED;


    /**
     * 分页查询.
     */
    public static JPAExtPaginator<WeixinData> findByCondition(Map<String, Object> conditionMap, String orderByExpress, int pageNumber, int pageSize) {
        StringBuilder xsqlBuilder = new StringBuilder("t.deleted=models.constants.DeletedStatus.UN_DELETED ")
                .append("/~ and t.name like {name} ~/")
                .append("/~ and t.appId = {appId} ~/");

        util.xsql.XsqlBuilder.XsqlFilterResult result = new util.xsql.XsqlBuilder().generateHql(xsqlBuilder.toString(), conditionMap);
        JPAExtPaginator<WeixinData> orderPage = new JPAExtPaginator<WeixinData>("WeixinData t", "t", WeixinData.class,
                result.getXsql(), conditionMap).orderBy(orderByExpress);
        orderPage.setPageNumber(pageNumber);
        orderPage.setPageSize(pageSize);
        orderPage.setBoundaryControlsEnabled(false);
        return orderPage;
    }

    /**
     * Update
     * @param id
     * @param newObject
     */
    public static void update(Long id, WeixinData newObject) {
        WeixinData weixinData = WeixinData.findById(id);
        BeanCopy.beans(newObject, weixinData).ignoreNulls(true).copy();
        weixinData.save();
    }

    /**
     * 根据ID查找WeixinData
     */
    public static WeixinData findByWeixinDataId(long id){
        return WeixinData.find("id=?1 and deleted=?2",id,DeletedStatus.UN_DELETED).first();
    }

    /**
     * 查询所有公众号
     */
    public static List<WeixinData> findAllWeixinData(){
        return WeixinData.find("deleted=?1", DeletedStatus.UN_DELETED).fetch();
    }

    /**
     * 检查是否被使用
     * @param weixinDataId
     * @return
     */
    public static Boolean checkIsUsed(long weixinDataId){
        Boolean isUsed = false;
        //是否被merchant使用
        long userNum = WebUser.count("weixinData.id = ?1 and deleted = ?2" , weixinDataId , DeletedStatus.UN_DELETED);
        if(userNum > 0)
            isUsed = true ;
        //是否被webUser使用
        long merchantNum = Merchant.count("weixinData.id = ?1 and deleted = ?2 " , weixinDataId , DeletedStatus.UN_DELETED);
        if(merchantNum > 0)
            isUsed = true ;
        return isUsed;
    }


}
