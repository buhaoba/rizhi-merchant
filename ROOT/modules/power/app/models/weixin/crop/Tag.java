package models.weixin.crop;

import models.admin.AdminUser;
import models.constants.DeletedStatus;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * 企业号 公司标签.
 */
@Entity
@Table(name = "wxcp_tags")
public class Tag extends Model {

    /**
     * 所属企业号公司.
     */
    @ManyToOne
    @JoinColumn(name = "corp_id")
    public Corp corp;

    /**
     * 微信企业号维护的标签ID.
     */
    @Column(name = "wx_tag_id")
    public Integer wxTagId;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted = DeletedStatus.UN_DELETED;

    /**
     * 标签名称.
     */
    @Column(name = "name", length = 50)
    public String name;

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(name = "wxcp_tags_users",
            inverseJoinColumns = @JoinColumn(name = "user_id"),
            joinColumns = @JoinColumn(name = "tag_id"))
    public Set<AdminUser> users;



    public  static List<Tag> loadTagId(long tagId){
        return Tag.find("corp.id =? and  deleted=?", tagId, DeletedStatus.UN_DELETED).fetch();
    }
    public static Tag findByTagId(long id) {
        return Tag.find("id=? and deleted =?", id, DeletedStatus.UN_DELETED).first();
    }
}
