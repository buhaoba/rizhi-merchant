package models.weixin.crop.bean;

import java.util.Arrays;

/**
 * 微信企业号 应用套件 授权的应用信息
 */
public class WxCpAgent {
    /**
     * 授权方应用id.
     */
    public String id;

    /**
     * 授权方应用名字.
     */
    public String name;

    /**
     * 授权方应用圆形头像.
     */
    public String roundLogoUrl;

    /**
     * 授权方应用方形头像.
     */
    public String squareLogoUrl;

    /**
     * 服务商套件中的对应应用id.
     */
    public String appId;

    /**
     * 授权方应用敏感权限组，目前仅有get_location，表示是否有权限设置应用获取地理位置的开关.
     */
    public String[] apiGroup;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("WxCpAgent{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", roundLogoUrl='").append(roundLogoUrl).append('\'');
        sb.append(", squareLogoUrl='").append(squareLogoUrl).append('\'');
        sb.append(", appId='").append(appId).append('\'');
        sb.append(", apiGroup=").append(Arrays.toString(apiGroup));
        sb.append('}');
        return sb.toString();
    }
}
