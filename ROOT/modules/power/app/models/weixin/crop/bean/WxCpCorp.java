package models.weixin.crop.bean;


import java.util.List;

/**
 * 微信企业号应用套件相关的企业信息.
 */
public class WxCpCorp {

    /**
     * 授权方企业号id.
     */
    public String id;

    /**
     * 永久授权码，从get_permanent_code接口中获取
     */
    public String permanentCode;

    /**
     * 授权方企业号名称.
     */
    public String name;

    /**
     * 授权方企业号类型，认证号：verified, 注册号：unverified，体验号：test.
     */
    public String type;

    /**
     * 授权方企业号圆形头像.
     */
    public String roundLogoUrl;

    /**
     * 授权方企业号方形头像.
     */
    public String squareLogoUrl;

    /**
     * 授权方企业号用户规模.
     */
    public Integer userMax;

    /**
     * 授权方企业号应用规模.
     */
    public Integer agentMax;

    /**
     * 授权方企业号二维码.
     */
    public String wxQrCode;

    public List<WxCpAgent> agents;


    public Integer getMaxAgailableAgentId() {
        if (this.agents == null) {
            return 1;
        }
        int maxAgentId = 0;
        for (WxCpAgent agent : agents) {
            if (Integer.parseInt(agent.id) > maxAgentId) {
                maxAgentId = Integer.parseInt(agent.id);
            }
        }
        return maxAgentId + 1;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("WxCpCorp{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", roundLogoUrl='").append(roundLogoUrl).append('\'');
        sb.append(", squareLogoUrl='").append(squareLogoUrl).append('\'');
        sb.append(", userMax=").append(userMax);
        sb.append(", agentMax=").append(agentMax);
        sb.append(", wxQrCode='").append(wxQrCode).append('\'');
        sb.append(", agents=").append(agents);
        sb.append('}');
        return sb.toString();
    }
}
