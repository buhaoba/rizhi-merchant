package models.weixin.crop.bean;

import com.google.gson.*;
import me.chanjar.weixin.common.util.json.GsonHelper;

import java.lang.reflect.Type;

/**
 * WxCpCorp转换适配器.
 */
public class WxCpCorpGsonAdapter implements JsonDeserializer<WxCpCorp>, JsonSerializer<WxCpCorp> {
    @Override
    public WxCpCorp deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject o = json.getAsJsonObject();
        WxCpCorp wxCpCorp = new WxCpCorp();
        wxCpCorp.id = GsonHelper.getString(o, "corpid");
        wxCpCorp.name = GsonHelper.getString(o, "corp_name");
        wxCpCorp.type = GsonHelper.getString(o, "corp_type");
        wxCpCorp.roundLogoUrl = GsonHelper.getString(o, "corp_round_logo_url");
        wxCpCorp.squareLogoUrl = GsonHelper.getString(o, "corp_square_logo_url");
        wxCpCorp.userMax = GsonHelper.getInteger(o, "corp_user_max");
        wxCpCorp.agentMax = GsonHelper.getInteger(o, "corp_agent_max");
        wxCpCorp.wxQrCode = GsonHelper.getString(o, "corp_wxqrcode");
        return wxCpCorp;
    }

    @Override
    public JsonElement serialize(WxCpCorp wxCpCorp, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject o = new JsonObject();
        o.addProperty("corpid", wxCpCorp.id);
        o.addProperty("corp_name", wxCpCorp.name);
        o.addProperty("corp_type", wxCpCorp.type);
        o.addProperty("corp_square_logo_url", wxCpCorp.squareLogoUrl);
        o.addProperty("corp_user_max", wxCpCorp.userMax);
        o.addProperty("corp_agent_max", wxCpCorp.agentMax);
        o.addProperty("corp_wxqrcode", wxCpCorp.wxQrCode);
        return o;
    }
}
