package models.weixin.amount;

import models.constants.DeletedStatus;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * 充值 赠送
 * Created by buhaoba on 2016/10/11.
 */
@Entity
@Table(name = "power_give_recharge")
public class GiveRecharge extends Model {

    /**
     * 赠送名称
     */
    @Column(name = "name")
    public String name;

    /**
     * 最低充值金额
     */
    @Column(name = "min_amount")
    public Double minAmount;

    /**
     * 最大充值金额
     */
    @Column(name = "max_amount")
    public Double maxAmount;


    /**
     * 赠送金额
     */
    @Column(name = "give_amount")
    public Double giveAmount;

    /**
     * 用户注册时间
     */
    @Column(name = "created_at")
    public Date createAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "deleted")
    public DeletedStatus deleted = DeletedStatus.UN_DELETED;


    public GiveRecharge() {
        super();
    }

    public GiveRecharge(String name , Double minAmount , Double maxAmount , Double giveAmount) {
        this.name = name;
        this.maxAmount = maxAmount;
        this.minAmount = minAmount;
        this.giveAmount = giveAmount;
        this.createAt = new Date();
        this.deleted = DeletedStatus.UN_DELETED;
    }



}
