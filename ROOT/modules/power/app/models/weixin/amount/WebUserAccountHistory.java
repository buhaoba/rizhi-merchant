package models.weixin.amount;

import jodd.bean.BeanCopy;
import models.constants.DeletedStatus;
import models.weixin.WebUser;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 微信用户账户设置
 * Created by shancheng on 16-6-17.
 */
@Entity
@Table(name = "power_web_user_account_historys")
public class WebUserAccountHistory extends Model {

    private static final long serialVersionUID = 28910911232181221L;

    /**
     * 微信账户
     */
    @JoinColumn(name = "account_id")
    @ManyToOne
    public WebUserAccount account;

    /**
     * 充值金额
     */
    @Column(name = "amount")
    public Double amount;

    /**
     * 赠送金额
     */
    @Column(name = "give_amount")
    public Double giveAmount;

    /**
     * 充值内容
     */
    @Column(name = "content")
    public String content;


    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "deleted")
    public DeletedStatus deleted = DeletedStatus.UN_DELETED;

    public WebUserAccountHistory() {
        super();
    }


    public WebUserAccountHistory(WebUserAccount account , Double amount , Double giveAmount , String content) {
        this.account = account;
        this.amount = amount;
        this.giveAmount = giveAmount;
        this.content = content;
        this.createAt = new Date();
        this.deleted = DeletedStatus.UN_DELETED;
    }


    /**
     * 按weixinUser值更新指定ID的WeixinUser.
     */
    public static void update(Long id, WebUserAccountHistory webUserMerchant) {
        WebUserAccountHistory oldEntity = WebUserAccountHistory.findById(id);
        BeanCopy.beans(webUserMerchant, oldEntity).ignoreNulls(true).copy();
        oldEntity.save();
    }

    public static WebUserAccountHistory findByWebUser(WebUser webUser) {
        return WebUserAccountHistory.find("deleted = ? and account.webUser = ?" , DeletedStatus.UN_DELETED , webUser).first();
    }

    public static List<WebUserAccountHistory> findAllByWebUser(WebUser webUser) {
        return WebUserAccountHistory.find("deleted = ? and account.webUser = ?" , DeletedStatus.UN_DELETED , webUser).fetch();
    }

    public static List<WebUserAccountHistory> findAllByWebUserAccount(WebUserAccount account) {
        return WebUserAccountHistory.find("deleted = ? and account = ?" , DeletedStatus.UN_DELETED , account).fetch();
    }

}
