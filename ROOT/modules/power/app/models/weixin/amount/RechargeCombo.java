package models.weixin.amount;

import enums.GoodsType;
import models.BaseModel;
import models.admin.AdminBusiness;
import models.constants.AvailableStatus;
import play.modules.paginate.JPAExtPaginator;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

/**
 * 充值套餐,比如充100送20
 * Created by youliangcheng on 17/5/11.
 */
@Entity
@Table(name = "recharge_combos")
public class RechargeCombo extends BaseModel {

    /**
     * 充值金额
     */
    @Column(name = "recharge_amount")
    public Double rechargeAmount;

    /**
     * 赠送金额
     */
    @Column(name = "give_amount")
    public Double giveAmount;


    /**
     * 套餐类型
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "goods_type")
    public GoodsType goodsType;


    /**
     * 当前状态  禁用\可用
     */
    @Column(name = "available_status")
    @Enumerated(EnumType.STRING)
    public AvailableStatus availableStatus;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;


    /**
     * 分页查询.
     */
    public static JPAExtPaginator<RechargeCombo> findByCondition(Map<String, Object> conditionMap, String orderByExpress, int pageNumber, int pageSize) {
        // 拼接sql
        StringBuilder xsqlBuilder = new StringBuilder("1=1")
                .append("/~ and t.id = {id} ~/")
                .append("/~ and t.rechargeAmount = {rechargeAmount} ~/") // 充值金额
                .append("/~ and t.availableStatus = {availableStatus} ~/"); // 启用状态


        util.xsql.XsqlBuilder.XsqlFilterResult result = new util.xsql.XsqlBuilder().generateHql(xsqlBuilder.toString(), conditionMap);
        JPAExtPaginator<RechargeCombo> orderPage = new JPAExtPaginator<>("RechargeCombo t", "t", RechargeCombo.class,
                result.getXsql(), conditionMap).orderBy(orderByExpress);
        orderPage.setPageNumber(pageNumber);
        orderPage.setPageSize(pageSize);
        orderPage.setBoundaryControlsEnabled(false);
        return orderPage;
    }

    //根据充值金额查询套餐
    public static RechargeCombo findByRechargeAmount(Double amount){
        return  RechargeCombo.find("rechargeAmount = ? and availableStatus = ?" , amount , AvailableStatus.AVAILABLE).first();
    }

    //根据分类查询分类下的所有套餐
    public static List<RechargeCombo>  loadByGoodsType(GoodsType goodsType){
        return RechargeCombo.find("goodsType = ? and availableStatus = ?", goodsType , AvailableStatus.AVAILABLE).fetch();
    }




}
