package models.weixin;

import models.constants.DeletedStatus;
import order.Order;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by LiuBin on 2016/5/13.
 */
@Entity
@Table(name = "weixin_redbao")
public class WeixinRedBao extends Model{
    /**
     * 用户下单信息
     */
    @ManyToOne
    @JoinColumn(name = "order_id")
    public Order order;

    /**
     * 关联的收到的微信用户.
     */
    @ManyToOne
    @JoinColumn(name = "share_user_id")
    public WebUser shareUser;
    /**
     * 红包金额
     */
    @Column(name = "amount")
    public Double amount;

//    /**
//     * 红包状态
//     */
//    @Enumerated(EnumType.STRING)
//    public RedbaoType type;


//    /**
//     * 红包状态
//     */
//    @Enumerated(EnumType.STRING)
//    public RedbaoStatus redbaoStatus;

    /**
     * 收入红包原因
     */
    @Column(name = "content")
    public String content;
    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;
    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;


    public WeixinRedBao() {
        super();
    }


    public WeixinRedBao(Order order , WebUser webUser , Double amount , String content) {
        this.order = order;
        this.shareUser = webUser;
        this.amount = amount;
        this.content = content;
        this.createdAt = new Date();
        this.deleted = DeletedStatus.UN_DELETED;
    }


}
