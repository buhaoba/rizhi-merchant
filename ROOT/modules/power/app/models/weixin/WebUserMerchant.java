package models.weixin;

import jodd.bean.BeanCopy;
import models.constants.DeletedStatus;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * 微信用户 设置为商户身份时 需要设置商户的信息 比如 超市名称 联系人 联系电话 授权码 等
 * Created by shancheng on 16-6-17.
 */
@Entity
@Table(name = "power_web_user_merchant")
public class WebUserMerchant extends Model {

    private static final long serialVersionUID = 28910993422185631L;

    /**
     * 微信用户名称
     */
    @JoinColumn(name = "web_user_id")
    @ManyToOne
    public WebUser webUser;

    /**
     * 联系电话
     */
    @Column(name = "phone")
    public String phone;

    /**
     * 名字
     */
    @Column(name = "name")
    public String name;

    /**
     * 商户名称
     */
    @Column(name = "merchant_name")
    public String merchantName;

    /**
     * 认证代码
     */
    @Column(name = "auto_code")
    public String autoCode;



    /**
     * 用户注册时间.
     * 用户注册时间
     */
    @Column(name = "created_at")
    public Date createAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "deleted")
    public DeletedStatus deleted = DeletedStatus.UN_DELETED;

    public WebUserMerchant() {
        super();
    }

    public WebUserMerchant(WebUser webUser , String phone , String name , String merchantName , String autoCode) {
        this.phone = phone;
        this.webUser = webUser;
        this.name = name ;
        this.merchantName = merchantName;
        this.autoCode = autoCode;
    }

    /**
     * 按weixinUser值更新指定ID的WeixinUser.
     */
    public static void update(Long id, WebUserMerchant webUserMerchant) {
        WebUserMerchant oldEntity = WebUserMerchant.findById(id);
        BeanCopy.beans(webUserMerchant, oldEntity).ignoreNulls(true).copy();
        oldEntity.save();
    }

    public static WebUserMerchant findByWebUser(WebUser webUser) {
        return WebUserMerchant.find("deleted = ? and webUser = ?" , DeletedStatus.UN_DELETED , webUser).first();
    }

}
