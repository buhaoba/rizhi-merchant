package models;

/**
 * Created by liming on 2016/2/24.
 */
public class DataResult {
    public final static int SUCCEED = 1;
    public final static int FAILURE = 2;
    public final static String ERROR_MSG_VERSION_CHANGE = "当前数据已经被更改,请刷新加载最新数据后再进行操作!";
    public int resultStatus;
    public String resultMessage;
    public Object data;

    public DataResult(){};

    public DataResult(String resultMessage){
        resultStatus = FAILURE;
        this.resultMessage = resultMessage;
    }

    public DataResult(Object data){
        resultStatus = SUCCEED;
        this.data = data;
    }


}
