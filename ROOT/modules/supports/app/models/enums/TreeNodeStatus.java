package models.enums;

/**
 * Created by liming on 16/6/22.
 */
public enum TreeNodeStatus {
    open,
    closed
}
