package models.constants;

/**
 * 记录成功 失败的枚举
 * SUCCESS 成功
 * FAIL 失败
 * ABNORMAL 异常
 * WAIT 等待处理
 */
public enum SuccessStatus {
    SUCCESS(0), FAIL(1) , ABNORMAL(2) , WAIT(3);

    private int value;

    SuccessStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
