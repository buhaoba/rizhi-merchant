package models.constants;

/**
 可用状态
 */
public enum AvailableStatus {

    AVAILABLE("启用"),
    FREEZE("禁用");



    private String code;

    private AvailableStatus(String value) {
        code = value;
    }

    @Override
    public String toString() {
        return code;
    }


}
