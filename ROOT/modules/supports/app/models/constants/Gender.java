package models.constants;

/**
 * Created by shancheng on 16-6-17.
 */
public enum Gender {
    MALE("男"),   //男
    FEMALE("女"), //女
    UNKNOWN("未知"); //未知

    private String code;

    private Gender(String value) {
        code = value;
    }

    @Override
    public String toString() {
        return code;
    }
}
