package util.list.enums;

/**
 * Created by YSC on 2016/1/29.
 */
public enum FilterType {
    EQ,
    LT,
    GT,
    LIKE,
    BETWEEN,
}
