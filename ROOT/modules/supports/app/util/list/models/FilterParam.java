package util.list.models;


import util.list.enums.FilterType;

/**
 * Created by YSC on 2016/1/29.
 */
public class FilterParam {
    public String fieldName;
    public Object value1;
    public Object value2;
    public FilterType filterType;

    public FilterParam(){}

    public FilterParam(String fieldName,Object value1,FilterType filterType){
        this.fieldName = fieldName;
        this.filterType = filterType;
        this.value1 = value1;
    }

    public FilterParam(String fieldName,Object value1,Object value2,FilterType filterType){
        this.fieldName = fieldName;
        this.filterType = filterType;
        this.value1 = value1;
        this.value2 = value2;
    }
}
