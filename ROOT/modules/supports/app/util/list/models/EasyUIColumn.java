package util.list.models;

/**
 * Created by liming on 16/6/21.
 */
public class EasyUIColumn {
    //属性名称
    public String field;
    //表头标题
    public String title;
    //宽度
    public Integer width;
    //格式化函数名称
    public String formatter;

    public EasyUIColumn() {
    }

    public EasyUIColumn(String field, String title, Integer width, String formatter) {
        this.field = field;
        this.title = title;
        this.width = width;
        this.formatter = formatter;
    }

    public EasyUIColumn(String field, String title, Integer width) {
        this.field = field;
        this.title = title;
        this.width = width;
    }
}
