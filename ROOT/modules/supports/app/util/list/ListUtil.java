package util.list;



import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang.StringEscapeUtils;
import util.common.ConvertUtil;
import util.common.DateUtil;
import util.common.UStringUtil;
import util.list.models.EasyUIFilter;
import util.list.models.FilterParam;

import java.util.Date;
import java.util.List;

/**
 * Created by liming on 2016/1/29.
 */
public class ListUtil {
    /**
     * 根据筛选条件创建筛选HQL
     * @param filterParams
     * @return
     */
    public static String initFilterHQL(FilterParam... filterParams){
        String filterHQLStr = "";
        if(filterParams!=null){
            for (FilterParam filterParam:filterParams){
                if(filterParam != null && filterParam.value1 != null){
                    switch (filterParam.filterType){
                        case EQ:
                            filterHQLStr += " AND " + StringEscapeUtils.escapeSql(filterParam.fieldName) + "= "  + initValue(filterParam.value1);
                            break;
                        case BETWEEN:
                            if(filterParam.value2 == null){
                                filterHQLStr += " AND " + StringEscapeUtils.escapeSql(filterParam.fieldName) + ">=" + initValue(filterParam.value1) ;
                            }else{
                                filterHQLStr += " AND (" + StringEscapeUtils.escapeSql(filterParam.fieldName) + ">= " + initValue(filterParam.value1) + " AND " + StringEscapeUtils.escapeSql(filterParam.fieldName) + "<="+ initValue(filterParam.value2) +")";
                            }
                            break;
                        case LIKE:
                            filterHQLStr += " AND " + StringEscapeUtils.escapeSql(filterParam.fieldName) + " LIKE '%" + StringEscapeUtils.escapeSql(filterParam.value1.toString()) + "%'";
                            break;
                        case LT:
                            filterHQLStr += " AND " + StringEscapeUtils.escapeSql(filterParam.fieldName) + ">= " + initValue(filterParam.value1);
                            break;
                        case GT:
                            filterHQLStr += " AND " + StringEscapeUtils.escapeSql(filterParam.fieldName) + "<= " + initValue(filterParam.value1);
                            break;
                    }
                }
            }
            if(filterHQLStr.length() > 0)
                filterHQLStr = filterHQLStr.substring(4);
        }
        return filterHQLStr;
    }

    /**
     * 根据筛选条件创建筛选HQL
     * @param filterParams
     * @return
     */
    public static String initFilterHQL(EasyUIFilter... filterParams){
        String filterHQLStr = "";
        if(filterParams!=null){
            for (EasyUIFilter filterParam:filterParams){
                if(filterParam != null && filterParam.value != null){
                    switch (filterParam.op){
                        case "contains":
                            filterHQLStr += " AND " + filterParam.field + " LIKE '%" + StringEscapeUtils.escapeSql(filterParam.value.toString()) + "%'";
                            break;
                    }
                }
            }
            if(filterHQLStr.length() > 0)
                filterHQLStr = filterHQLStr.substring(4);
        }
        return filterHQLStr;
    }

    //根据数据类型处理数据
    public static String initValue(Object value){
        String valueStr = "";

        String classType = value.getClass().getName();
        if("java.lang.String".equals(classType)){
            valueStr = "'" + StringEscapeUtils.escapeSql(ConvertUtil.toString(value)) + "'";
        }else if("java.util.Date".equals(classType)){
            valueStr = "'" + DateUtil.dateToString((Date)value,"yyyy-MM-dd") + "'";
        }else {
            valueStr = value.toString();
        }
        return valueStr;
    }

    /**
     * 只拼装查询条件
     * @param baseWhere
     * @param filterParams
     * @return
     */
    public static String initWhereStr(String baseWhere,FilterParam[] filterParams){
        return initWhereOrderStr(baseWhere,filterParams,null,null,null);
    }

    /**
     * 重写一个匹配List的方法
     * @param baseWhere
     * @param filterParams
     * @return
     */
    public static String initWhereStr(String baseWhere,List<FilterParam> filterParams){
        FilterParam[] filterParamsArray = filterParams.toArray(new FilterParam[filterParams.size()]);
        return initWhereStr(baseWhere,filterParamsArray);
    }

    /**
     * 组装列表页查询、排序条件
     * @param baseWhere
     * @param filterParams
     * @param baseOrder
     * @param sort
     * @param order
     * @return
     */
    public static String initWhereOrderStr(String baseWhere,FilterParam[] filterParams,String baseOrder,String sort,String order){
        String resultStr = "";
        String filterStr = initFilterHQL(filterParams);
        baseWhere = UStringUtil.concatStr(" AND ",baseWhere,filterStr);
        baseOrder = UStringUtil.concatStr(",",UStringUtil.concatStr(" ",sort,order),baseOrder);
        resultStr = baseWhere + (baseOrder.length()>0?" ORDER BY ":"") + baseOrder;

        return resultStr;
    }

    /**
     * EasyUI版本组装列表页查询、排序条件
     * @param baseWhere
     * @param filterRules
     * @param baseOrder
     * @param sort
     * @param order
     * @return
     */
    public static String initWhereOrderStr(String baseWhere,String filterRules,String baseOrder,String sort,String order){
        String resultStr = "";
        String filterStr = "";
        Gson gson = new Gson();
        EasyUIFilter[] easyUIFilters = gson.fromJson(filterRules,new TypeToken<EasyUIFilter[]>(){}.getType());
        if(easyUIFilters != null )
            filterStr = initFilterHQL(easyUIFilters);
        baseWhere = UStringUtil.concatStr(" AND ",baseWhere,filterStr);
        baseOrder = UStringUtil.concatStr(",",UStringUtil.concatStr(" ",sort,order),baseOrder);
        resultStr = baseWhere + (baseOrder.length()>0?" ORDER BY ":"") + baseOrder;

        return resultStr;
    }

    /**
     * EasyUI版本只拼装查询条件
     * @param baseWhere
     * @param filterRules
     * @return
     */
    public static String initWhereStr(String baseWhere,String filterRules){
        return initWhereOrderStr(baseWhere,filterRules,null,null,null);
    }

    /**
     * 重写一个匹配List类型的方法
     * @param baseWhere
     * @param filterParams
     * @param baseOrder
     * @param sort
     * @param order
     * @return
     */
    public static String initWhereOrderStr(String baseWhere, List<FilterParam> filterParams, String baseOrder, String sort, String order){
        FilterParam[] filterParamsArray = filterParams.toArray(new FilterParam[filterParams.size()]);
        return initWhereOrderStr(baseWhere,filterParamsArray,baseOrder,sort,order);
    }

}
