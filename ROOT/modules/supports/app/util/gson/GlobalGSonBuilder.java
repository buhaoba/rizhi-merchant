package util.gson;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;

/**
 * 单例GSon
 */
public class GlobalGSonBuilder {

    public static final Gson INSTANCE = new Gson();

    public static final Gson CUSTOM_INSTANCE = new GsonBuilder()
            .setDateFormat(DateFormat.LONG)
            .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    //.setPrettyPrinting()
            .create();
}
