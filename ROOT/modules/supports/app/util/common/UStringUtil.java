package util.common;

import java.util.List;

/**
 * Created by liming on 16/6/16.
 */
public class UStringUtil {

    //拼接字符串
    public static String concatStr(String splitStr,String... strArray){
        String concatStr = "";
        if(strArray!=null && strArray.length>0){
            splitStr = ConvertUtil.toString(splitStr);
            for(int i=0;i<strArray.length;i++){
                String str = strArray[i];
                if(str!=null &&str.length()>0)
                    concatStr += splitStr + str;
            }
        }
        return concatStr.length()>0?concatStr.substring(splitStr.length()):"";
    }

    //将对象类型拼接成字符串
    public static String concatStr(String splitStr,Integer... objArray){
        String concatStr = "";
        if(objArray!=null && objArray.length>0){
            splitStr = ConvertUtil.toString(splitStr);
            for(int i=0;i<objArray.length;i++){
                String str = ConvertUtil.toString(objArray[i]);
                if(str!=null &&str.length()>0)
                    concatStr += splitStr + str;
            }
        }
        return concatStr.length()>0?concatStr.substring(splitStr.length()):"";
    }

    //将对象类型拼接成字符串
    public static String concatStr(String splitStr,List<Integer> objArray){
        String concatStr = "";
        if(objArray!=null && objArray.size()>0){
            splitStr = ConvertUtil.toString(splitStr);
            for(int i=0;i<objArray.size();i++){
                String str = ConvertUtil.toString(objArray.get(i));
                if(str!=null &&str.length()>0)
                    concatStr += splitStr + str;
            }
        }
        return concatStr.length()>0?concatStr.substring(splitStr.length()):"";
    }
    //将对象类型拼接成字符串
    public static String concatStr(String splitStr,Long[] objArray){
        String concatStr = "";
        if(objArray!=null && objArray.length>0){
            splitStr = ConvertUtil.toString(splitStr);
            for(int i=0;i<objArray.length;i++){
                String str = ConvertUtil.toString(objArray[i]);
                if(str!=null &&str.length()>0)
                    concatStr += splitStr + str;
            }
        }
        return concatStr.length()>0?concatStr.substring(splitStr.length()):"";
    }

}
