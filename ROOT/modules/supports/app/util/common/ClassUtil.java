package util.common;

import org.apache.commons.beanutils.ConvertUtils;

import java.lang.reflect.Field;

/**
 * Created by liming on 16/7/14.
 */
public class ClassUtil {
    public static void setFiledValue(Object o, String fieldName, Object value) {
        Field f = null;
        try {
            f = o.getClass().getDeclaredField(fieldName);
        } catch (NoSuchFieldException localNoSuchFieldException) {
        }
        if (f == null) {
            return;
        }
        f.setAccessible(true);
        try {
            f.set(o, ConvertUtils.convert(value, f.getType()));
        } catch (IllegalAccessException localIllegalAccessException) {
        }
    }

    public static Object getFiledValue(Object o, String fieldName) {
        Field f = null;
        try {
            f = o.getClass().getDeclaredField(fieldName);
        } catch (NoSuchFieldException localNoSuchFieldException) {
        }
        if (f == null) {
            return null;
        }
        f.setAccessible(true);
        try {
            return f.get(o);
        } catch (IllegalAccessException localIllegalAccessException) {
        }
        return null;
    }
}
