package util.mobile;

public class MinaResultConst implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	public static final int SUCCEED = 1;
	public static final int FAILURE = 2;
	public static final int CANCEL = 3;
}
