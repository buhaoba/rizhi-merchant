package models.msgtemp;

/**
 * Created by LiuBin on 2017/2/24.
 */
public class WxMsgTempUtil {













//    private static void sendTempTest() {
//        WebUser webUser = WebUser.findById(1l);
//        WxMpService wxMpService = WxMpHelper.getWxMpService();
//        WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
//                .toUser(webUser.openId)
//                .templateId("gxxid-84bNfJGZRYCEvKERu1fu-ZYolxJYM46qyF300")
//                .build();
//        templateMessage.getData().add(new WxMpTemplateData("first", "商户端发送模板消息测试", "#dbdbdb"));
//        templateMessage.getData().add(new WxMpTemplateData("keyword1", "当前系统", "#dbdbdb"));
//        templateMessage.getData().add(new WxMpTemplateData("keyword2", DateUtil.dateToString(new Date() , "yyyy-MM-dd HH:mm:ss"), "#dbdbdb"));
//        templateMessage.getData().add(new WxMpTemplateData("remark", "商户端测试成功", "#dbdbdb"));
//        try {
//            wxMpService.getTemplateMsgService().sendTemplateMsg(templateMessage);
//        } catch (WxErrorException e) {
//            Logger.info("模板消息发送失败： %s" , e.getMessage());
//            e.printStackTrace();
//        }
//    }







//    /**
//     * 通知用户商户是否注册成功
//     * @param merchant
//     * @return
//     */
//    public static Boolean sendMsgforAuditMerchant(Merchant merchant ){
//        Boolean sendSuccess = false ;
//
//        //判断商户用户是否存在
//        if(merchant == null || merchant.webUserId ==null ){
//            Logger.info("------------------商户用户不存在,无法发送模板消息!");
//
//            sendSuccess = false;
//            return  sendSuccess ;
//
//        }
//        String auditResult = merchant.verifyState == VerifyState.PASS ? "审核通过" : "审核不通过" ;
//        //判断用户openId是否存在
//        WebUser webUser = WebUser.findByUserId(merchant.webUserId);
//        if(webUser == null || webUser.openId == null){
//            Logger.info("------------------用户openID不存在,无法发送模板消息!");
//
//            sendSuccess = false;
//            return  sendSuccess ;
//        }
//        String openId = webUser.openId;
//
//
//
//        WxMpService wxMpService = getWxMpService(getWxMpConfigStorage());
//        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
//        wxMpTemplateMessage.setToUser(openId);
////            wxMpTemplateMessage.setTemplateId("Q4i_69e9KEI649LeyTPn2AxE8f3vCsL327DRdnjmI-Y");
//        wxMpTemplateMessage.setTemplateId("-NyepQmqGd2akUbzeXHXo-HHOpXpRSrnHFdZJVbpc9s");
//        String weixinUrl = Play.configuration.getProperty("weixin.merchant.url");
//        wxMpTemplateMessage.setUrl(weixinUrl);
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", "您好！您新建的商户"+auditResult+"！" ));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1",merchant.name));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2",auditResult));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3", DateUtil.dateToString(new Date() , "yyyy-MM-dd")));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark", "内容修改：如有疑问欢迎拨打热线15263399860"));
//        try {
//            wxMpService.templateSend(wxMpTemplateMessage);
//            sendSuccess = true;
//        } catch (WxErrorException e) {
//            Logger.info("weixinCode : %s 发送模板消息失败 . 错误原因 : %s", openId, e.getMessage());
//        }
//     return sendSuccess;
//    }


//    /**
//     * 商户冻结通知
//     * @param merchant
//     * @return
//     */
//    public static Boolean sendMsgforFreezingMerchant(Merchant merchant ){
//        Boolean sendSuccess = false ;
//
//        //判断商户用户是否存在
//        if(merchant == null || merchant.webUserId ==null ){
//            Logger.info("------------------商户用户不存在,无法发送模板消息!");
//            sendSuccess = false;
//            return  sendSuccess ;
//
//        }
//        //判断用户openId是否存在
//        WebUser webUser = WebUser.findByUserId(merchant.webUserId);
//        if(webUser == null || webUser.openId == null){
//            Logger.info("------------------用户openId不存在,无法发送模板消息!");
//
//            sendSuccess = false;
//            return  sendSuccess ;
//        }
//        String openId = webUser.openId;
//
//        WxMpService wxMpService = getWxMpService(getWxMpConfigStorage());
//        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
//        wxMpTemplateMessage.setToUser(openId);
////            wxMpTemplateMessage.setTemplateId("Q4i_69e9KEI649LeyTPn2AxE8f3vCsL327DRdnjmI-Y");
//        wxMpTemplateMessage.setTemplateId("XV-BxxmBD8KHPz9zWPq08xQ-s_3A_t7c_zNRG6otisQ");
//        String weixinUrl = Play.configuration.getProperty("weixin.merchant.url");
//        wxMpTemplateMessage.setUrl(weixinUrl);
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", "您好！您的商户被冻结!" ));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1",merchant.name));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2","商铺违规"));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3", DateUtil.dateToString(new Date() , "yyyy-MM-dd") ));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark", "如有疑问请咨询客服 15263399860."));
//        try {
//            wxMpService.templateSend(wxMpTemplateMessage);
//            sendSuccess = true;
//        } catch (WxErrorException e) {
//            Logger.info("weixinCode : %s 发送模板消息失败 . 错误原因 : %s", openId, e.getMessage());
//        }
//
//    return sendSuccess;
//
//    }




//    /**
//     * 总后台，商户端同意退款：商户投诉
//     * 投诉结果,发送给用户
//     *
//     * @param webUser
//     * @param report
//     * @return
//     */
//    public static Boolean sendMsgforReportToUser(WebUser webUser, Report report) {
//        Boolean sendSuccess = false;
//
//        //判断商户用户是否存在
//        if (report == null || report.trial == null) {
//            Logger.info("------------------投诉结果没有数据,无法发送消息!");
//            sendSuccess = false;
//            return sendSuccess;
//
//        }
//
//        if (report.order == null || report.order.webUser == null) {
//            Logger.info("------------------投诉人信息获取不到,模板消息发送失败!");
//            sendSuccess = false;
//            return sendSuccess;
//        }
//        String resultType = report.trial == TrialType.MERCHANT ? "投诉不成立" : "投诉成立";
//        String resultMsg = report.refund == null ? "," : (report.refund == RefundStatus.ADMIN_AGREES ? " ,管理员同意退款" : " ,卖家同意退款");
//        //判断用户openId是否存在
//        if (webUser == null || webUser.openId == null) {
//            Logger.info("------------------用户openId不存在,无法发送模板消息!");
//
//            sendSuccess = false;
//            return sendSuccess;
//        }
//        String openId = webUser.openId;
//
//        WxMpService wxMpService = getWxMpService(getWxMpConfigStorage());
//        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
//        wxMpTemplateMessage.setToUser(openId);
////            wxMpTemplateMessage.setTemplateId("Q4i_69e9KEI649LeyTPn2AxE8f3vCsL327DRdnjmI-Y");
//        wxMpTemplateMessage.setTemplateId("NG32XEgurQOFBN4j3WZj3AgQ33KYN2bguIkVHKALRDY");
//        String weixinUrl = Play.configuration.getProperty("weixin.merchant.url");
//        wxMpTemplateMessage.setUrl(weixinUrl);
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", "您好！您投诉的问题已处理," + resultType + resultMsg));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1", report.order.webUser.nickName));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2", ""));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3", report.orderItem.goods.name));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword4", report.order.orderNumber));
////        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword3", adminUser.phoneNo!=null ? adminUser.phoneNo:"" ));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark", "如有疑问请咨询客服.15263399860"));
//        try {
//            wxMpService.templateSend(wxMpTemplateMessage);
//            sendSuccess = true;
//        } catch (WxErrorException e) {
//            Logger.info("weixinCode : %s 发送模板消息失败 . 错误原因 : %s", openId, e.getMessage());
//        }
//
//        return sendSuccess;
//
//    }






//    /**
//     * 支付成功后，商户推送信息
//     * @param order
//     * @return
//     */
//    public static Boolean sendMsgforPaySuccessMerchant(Order order){
//        Boolean sendSuccess = false ;
//        List<Map<String,Object>> merchantlist = OrderItem.getListForWeiXinMsgByOrderId(order);
//        for (Map<String,Object> merchant : merchantlist) {
//            Set set = merchant.keySet();
//            Iterator it = set.iterator();
//            while (it.hasNext()) {
//                Object key = it.next();
//                Object value = merchant.get(key);
//                if(value != null){
//                    WebUser webUser = WebUser.findByUserId(Long.parseLong(value.toString()));
//                    String openId = webUser.openId;
//                    WxMpService wxMpService = getWxMpService(getWxMpConfigStorage());
//                    WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
//                    wxMpTemplateMessage.setToUser(openId);
////            wxMpTemplateMessage.setTemplateId("Q4i_69e9KEI649LeyTPn2AxE8f3vCsL327DRdnjmI-Y");
//                    wxMpTemplateMessage.setTemplateId("");
//                    String weixinUrl = Play.configuration.getProperty("weixin.merchant.url");
//                    wxMpTemplateMessage.setUrl(weixinUrl);
//                    wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", "您好！有新订单!" ));
////        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword3", adminUser.phoneNo!=null ? adminUser.phoneNo:"" ));
//                    wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark", "如有疑问请咨询客服."));
//                    try {
//                        wxMpService.templateSend(wxMpTemplateMessage);
//                        sendSuccess = true;
//                    } catch (WxErrorException e) {
//                        Logger.info("weixinCode : %s 发送模板消息失败 . 错误原因 : %s", openId, e.getMessage());
//                    }
//                }
//
//            }
//        }
//
//
//        return sendSuccess;
//
//    }

//    /**
//     * 产品冻结通知
//     * @param product
//     * @return
//     */
//    public static Boolean sendMsgforFreezingProduct(Product product ){
//        Boolean sendSuccess = false ;
//
//        //判断商户用户是否存在
//        if(product.merchant == null || product.merchant.webUserId ==null ){
//            Logger.info("------------------商户用户不存在,无法发送模板消息!");
//            sendSuccess = false;
//            return  sendSuccess ;
//
//        }
//        //判断用户openId是否存在
//        WebUser webUser = WebUser.findByUserId(product.merchant.webUserId);
//        if(webUser == null || webUser.openId == null){
//            Logger.info("------------------用户openId不存在,无法发送模板消息!");
//            sendSuccess = false;
//            return  sendSuccess ;
//        }
//        String openId = webUser.openId;
//
//        WxMpService wxMpService = getWxMpService(getWxMpConfigStorage());
//        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
//        wxMpTemplateMessage.setToUser(openId);
////            wxMpTemplateMessage.setTemplateId("Q4i_69e9KEI649LeyTPn2AxE8f3vCsL327DRdnjmI-Y");
//        wxMpTemplateMessage.setTemplateId("tPqlAgpRoELHnawEVeHe2IuBJmPLFHNO31jqR_Nnro4");
//        String weixinUrl = Play.configuration.getProperty("weixin.merchant.url");
//        wxMpTemplateMessage.setUrl(weixinUrl);
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", "您好！您的产品已在前台下线!" ));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1",product.name));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2", DateUtil.dateToString(new Date() , "yyyy-MM-dd HH:mm") ));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3","退货率高"));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark", "感谢您的使用"));
//        try {
//            wxMpService.templateSend(wxMpTemplateMessage);
//            sendSuccess = true;
//        } catch (WxErrorException e) {
//            Logger.info("weixinCode : %s 发送模板消息失败 . 错误原因 : %s", openId, e.getMessage());
//        }
//
//        return sendSuccess;
//
//    }


}
