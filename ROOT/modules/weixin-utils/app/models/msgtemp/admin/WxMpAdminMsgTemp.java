package models.msgtemp.admin;

import enums.RefundStatus;
import enums.TrialType;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import models.helper.WxMpHelper;
import models.merchant.Merchant;
import models.weixin.WebUser;
import order.Report;
import order.ReportRemark;
import play.Logger;
import play.Play;
import product.Product;
import utils.DateUtil;

import java.util.Date;

/**
 * Created by Administrator on 2017/12/16.
 */
public class WxMpAdminMsgTemp {
    /**
     * 总后台，商户端同意退款：商户投诉
     * 投诉结果,发送给用户
     *
     * @param webUser
     * @param report
     * @return
     */
    public static Boolean sendMsgforReportToUser(WebUser webUser, Report report) {
        Boolean sendSuccess = false;

        //判断商户用户是否存在
        if (report == null || report.trial == null) {
            Logger.info("------------------投诉结果没有数据,无法发送消息!");
            sendSuccess = false;
            return sendSuccess;

        }

        if (report.order == null || report.order.webUser == null) {
            Logger.info("------------------投诉人信息获取不到,模板消息发送失败!");
            sendSuccess = false;
            return sendSuccess;
        }
        String resultType = report.trial == TrialType.MERCHANT ? "投诉不成立" : "投诉成立";
        String resultMsg = report.refund == null ? "," : (report.refund == RefundStatus.ADMIN_AGREES ? " ,管理员同意退款" : " ,卖家同意退款");
        //判断用户openId是否存在
        if (webUser == null || webUser.openId == null) {
            Logger.info("------------------用户openId不存在,无法发送模板消息!");

            sendSuccess = false;
            return sendSuccess;
        }
        String openId = webUser.openId;

        WxMpService wxMpService = WxMpHelper.getWxMpService();

        WxMpTemplateMessage wxMpTemplateMessage = WxMpTemplateMessage.builder()
                .toUser(openId)
                .templateId("NG32XEgurQOFBN4j3WZj3AgQ33KYN2bguIkVHKALRDY")
                .build();

        String weixinUrl = Play.configuration.getProperty("weixin.merchant.url");
        wxMpTemplateMessage.setUrl(weixinUrl);
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("first", "您好！您投诉的问题已处理," + resultType + resultMsg));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword1", report.order.webUser.nickName));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword2", ""));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword3", report.orderItem.goods.name));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword4", report.order.orderNumber));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("remark", "如有疑问请咨询客服.15263399860"));
        try {
            wxMpService.getTemplateMsgService().sendTemplateMsg(wxMpTemplateMessage);
            sendSuccess = true;
        } catch (WxErrorException e) {
            Logger.info("weixinCode : %s 发送模板消息失败 . 错误原因 : %s", openId, e.getMessage());
        }

        return sendSuccess;

    }

    /**
     * 总后台，商户端同意退款：商户投诉
     * 投诉结果,发送给商户
     *
     * @param webUser
     * @param report
     * @return
     */
    public static Boolean sendMsgforReportToMerchant(WebUser webUser, Report report, ReportRemark reportRemark) {
        Boolean sendSuccess = false;

        if (report.order == null || report.order.webUser == null) {
            Logger.info("------------------投诉人信息获取不到,模板消息发送失败!");
            sendSuccess = false;
            return sendSuccess;
        }

        //判断商户用户是否存在
        if (report == null || report.trial == null) {
            Logger.info("------------------投诉结果没有数据,无法发送消息!");
            sendSuccess = false;
            return sendSuccess;

        }

        //判断用户openId是否存在
        if (webUser == null || webUser.openId == null) {
            Logger.info("------------------用户openId不存在,无法发送模板消息!");

            sendSuccess = false;
            return sendSuccess;
        }
        String resultType = report.trial == TrialType.MERCHANT ? "投诉不成立" : "投诉成立";
        String resultMsg = report.refund == null ? "," : (report.refund == RefundStatus.ADMIN_AGREES ? " ,管理员同意退款" : " ,卖家同意退款");
        String openId = webUser.openId;

        WxMpService wxMpService = WxMpHelper.getWxMpService();

        WxMpTemplateMessage wxMpTemplateMessage = WxMpTemplateMessage.builder()
                .toUser(openId)
                .templateId("IkXZ9ryCzNHCzM9zpKBsNS_kA4oACXqzkWLogYfmVrY")
                .build();

        String weixinUrl = Play.configuration.getProperty("weixin.merchant.url");
        wxMpTemplateMessage.setUrl(weixinUrl);
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("first", "投诉处理结果"));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword1", report.order.webUser.nickName));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword2", report.merchant.name));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword3", report.order.orderNumber));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword4", reportRemark.reportRemarks));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword5", resultType + resultMsg));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("remark", "如有疑问请咨询客服.15263399860"));
        try {
            wxMpService.getTemplateMsgService().sendTemplateMsg(wxMpTemplateMessage);
            sendSuccess = true;
        } catch (WxErrorException e) {
            Logger.info("weixinCode : %s 发送模板消息失败 . 错误原因 : %s", openId, e.getMessage());
        }

        return sendSuccess;

    }

    /**
     * 商户冻结通知
     * @param merchant
     * @return
     */
    public static Boolean sendMsgforFreezingMerchant(Merchant merchant ){
        Boolean sendSuccess = false ;

        //判断商户用户是否存在
        if(merchant == null || merchant.webUserId ==null ){
            Logger.info("------------------商户用户不存在,无法发送模板消息!");
            sendSuccess = false;
            return  sendSuccess ;

        }
        //判断用户openId是否存在
        WebUser webUser = WebUser.findByUserId(merchant.webUserId);
        if(webUser == null || webUser.openId == null){
            Logger.info("------------------用户openId不存在,无法发送模板消息!");

            sendSuccess = false;
            return  sendSuccess ;
        }
        String openId = webUser.openId;


        WxMpService wxMpService = WxMpHelper.getWxMpService();

        WxMpTemplateMessage wxMpTemplateMessage = WxMpTemplateMessage.builder()
                .toUser(openId)
                .templateId("XV-BxxmBD8KHPz9zWPq08xQ-s_3A_t7c_zNRG6otisQ")
                .build();
//        wxMpTemplateMessage.setTemplateId("XV-BxxmBD8KHPz9zWPq08xQ-s_3A_t7c_zNRG6otisQ");
        String weixinUrl = Play.configuration.getProperty("weixin.merchant.url");
        wxMpTemplateMessage.setUrl(weixinUrl);
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("first", "您好！您的商户被冻结!" ));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword1",merchant.name));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword2","商铺违规"));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword3", DateUtil.dateToString(new Date() , "yyyy-MM-dd") ));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("remark", "如有疑问请咨询客服 15263399860."));
        try {
            wxMpService.getTemplateMsgService().sendTemplateMsg(wxMpTemplateMessage);
            sendSuccess = true;
        } catch (WxErrorException e) {
            Logger.info("weixinCode : %s 发送模板消息失败 . 错误原因 : %s", openId, e.getMessage());
        }

        return sendSuccess;

    }



    /**
     * 产品冻结通知
     * @param product
     * @return
     */
    public static Boolean sendMsgforFreezingProduct(Product product ){
        Boolean sendSuccess = false ;

        //判断商户用户是否存在
        if(product.merchant == null || product.merchant.webUserId ==null ){
            Logger.info("------------------商户用户不存在,无法发送模板消息!");
            sendSuccess = false;
            return  sendSuccess ;

        }
        //判断用户openId是否存在
        WebUser webUser = WebUser.findByUserId(product.merchant.webUserId);
        if(webUser == null || webUser.openId == null){
            Logger.info("------------------用户openId不存在,无法发送模板消息!");
            sendSuccess = false;
            return  sendSuccess ;
        }
        String openId = webUser.openId;

        WxMpService wxMpService = WxMpHelper.getWxMpService();

        WxMpTemplateMessage wxMpTemplateMessage = WxMpTemplateMessage.builder()
                .toUser(openId)
                .templateId("tPqlAgpRoELHnawEVeHe2IuBJmPLFHNO31jqR_Nnro4")
                .build();

        String weixinUrl = Play.configuration.getProperty("weixin.merchant.url");
        wxMpTemplateMessage.setUrl(weixinUrl);
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("first", "您好！您的产品已在前台下线!" ));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword1",product.name));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword2", DateUtil.dateToString(new Date() , "yyyy-MM-dd HH:mm") ));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword3","退货率高"));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("remark", "感谢您的使用"));
        try {
            wxMpService.getTemplateMsgService().sendTemplateMsg(wxMpTemplateMessage);
            sendSuccess = true;
        } catch (WxErrorException e) {
            Logger.info("weixinCode : %s 发送模板消息失败 . 错误原因 : %s", openId, e.getMessage());
        }

        return sendSuccess;

    }


    /**
     * 商户审核不通过
     * @param merchant
     * @return
     */
    public static Boolean sendMsgforNotPassMerchant(Merchant merchant ){
        Boolean sendSuccess = false ;

        //判断商户用户是否存在
        if(merchant == null || merchant.webUserId ==null ){
            Logger.info("------------------商户用户不存在,无法发送模板消息!");
            sendSuccess = false;
            return  sendSuccess ;

        }
        //判断用户openId是否存在
        WebUser webUser = WebUser.findByUserId(merchant.webUserId);
        if(webUser == null || webUser.openId == null){
            Logger.info("------------------用户openId不存在,无法发送模板消息!");

            sendSuccess = false;
            return  sendSuccess ;
        }
        String openId = webUser.openId;


        WxMpService wxMpService = WxMpHelper.getWxMpService();

        WxMpTemplateMessage wxMpTemplateMessage = WxMpTemplateMessage.builder()
                .toUser(openId)
                .templateId("XobJtN6NHFitq4tRIbA0sbJPPUGtcEOm2TN6Tum_lZA")
                .build();
//        wxMpTemplateMessage.setTemplateId("XV-BxxmBD8KHPz9zWPq08xQ-s_3A_t7c_zNRG6otisQ");
        String weixinUrl = Play.configuration.getProperty("weixin.merchant.url");
        wxMpTemplateMessage.setUrl(weixinUrl);
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("first", "您好！您的商户审核未通过!" ));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword1","拒绝"));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword2","您的综合评分不足!"));
        wxMpTemplateMessage.getData().add(new WxMpTemplateData("remark", "感谢您的支持"));
        try {
            wxMpService.getTemplateMsgService().sendTemplateMsg(wxMpTemplateMessage);
            sendSuccess = true;
        } catch (WxErrorException e) {
            Logger.info("weixinCode : %s 发送模板消息失败 . 错误原因 : %s", openId, e.getMessage());
        }

        return sendSuccess;

    }

}
