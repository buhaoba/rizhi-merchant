package models.pay;

import com.github.binarywang.wxpay.bean.request.WxEntPayRequest;
import helper.GlobalConfig;
import models.weixin.WebUser;
import order.Order;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import java.util.Date;

public class SimulationPayData {

    public static WxEntPayRequest wxEntPayInfo(WebUser webUser) {
        WxEntPayRequest wxEntPayRequest = new WxEntPayRequest();
        wxEntPayRequest.setAppid(webUser.weixinData.appId);
        wxEntPayRequest.setMchId(webUser.weixinData.mchId);
        wxEntPayRequest.setNonceStr(Sha1Util.getNonceStr());
        wxEntPayRequest.setPartnerTradeNo(DateFormatUtils.format(new Date(), "yyMMdd") + RandomStringUtils.randomNumeric(3));
        wxEntPayRequest.setOpenid(webUser.openId);
        wxEntPayRequest.setCheckName("NO_CHECK");
        wxEntPayRequest.setAmount(100);
        wxEntPayRequest.setDescription("测试企业向个人付款");
        wxEntPayRequest.setSpbillCreateIp(GlobalConfig.SPBILL_CREATE_IP);
        return wxEntPayRequest;
    }

    public static Order createOrder(WebUser webUser) {
        Order order  = new Order();
        order.orderNumber = DateFormatUtils.format(new Date(), "yyMMdd") + RandomStringUtils.randomNumeric(9);
        order.webUser = webUser;
        order.amount = 0.01d;
        order.remark= "单笔下单 测试";
        return order;
    }

}
