package models.pay;

import bill.WithdrawalsBill;
import bill.WithdrawalsUserBill;
import com.github.binarywang.wxpay.bean.request.WxEntPayRequest;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.google.gson.Gson;
import helper.GlobalConfig;
import models.weixin.WebUser;
import models.weixin.WeixinData;
import order.Order;
import play.Logger;
import utils.DateUtil;

import java.util.Date;

public class WeixinPayHelper {

    /**
     * 根据订单 和 用户Ip 来生成 WxPayUnifiedOrderRequest
     * @param order
     * @param webUserIp
     * @return
     */
    public static WxPayUnifiedOrderRequest prepayInfo(Order order , String webUserIp) {
        Logger.info("Order : %s" , new Gson().toJson(order));
        Double payAmount = order.amount * 100;
        WxPayUnifiedOrderRequest prepayInfo = WxPayUnifiedOrderRequest.newBuilder()
                .openid(order.webUser.openId)
                .outTradeNo(order.orderNumber)
                .totalFee(payAmount.intValue())
                .body(order.orderNumber)
                .tradeType("JSAPI")
                .spbillCreateIp(webUserIp)
                .notifyURL(GlobalConfig.WEIXIN_BASE_DOMAIN + GlobalConfig.WEIXIN_PAY_BACK_URL)
                .build();
        Logger.info("Log 100011 prepayInfo : %s" , prepayInfo);
        return prepayInfo;
    }

    /**
     * 企业提现 返回 WxEntPayRequest
     * @param withdrawalsBill
     * @return
     */
    public static WxEntPayRequest entpayInfo(WithdrawalsBill withdrawalsBill) {
        WeixinData weixinDate = WeixinData.findById(GlobalConfig.WEIXIN_DATE_ID);
        WebUser webUser = WebUser.findById(withdrawalsBill.merchant.webUserId);
        WxEntPayRequest wxEntPayRequest = new WxEntPayRequest();
        wxEntPayRequest.setAppid(weixinDate.appId);
        wxEntPayRequest.setMchId(weixinDate.mchId);
        wxEntPayRequest.setNonceStr(Sha1Util.getNonceStr());
        wxEntPayRequest.setPartnerTradeNo(withdrawalsBill.billCode);
        wxEntPayRequest.setOpenid(webUser.openId);
        wxEntPayRequest.setCheckName("NO_CHECK");
        wxEntPayRequest.setAmount(withdrawalsBill.amount * 100);
        wxEntPayRequest.setDescription(withdrawalsBill.merchant.name + "于" + DateUtil.dateToString(new Date() , "yyyy-MM-dd HH:mm:ss") + "提现" +  withdrawalsBill.amount +"元");
        wxEntPayRequest.setSpbillCreateIp(GlobalConfig.SPBILL_CREATE_IP);
        return wxEntPayRequest;
    }


    /**
     * 企业提现 返回 WxEntPayRequest
     * @param withdrawalsBill
     * @return
     */
    public static WxEntPayRequest entpayInfoByUser(WithdrawalsUserBill withdrawalsBill) {
        WeixinData weixinDate = WeixinData.findById(GlobalConfig.WEIXIN_DATE_ID);
        WebUser webUser = WebUser.findById(withdrawalsBill.webUser.id);
        WxEntPayRequest wxEntPayRequest = new WxEntPayRequest();
        wxEntPayRequest.setAppid(weixinDate.appId);
        wxEntPayRequest.setMchId(weixinDate.mchId);
        wxEntPayRequest.setNonceStr(Sha1Util.getNonceStr());
        wxEntPayRequest.setPartnerTradeNo(withdrawalsBill.billCode);
        wxEntPayRequest.setOpenid(webUser.openId);
        wxEntPayRequest.setCheckName("NO_CHECK");
        wxEntPayRequest.setAmount(withdrawalsBill.amount * 100);
        wxEntPayRequest.setDescription(withdrawalsBill.webUser.nickName + "于" + DateUtil.dateToString(new Date() , "yyyy-MM-dd HH:mm:ss") + "提现" +  withdrawalsBill.amount +"元");
        wxEntPayRequest.setSpbillCreateIp(GlobalConfig.SPBILL_CREATE_IP);
        return wxEntPayRequest;
    }

}
