package enums;

/**
 * Created by LiuBin on 2016/5/23.
 */
public enum ReviewStatus {
    FROZEN,//冻结
    NORMAL//正常
}
