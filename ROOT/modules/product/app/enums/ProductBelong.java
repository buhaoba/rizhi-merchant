package enums;

/**
 * Created by youliangcheng on 16/10/24.
 */
public enum ProductBelong {
    SELF_SUPPORT,//自营
    AGENT_PRODUCT,//入住产品、代理产品
}
