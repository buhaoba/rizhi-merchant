package enums;

/**
 * 商品上下架状态.
 */
public enum ProductStatus {
    UP,//上架
    DOWN//下架
}
