package product;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.merchant.Merchant;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 产品限购 设置
 * Created by buhaoba on 2016/10/11.
 */
@Entity
@Table(name = "limitations")
public class Limitations extends BaseModel {

    /**
     * 产品限购名称
     */
    @Column(name = "name")
    public String name;


    /**
     * 商户信息
     */
    @JoinColumn(name = "merchant_id")
    @ManyToOne
    public Merchant merchant;


    /**
     * 产品限购数量
     */
    @Column(name = "count")
    public Integer count;


    @Transient
    public Integer statisticsCount;

    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;


    /**
     * 加载所有设置列表
     * @return
     */
    public static List<Limitations> findAvailable() {
        return Limitations.find("deleted = ?" , DeletedStatus.UN_DELETED).fetch();
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    public static Limitations findByLimitationsId(long id){
        return  Limitations.find(" id = ? and deleted = ?" , id ,DeletedStatus.UN_DELETED).first();
    }



}
