package product.type;

import enums.ReviewStatus;
import jodd.bean.BeanCopy;
import models.BaseModel;
import models.constants.AvailableStatus;
import models.constants.DeletedStatus;
import models.enums.TreeNodeStatus;
import models.merchant.Merchant;
import order.OrderItem;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import play.modules.paginate.JPAExtPaginator;
import product.Product;
import product.ProductTypes;
import util.common.ConvertUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by buhaoba on 16/6/13.
 */
@Entity
@Table(name = "product_type")
public class ProductType extends BaseModel {

    /**
     * 类别编号
     */
    @Column(name = "code")
    public String code;

    /**
     * 类别名称
     */
    @Column(name = "name")
    public String name;

    /**
     * 排序号
     */
    @Column(name = "sort")
    public Integer sort;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;



    /**
     * 商户信息
     */
    @JoinColumn(name = "merchant_id")
    @ManyToOne
    public Merchant merchant;


    /**
     * 末端节点
     */
    @Column(name = "is_leaf")
    public Boolean isLeaf;
    /**
     * 是否为叶子节点 open:叶子节点 close:非叶子节点
     */
    @Transient
    public TreeNodeStatus state;
    /**
     * 等级
     */
    @Column(name = "level")
    public Integer level;
    /**
     * 所属类别
     */
    @Column(name = "parent_type_id")
    public Long parentTypeId;

    @Transient
    public Integer isModify;

    @Transient
    public Integer isCheck;

    /**
     * 节点全路径
     */
    @Column(name = "node_id_path")
    public String nodeIdPath;

    /**
     * 排序号
     */
    @Column(name = "show_order")
    public Integer showOrder = 0;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;

    @Transient
    public Integer sumCount;

    /**
     * Update
     * @param id
     * @param newObject
     */
    public static void update(Long id, ProductType newObject) {
        ProductType productType= ProductType.findById(id);
        BeanCopy.beans(newObject, productType).ignoreNulls(true).copy();
        productType.save();
    }

    /**
     * 分页查询.
     */
    public static JPAExtPaginator<ProductType> findByCondition(Map<String, Object> conditionMap, String orderByExpress, int pageNumber, int pageSize) {
        StringBuilder xsqlBuilder = new StringBuilder("t.deleted=models.constants.DeletedStatus.UN_DELETED ")
                .append("/~ and t.name like {name} ~/");


        util.xsql.XsqlBuilder.XsqlFilterResult result = new util.xsql.XsqlBuilder().generateHql(xsqlBuilder.toString(), conditionMap);
        JPAExtPaginator<ProductType> orderPage = new JPAExtPaginator<ProductType>("ProductType t", "t", ProductType.class,
                result.getXsql(), conditionMap).orderBy(orderByExpress);
        orderPage.setPageNumber(pageNumber);
        orderPage.setPageSize(pageSize);
        orderPage.setBoundaryControlsEnabled(false);
        return orderPage;
    }

    public ProductType() {}

    public ProductType(String name) {
        this.createAt = new Date();
        this.deleted = DeletedStatus.UN_DELETED;
    }

    /**
     * 根据层级查找产品类型
     * @return
     */
    public static List<ProductType> findAllProductTypes(long merchantId){
        return find("deleted = ?1 and merchant.id = ?2 order by showOrder,id", DeletedStatus.UN_DELETED, merchantId).fetch();
    }

    /**
     * 根据层级查找产品类型
     * @return
     */
    public static List<ProductType> findProductTypeByLevel(Integer level){
        return find("deleted = ?1 and status = ?2 and level = ?3 order by showOrder,id",DeletedStatus.UN_DELETED,AvailableStatus.AVAILABLE,level).fetch();
    }

    /**
     * 查看可用 类别
     *
     * @return
     */
    public static List<ProductType> findAvailable() {
        return find("deleted = ?1 and status = ?2 order by showOrder,id", DeletedStatus.UN_DELETED, AvailableStatus.AVAILABLE).fetch();
    }

    /**
     * 根据类别查看
     *
     * @return
     */
    public static List<ProductType> findAvailable(AvailableStatus status) {
        return find("deleted = ?1 and status = ?2 order by showOrder,id", DeletedStatus.UN_DELETED, status).fetch();
    }

    /**
     * 加载产品类型树
     *
     * @return
     */
    public static List<ProductType> loadTypeTree(Long id, long productId) {
        Query query = em().createNativeQuery("SELECT distinct t.id,t.sort,t.parent_type_id as parentTypeId,t.name,case when p.id is null THEN 0 else 1 end as 'isCheck',t.level,t.is_leaf as isLeaf ,t.status,case when is_leaf = 1 then 'open' else 'closed' end as 'state'  from product_type t LEFT JOIN product_types p ON t.id = p.type_id and p.product_id = " + productId + " where t.deleted = 0 and t.status = 'AVAILABLE' and t.parent_type_id = " + id + " order by t.sort asc,id desc");
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        return query.getResultList();
    }


    /**
     * 加载可用的一级节点
     *
     * @return
     */
    public static List<ProductType> findChildNode(long id) {
        return ProductType.find(" parentTypeId = ?1  and deleted = ?2 order by showOrder,id", id,DeletedStatus.UN_DELETED).fetch();
    }

    //根据ID删除操作明细
    public static void delByIds(Long[] delItemArray) {
        if (delItemArray != null) {
            for (Long typeId : delItemArray) {
                ProductType productType = ProductType.findById(typeId);
                productType.deleted = DeletedStatus.DELETED;
                productType.save();
            }
        }
    }


    /**
     * 判断productType是否被使用
     */
    public static String checkIsUsed(Long[] delItemArray) {
        String str = "销售类型:";
        if (delItemArray != null) {
            for (long typeId : delItemArray) {
                boolean check = ProductTypes.count(" type.id=?1 and deleted=?2", typeId, DeletedStatus.UN_DELETED) > 0;
                if (check) {
                    ProductType productType = ProductType.findById(typeId);
                    str += ConvertUtil.toString(productType.name) + ",";
                }
            }
        }
        return str;
    }

    /**
     * 判断productType是否被使用
     */
    public static Boolean checkIsUsed(long typeId) {
       return Product.count("productType.id = ?1 and deleted = ?2 and reviewStatus = ?3 order by id" , typeId , DeletedStatus.UN_DELETED, ReviewStatus.NORMAL) > 0 ;
    }


    /**
     * 查询类型树
     */
    public static List<Map> loadProductTypeTree(long id) {
        String sqlSelect = "Select id,name as 'text',node_id_path,is_leaf,remark,case when is_leaf = 1 then 'open' else 'closed' end as 'state' from product_type where deleted=0 and parent_type_id =" + id;
        Query query = ProductType.em().createNativeQuery(sqlSelect);
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        return query.getResultList();
    }


    /**
     * 获取商品分类,且分类下有产品
     * @param merchatId
     * @return
     */
    public static List<Map<String , Object>> loadBeUsedType(long merchatId){
        String sqlSelect = "select * from product_type a where a.id in (" +
                "    select a.product_type_id from products a where a.deleted = 0 and a.merchant_id = "+merchatId+"  group by a.product_type_id " +
                "    )";

        Query query = ProductType.em().createNativeQuery(sqlSelect);
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        List<Map<String , Object>> resultList = query.getResultList();
        return resultList ;
    }

    public static List<ProductType> loadAllType(){
        return ProductType.find(" deleted = ?1 order by showOrder,id " ,  DeletedStatus.UN_DELETED).fetch();
    }

    /**
     * 根据Id查询分类
     * @param id
     * @return
     */
    public static ProductType findByTypeId(long id){
        return ProductType.find("deleted = ?1 and id = ?2 order by showOrder,id" , DeletedStatus.UN_DELETED , id).first();
    }

    /**
     * 查询所有分类
     * @return
     */
    public static List<ProductType> loadAll(){
        return find("deleted = ?1 order by showOrder,id", DeletedStatus.UN_DELETED).fetch();
    }

    /**
     * 加载孩子节点
     *
     * @param id
     * @return
     */
    public static List<ProductType> loadChildType(long id) {
        List<ProductType> productTypes = ProductType.find("parentTypeId = ?1 and deleted = ?2 order by showOrder,id ", id, DeletedStatus.UN_DELETED).fetch();
        for (ProductType productType : productTypes) {
            productType.state = productType.isLeaf != null && productType.isLeaf ? TreeNodeStatus.open : TreeNodeStatus.closed;
        }
        return productTypes;
    }

    public static List<ProductType> loadAllParentType(long parentTypeId){
        return ProductType.find(" deleted = ?1 and parentTypeId = ?2 order by showOrder,id " ,  DeletedStatus.UN_DELETED,parentTypeId).fetch();
    }

    /**
     * 获取商品分类,且分类下有产品
     * @param parentTypeId
     * @return
     */
    public static List<ProductType> loadBeUsedTwoType(long parentTypeId){
        String sqlSelect = "select t.name name,t.id id from product_type t where t.deleted = 0 and t.parent_type_id = "+parentTypeId;
        sqlSelect+=" order by t.showOrder,t.id";
        Query query = em().createNativeQuery(sqlSelect);
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        List<ProductType> resulList = query.getResultList();
        return  resulList;
    }
    public static ProductType loadFirParentType(long parentTypeId){
        return ProductType.find(" deleted = ?1 and parentTypeId = ?2 order by showOrder,id " ,  DeletedStatus.UN_DELETED,parentTypeId).first();
    }

    public static List<ProductType> loadChildType(){
        return ProductType.find(" deleted = ?1  and parentTypeId != 0 order by showOrder,id" ,  DeletedStatus.UN_DELETED).fetch();
    }

    public static List<ProductType> loadTypeById(long id){
        return ProductType.find(" deleted = ?1  and id = ?2 order by showOrder,id" ,  DeletedStatus.UN_DELETED,id).fetch();
    }
    public static void  update(ProductType productType){
        ProductType oldEntity =  ProductType.findById(productType.id);
        BeanCopy.beans(productType,oldEntity).ignoreNulls(true).copy();
        productType.save();
    }


    /**
     *  商品大类销售统计
     */
    public static List<ProductType> getSaleProdcutType(){
        StringBuilder sb = new StringBuilder(" SELECT pt.name name,( SELECT count(oi.id)   FROM order_items oi LEFT JOIN goods g ON oi.goods_id = g.id ");
        sb.append(" LEFT JOIN products p ON p.id = g.serial_id WHERE pt.id = p.parent_type_id) sumCount  ");
        sb.append(" FROM product_type pt WHERE pt.parent_Type_Id = 0 ");
        Query query = OrderItem.em().createNativeQuery(sb.toString());
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        List<ProductType> resulList = query.getResultList();
        return  resulList;
    }

}
