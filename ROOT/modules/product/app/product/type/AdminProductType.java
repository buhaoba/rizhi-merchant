package product.type;

import models.BaseModel;
import models.constants.AvailableStatus;
import models.constants.DeletedStatus;
import models.enums.TreeNodeStatus;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import product.Product;
import util.common.ConvertUtil;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

/**
 * Created by liming on 16/7/13.
 */
@Entity
@Table(name = "admin_product_type")
public class AdminProductType extends BaseModel {


    /**
     * 类别编号
     */
    @Column(name = "code")
    public String code;

    /**
     * 类别名称
     */
    @Column(name = "name")
    public String name;

    /**
     * 末端节点
     */
    @Column(name = "is_leaf")
    public Boolean isLeaf;
    /**
     * 是否为叶子节点 open:叶子节点 close:非叶子节点
     */
    @Transient
    public TreeNodeStatus state;
    /**
     * 等级
     */
    @Column(name = "level")
    public Integer level;
    /**
     * 所属类别
     */
    @Column(name = "parent_type_id")
    public Long parentTypeId;

    @Transient
    public Integer isModify;

    @Transient
    public Integer isCheck;

    /**
     * 节点全路径
     */
    @Column(name = "node_id_path")
    public String nodeIdPath;

    /**
     * 排序号
     */
    @Column(name = "show_order")
    public Integer showOrder = 0;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;

    /**
     * 该标记用于系统自动创建采购计划,对添加产品进行识别
     * 产品标记 0 :添加不提取产品规格 1:添加并提取产品规格 2: 该类的产品不自动添加
     */
    @Column(name = "is_get_specs")
    public Integer isGetSpecs ;

    /**
     * 当前状态  禁用\可用
     */
    @Enumerated(EnumType.STRING)
    public AvailableStatus status;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 加载孩子节点列表
     */
    public static List<AdminProductType> loadChildNodeList(long id) {
        List<AdminProductType> adminProductTypeList = AdminProductType.find("parentTypeId=? and deleted=? order by showOrder, id desc", id, DeletedStatus.UN_DELETED).fetch();
        for (AdminProductType adminProductType : adminProductTypeList) {
            adminProductType.state = adminProductType.isLeaf != null && adminProductType.isLeaf ? TreeNodeStatus.open : TreeNodeStatus.closed;
        }
        return adminProductTypeList;
    }

    /**
     * 删除节点
     */
    public static void deleteAdminProductTypes(Long[] delIdArray) {
        if (delIdArray != null && delIdArray.length > 0) {
            for (Long typeId : delIdArray) {
                AdminProductType adminProductType = AdminProductType.findById(typeId);
                adminProductType.deleted = DeletedStatus.DELETED;
                adminProductType.save();
                if (!checkBrotherNode(adminProductType)) {
                    AdminProductType parentType = AdminProductType.findById(adminProductType.parentTypeId);
                    parentType.isLeaf = true;
                    parentType.save();
                }

            }
        }
    }

    /**
     * 是否还有兄弟节点
     */
    public static Boolean checkBrotherNode(AdminProductType adminProductType) {
        return AdminProductType.count("parentTypeId=? and deleted=?", adminProductType.parentTypeId, DeletedStatus.UN_DELETED) > 0;
    }

    /**
     *
     * 内部产品类型是否被使用
     * @param id
     * @return
     */
    public static Boolean checkIsUsed(long id) {
        return Product.count("adminProductType.id=? and deleted=?", id,DeletedStatus.UN_DELETED) > 0;
    }

    /**
     * 生成编码code
     * @param parentProType
     * @return
     */
    public static String createCode(AdminProductType parentProType) {
        Long number =AdminProductType.count("parentTypeId=?", parentProType.id);
        return number>=100 ?ConvertUtil.toString(parentProType.code)+number:(ConvertUtil.toString(parentProType.code)+(ConvertUtil.toString(number+100).substring(1)));
    }
}
