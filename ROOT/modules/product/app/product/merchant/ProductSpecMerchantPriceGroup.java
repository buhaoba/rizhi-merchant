package product.merchant;


import models.BaseModel;
import models.constants.DeletedStatus;


import javax.persistence.*;


/**
 * 商品价格组
 * Created by buhaoba on 16/6/16.
 */
@Entity
@Table(name = "product_spec_merchant_price_groups")
public class ProductSpecMerchantPriceGroup extends BaseModel {

    /**
     * 商户产品
     */
    @JoinColumn(name = "product_spec_merchant_id")
    @ManyToOne
    public ProductSpecMerchant productSpecMerchant;

    /**
     * 销售金额
     */
    @Column(name = "price")
    public Double price = 0D;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark ;

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public static ProductSpecMerchantPriceGroup findByGroupId(long id){
        return ProductSpecMerchantPriceGroup.find("id = ? and deleted = ?",id , DeletedStatus.UN_DELETED).first();
    }



}
