package product.merchant;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import models.weixin.WebUser;
import play.db.jpa.JPA;
import util.common.UStringUtil;

import javax.persistence.*;
import java.util.List;

/**
 * 商品商户关系
 * Created by buhaoba on 16/6/16.
 */
@Entity
@Table(name = "product_spec_merchant_price_group_webusers")
public class ProductSpecMerchantPriceGroupWebUser extends BaseModel {


    /**
     * 产品价格组
     */
    @JoinColumn(name = "price_group_id")
    @ManyToOne
    public ProductSpecMerchantPriceGroup priceGroup;

    /**
     * 会员
     */
    @JoinColumn(name = "web_user_id")
    @ManyToOne
    public WebUser webUser;


    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark ;


    /**
     *
     * @param groupId
     * @return
     */
    public static List<ProductSpecMerchantPriceGroupWebUser> loadByGroup(long groupId){
        return ProductSpecMerchantPriceGroupWebUser.find(" priceGroup .id = ? and deleted = ?" , groupId , DeletedStatus.UN_DELETED).fetch();
    }

    /**
     *
     * @param specMerchantId
     * @param webUser
     * @return
     */
    public static ProductSpecMerchantPriceGroupWebUser findBySpecMerchantIdAndWebUser(Long specMerchantId , WebUser webUser) {
        return ProductSpecMerchantPriceGroupWebUser.find("priceGroup.productSpecMerchant.id = ? and webUser = ? and deleted = ?" , specMerchantId , webUser , DeletedStatus.UN_DELETED).first();
    }


    /**
     * 最小值
     * @param productId
     * @param webUser
     * @return
     */
    public static Double findMinBySpecMerchantIdAndWebUser(Long productId , WebUser webUser , Merchant merchant) {
        ProductSpecMerchant productSpecMerchant = ProductSpecMerchant.findMinPriceByProduct(productId , merchant);
        ProductSpecMerchantPriceGroupWebUser priceGroupWebUser = findBySpecMerchantIdAndWebUser(productSpecMerchant.id , webUser);
        if(priceGroupWebUser == null) {
            return productSpecMerchant.merchantPrice;
        }
        return priceGroupWebUser.priceGroup.price;
    }

    /**
     * 最大值
     * @param productId
     * @param webUser
     * @return
     */
    public static Double findMaxBySpecMerchantIdAndWebUser(Long productId , WebUser webUser , Merchant merchant) {
        ProductSpecMerchant productSpecMerchant = ProductSpecMerchant.findMaxPriceByProduct(productId , merchant);
        ProductSpecMerchantPriceGroupWebUser priceGroupWebUser = findBySpecMerchantIdAndWebUser(productSpecMerchant.id , webUser);
        if(priceGroupWebUser == null) {
            return productSpecMerchant.merchantPrice;
        }
        return priceGroupWebUser.priceGroup.price;
    }

    /**
     *
     * @param delIds
     */
    public static void deleteWebUser(Integer[] delIds){
        String str = UStringUtil.concatStr(",",delIds);
        String sql = " update product_spec_merchant_price_group_webusers a set a.deleted = 1 where a.id in ("+str+")";
        JPA.em().createNativeQuery(sql).executeUpdate();
    }

    /**
     *
     * @param groupId
     */
    public static void deleteWebUserByGroup(long groupId){
        String sql = " update product_spec_merchant_price_group_webusers a set a.deleted = 1 where a.price_group_id = "+groupId;
        JPA.em().createNativeQuery(sql).executeUpdate();
    }


}
