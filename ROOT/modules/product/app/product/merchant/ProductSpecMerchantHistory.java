package product.merchant;

import models.constants.DeletedStatus;
import models.merchant.Merchant;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * 商户购买历史记录
 * Created by buhaoba on 16/6/16.
 */
@Entity
@Table(name = "product_spec_merchant_historys")
public class ProductSpecMerchantHistory extends Model {

    /**
     * 商品商户关联
     */
    @JoinColumn(name = "product_spec_merchant_id")
    @ManyToOne
    public ProductSpecMerchant productSpec;


    /**
     * 原始库存
     */
    @Column(name = "old_stock")
    public Integer oldStock;


    /**
     * 购买数量
     */
    @Column(name = "buy_number")
    public Integer buyNumber;


    /**
     * 单价
     */
    @Column(name = "price")
    public Double price;


    /**
     * 市场价格
     */
    @Column(name = "market_price")
    public Double marketPrice;

    /**
     * 合伙人价格
     */
    @Column(name = "partner_price")
    public Double partnerPrice;

    /**
     * 商户价格
     */
    @Column(name = "merchant_price")
    public Double merchantPrice;


    /**
     * 最大购买数量
     */
    @Column(name = "max_num")
    public Integer maxNum;


    /**
     * 最小购买量
     */
    @Column(name = "min_num")
    public Integer minNum = 1;

    /**
     * 每次点击跳跃数量 前台客户点击一下 增加数量
     */
    @Column(name = "jump_num")
    public Integer jumpNum;

    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;



}
