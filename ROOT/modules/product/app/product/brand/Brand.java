package product.brand;

import models.BaseModel;
import models.constants.AvailableStatus;
import models.constants.DeletedStatus;
import models.enums.TreeNodeStatus;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import product.Product;
import product.type.ProductType;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by buhaoba on 16/6/14.
 */
@Entity
@Table(name = "brands")
public class Brand extends BaseModel {

    /**
     * 所属类别
     */
    @Column(name = "parent_brand_id")
    public Long parentBrandId;

    /**
     * 品牌名称
     */
    @Column(name = "name")
    public String name;

    /**
     * 是否为叶子节点
     */
    @Column(name = "is_leaf")
    public Boolean isLeaf;

    /**
     * 是否为叶子节点 open:叶子节点 close:非叶子节点
     */
    @Transient
    @Enumerated(EnumType.STRING)
    public TreeNodeStatus state;

    /**
     * 节点全路径
     */
    @Column(name = "node_id_path")
    public String nodeIdPath;

    /**
     *排序号
     */
    @Column(name = "show_order")
    public Integer showOrder = 0;

    /**
     * 图片路径
     */
    @Column(name = "img_path")
    public String imgPath;

    /**
     * 当前状态  禁用\可用
     */
    @Column(name = "available_status")
    @Enumerated(EnumType.STRING)
    public AvailableStatus status = AvailableStatus.AVAILABLE;

    @Column(name = "remark")
    public String remark;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 加载孩子节点
     *
     * @param id
     * @return
     */
    public static List<Brand> loadChildBrand(long id,long productId) {
        Query query = em().createNativeQuery("select distinct b.id ,b.show_order as showOrder,b.name,b.is_leaf,case when p.id is null THEN 0 else 1 end as 'isCheck',b.parent_brand_id,b.available_status as status,case when is_leaf = 1 then 'open' else 'closed' end as 'state' from brands b LEFT JOIN product_brands p on b.id = p.brand_id and p.product_id = "+productId+" where b.deleted = 0 and b.available_status = 'AVAILABLE' and b.parent_brand_id = " + id+" order by b.show_order asc,id desc");
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        return query.getResultList();
    }
    /**
     * 加载孩子节点
     * @param id
     * @return
     */
    public static List<Brand> loadChildBrand(long id) {
        List<Brand> brandList = Brand.find("parentBrandId = ? and deleted = ? order by showOrder,id desc", id, DeletedStatus.UN_DELETED).fetch();
        for (Brand brand : brandList)
            brand.state = brand.isLeaf != null && brand.isLeaf ? TreeNodeStatus.open : TreeNodeStatus.closed;

        return brandList;
    }
    /**
     * 删除品牌
     *
     * @param deleteBrandList
     */
    public static void deleteBrands(Long[] deleteBrandList) {
        for (Long brandId : deleteBrandList) {
            Brand brand = Brand.findById(brandId);
            brand.deleted = DeletedStatus.DELETED;
            brand.save();
            Long brotherQty = Brand.loadBrotherQty(brand);
            if (brotherQty == 0) {
                Brand parentBrand = Brand.findById(brand.parentBrandId);
                if(parentBrand!=null){
                    parentBrand.isLeaf = true;
                    parentBrand.save();
                }
            }
        }

    }

    /**
     * 获取兄弟节点个数
     * @param brand
     * @return
     */
    public static long loadBrotherQty(Brand brand) {
        return Brand.count("parentBrandId = ? and deleted = ?", brand.parentBrandId, DeletedStatus.UN_DELETED);
    }

}
