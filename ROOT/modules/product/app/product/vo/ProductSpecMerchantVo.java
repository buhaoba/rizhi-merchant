package product.vo;

import product.merchant.ProductSpecMerchant;
import utils.HtmlExtension;
import utils.ObjectUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by buhaoba on 16/8/3.
 */
public class ProductSpecMerchantVo {

    public Long id;

    public String mainImage;

    public String name;

    public String spec;

    public String unit;

    public Double merchantPrice;

    public String beginAt;

    public String endAt;

    public String beginTime;

    public String endTime;


    public static List<ProductSpecMerchantVo> toVos(List<ProductSpecMerchant> productSpecMerchantList) {
        List<ProductSpecMerchantVo> productSpecMerchantVoList = new ArrayList<>();
        ProductSpecMerchantVo vo = null;
        for(ProductSpecMerchant psm : productSpecMerchantList) {
            vo = new ProductSpecMerchantVo();
            vo.id = psm.id;
            vo.mainImage = ObjectUtil.checkBlock(psm.productSpec.mainImage) ? "/public/sui/img/p3.jpg" : psm.productSpec.mainImage;
            vo.name = psm.productSpec.product.name;
            vo.spec = HtmlExtension.truncatHtml(psm.productSpec.spec , 5) ;
            vo.unit = HtmlExtension.truncatHtml(psm.productSpec.unit , 4);
            vo.merchantPrice = psm.merchantPrice;
            productSpecMerchantVoList.add(vo);
        }
        return productSpecMerchantVoList;

    }


}
