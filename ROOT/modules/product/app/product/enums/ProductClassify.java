package product.enums;

/**
 * Created by liming on 16/7/12.
 */
public enum ProductClassify {
    BASIC_PRODUCT,//基础产品
    FINISHED_PRODUCT,//成品
    ACCESSORY,//辅料
    COMBO_PRODUCT //套餐
}
