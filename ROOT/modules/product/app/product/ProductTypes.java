package product;


import models.constants.DeletedStatus;
import play.db.jpa.Model;
import product.type.ProductType;
import util.common.UStringUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 产品基本信息
 * Created by upshan on 15/7/7.
 */
@Entity
@Table(name = "product_types")
public class ProductTypes extends Model {

    /**
     * 所属类别
     */
    @JoinColumn(name = "type_id")
    @ManyToOne
    public ProductType type;

    /**
     * 关联产品
     */
    @JoinColumn(name = "product_id")
    @ManyToOne
    public Product product;

    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 跟据Product 查询 Product所属的所有类别
     * @param product
     * @return
     */
    public static List<ProductTypes> findByProduct(Product product) {
        List<ProductTypes> productTypes = ProductTypes.find("product = ?1 and deleted = ?2" , product , DeletedStatus.UN_DELETED).fetch();
        return productTypes;
    }

    /**
     * 跟据 type 查询 productType 下所有商品
     * @param type
     * @return
     */
    public static List<ProductTypes> findByType(ProductType type) {
        List<ProductTypes> productTypes = ProductTypes.find("type = ?1 and deleted = ?2" , type , DeletedStatus.UN_DELETED).fetch();
        return productTypes;
    }

    /**
     * 跟据 type 查询 productType 下所有商品
     * @return
     */
    public List<ProductTypes> findByType() {
        List<ProductTypes> productTypes = ProductTypes.find("type = ?1 and deleted = ?2" , this.type , DeletedStatus.UN_DELETED).fetch();
        return productTypes;
    }

    /**
     * 根据产品删除
     */
    public static void delByProduct(Integer[] productTypeDelArray){
        String deletePTStr = UStringUtil.concatStr(",",productTypeDelArray);
        ProductTypes.delete("id in ("+deletePTStr+")");
    }
}
