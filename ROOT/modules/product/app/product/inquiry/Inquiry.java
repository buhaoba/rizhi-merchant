package product.inquiry;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.weixin.WebUser;
import play.db.jpa.Model;
import product.Product;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 商品报价表
 * Created by buhaoba on 16/9/9.
 */
@Entity
@Table(name = "inquirys")
public class Inquiry extends BaseModel {

    public static final Integer STATUS_SUCCESS = 1; //成功
    public static final Integer STATUS_CREATE = 0; // 创建
    public static final Integer STATUS_FAIL = 2;   // 失败


    /**
     * 产品信息
     */
    @ManyToOne
    @JoinColumn(name = "product_id")
    public Product product;


    /**
     * 提供信息用户
     */
    @ManyToOne
    @JoinColumn(name = "web_user_id")
    public WebUser webUser;

    /**
     * 报价
     */
    @Column(name = "price")
    public Double price;


    /**
     * 状态
     */
    @Column(name = "status")
    public Integer status = STATUS_CREATE;


    /**
     * 可提供数量 (KG)
     */
    @Column(name = "num")
    public Integer num;


    /**
     * 联系电话
     */
    @Column(name = "phone")
    public String phone;

    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;

    /**
     * 备注
     */
    @Column(name = "remark")
    public String remark;

    /**
     * 根据id查询未删除的
     */
    public static Inquiry findByInquiryId(long id) {
        return Inquiry.find("id=? and deleted=?", id, DeletedStatus.UN_DELETED).first();
    }

    /**
     * 根据微信用户查询报价
     * @param webUser
     * @return
     */
    public static List<Inquiry> findByWebUser(WebUser webUser , Date searchDate) {
        return Inquiry.find("deleted = ? and webUser = ? and createdAt > ? order by id" , DeletedStatus.UN_DELETED , webUser , searchDate).fetch();
    }

    /**
     * 根据微信用户和状态查询
     * @param webUser
     * @param status
     * @return
     */
    public static List<Inquiry> findByWebUserAndStatus(WebUser webUser,Integer status,Date searchDate){
        return Inquiry.find("deleted= ? and webUser = ? and status= ? and createdAt > ?", DeletedStatus.UN_DELETED, webUser, status, searchDate).fetch();
    }

}
