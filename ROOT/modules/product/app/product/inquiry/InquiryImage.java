package product.inquiry;

import models.constants.DeletedStatus;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 商品报价图片表
 * Created by buhaoba on 16/9/9.
 */
@Entity
@Table(name = "inquiry_images")
public class InquiryImage extends Model {

    /**
     * 报价信息
     */
    @ManyToOne
    @JoinColumn(name = "inquiry_id")
    public Inquiry inquiry;


    /**
     * 图片路径
     */
    @Column(name = "img_path")
    public String imgPath;

    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;


    public InquiryImage() {
        super();
    }

    public InquiryImage(Inquiry inquiry , String path) {
        this.inquiry = inquiry;
        this.imgPath = path;
        this.createdAt = new Date();
        this.deleted = DeletedStatus.UN_DELETED;
    }


    /**
     * 查询报价信息的相关的图片
     * @param id
     * @return
     */
    public static List<InquiryImage> loadByInquiryId(long id){
        return  InquiryImage.find("inquiry.id=? and deleted=?",id,DeletedStatus.UN_DELETED).fetch();
    }


}
