package product;

import play.db.jpa.Model;

import javax.persistence.*;

/**
 * Created by Administrator on 2017/9/25.
 */
@Entity
@Table(name = "product_comment")
public class ProductComment extends Model {

    //星数
    @Column(name = "star_num")
    public int starNum;


    /**
     * 关联商品
     */
    @JoinColumn(name = "product_id")
    @ManyToOne
    public Product product;



}
