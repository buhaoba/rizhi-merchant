package product;

import models.BaseModel;
import models.constants.DeletedStatus;
import models.weixin.WebUser;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2018/4/12.
 */
@Entity
@Table(name = "product_access_record")
public class ProductAccessRecord extends BaseModel {

    /**
     * 访问时间 年月日
     */
    @Column(name = "access_date")
    @Temporal(TemporalType.DATE)
    public Date accessDate;

    /**
     *  访问人
     */
    @JoinColumn(name = "web_user_id")
    @ManyToOne
    public WebUser webUser;

    /**
     *  访问产品
     */
    @JoinColumn(name = "product_id")
    @ManyToOne
    public Product product;

    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted = DeletedStatus.UN_DELETED;


    /**
     *  昨日访问顾客数
     * @param beginDate
     * @param endDate
     * @return
     */
    public static List<ProductAccessRecord> findYesterdayByWebUser(Date beginDate, Date endDate) {
        return ProductAccessRecord.find("  deleted=?1 and createAt between ?2 and ?3 GROUP BY webUser.id " , DeletedStatus.UN_DELETED ,beginDate,endDate ).fetch();
    }

    /**
     *  昨日访问商品数
     * @param beginDate
     * @param endDate
     * @return
     */
    public static List<ProductAccessRecord> findYesterdayByProduct(Date beginDate, Date endDate) {
        return ProductAccessRecord.find(" deleted = ?1 and  createAt between ?2 and ?3 GROUP BY product.id " , DeletedStatus.UN_DELETED ,beginDate,endDate ).fetch();
    }

    public static List<ProductAccessRecord> findListByProductIdAndDay(Date beginDate, Date endDate,long productId) {
        return ProductAccessRecord.find(" deleted = ?1 and  accessDate between ?2 and ?3 and product.id = ?4  " , DeletedStatus.UN_DELETED ,beginDate,endDate ,productId).fetch();
    }



}
