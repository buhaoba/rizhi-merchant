package product;


import models.constants.DeletedStatus;
import play.db.jpa.Model;
import product.brand.Brand;
import util.common.UStringUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 产品基本信息
 * Created by upshan on 15/7/7.
 */
@Entity
@Table(name = "product_brands")
public class ProductBrands extends Model {



    /**
     * 所属类别
     */
    @JoinColumn(name = "brand_id")
    @ManyToOne
    public Brand brand;


    /**
     * 所属类别
     */
    @JoinColumn(name = "product_id")
    @ManyToOne
    public Product product;


    /**
     * 创建时间
     */
    @Column(name = "created_at")
    public Date createdAt;

    /**
     * 逻辑删除,0:未删除，1:已删除
     */
    @Enumerated(EnumType.ORDINAL)
    public DeletedStatus deleted;


    /**
     * 跟据Product 查询 Product所属的所有品牌
     * @param product
     * @return
     */
    public static List<ProductBrands> findByProduct(Product product) {
        List<ProductBrands> productBrandses = ProductBrands.find("product = ? and deleted = ?" , product , DeletedStatus.UN_DELETED).fetch();
        return productBrandses;
    }

    /**
     * 跟据brand 查询 brand 下所有商品
     * @param brand
     * @return
     */
    public static List<ProductBrands> findByBrand(Brand brand) {
        List<ProductBrands> productBrandses = ProductBrands.find("brand = ? and deleted = ?" , brand , DeletedStatus.UN_DELETED).fetch();
        return productBrandses;
    }

    /**
     * 跟据brand 查询 brand 下所有商品
     * @return
     */
    public  List<ProductBrands> findByBrand() {
        List<ProductBrands> productBrandses = ProductBrands.find("brand = ? and deleted = ?" , this.brand , DeletedStatus.UN_DELETED).fetch();
        return productBrandses;
    }
    /**
     * 根据产品删除
     * @param productBrandDelArray
     */
    public static void delByProduct(Integer[] productBrandDelArray){
        String deleteProBrands= UStringUtil.concatStr(",",productBrandDelArray);
        ProductBrands.delete("id in ("+deleteProBrands+")");
    }
}
