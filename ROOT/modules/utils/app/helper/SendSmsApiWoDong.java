package helper;


import play.Logger;
import utils.DateUtil;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Date;

/**
 * 短信发送API 提供测试时使用
 * SEND_URL   USER_NAME   PASS_WORD  AUTOGRAPH 可能会修改
 */
public class SendSmsApiWoDong {

    private static final String SEND_URL = "http://115.29.242.32:8888/sms.aspx";
    private static final String ACCOUNT = "SHYT";
    private static final String PASS_WORD = "135642";
    private static final String USER_ID = "1996";
    private static final String AUTOGRAPH = "【义提科技】";


    private static StringBuffer init() {
        StringBuffer prefix = new StringBuffer();
        prefix.append("action=send");
        prefix.append("&userid=" + USER_ID);
        prefix.append("&account=" + ACCOUNT);
        prefix.append("&password=" + PASS_WORD);
        prefix.append("&sendTime=" );
        prefix.append("&extno=" );
        return prefix;
    }


    /**
     * 组合出 要发送的 String 字符串
     * 格式为: http://114.215.196.145/sendSmsApi?username=shhw&password=3GXREQDv&mobile=18006333020&xh=&content=【恒巍】您的验证码为1234
     * @param phone  传入的手机好吗。 多个用 , 隔开 例如: 19006333020,18877712221
     * @param content 要发送的内容 例如: 您的验证码为:22211   或者  您有一条新的消息请注意查收
     * @return
     */
    public static String combinationSendSms(String phone , String content) {
        StringBuffer prefix = init();
        try {
            prefix.append("&mobile=" + phone);
            prefix.append("&content="  + URLEncoder.encode(content+ AUTOGRAPH, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String responseCode = sendPost(SEND_URL,prefix.toString() , "UTF-8");
        return responseCode;
    }
    /**
     * <p>
     * POST方法
     * </p>
     *
     * @param sendUrl
     *            ：访问URL
     * @param sendParam
     *            ：参数串
     * @param backEncodType
     *            ：返回的编码
     * @return
     */
    public static String sendPost(String sendUrl, String sendParam,
                           String backEncodType) {

        Logger.info("SendUrl : %s" , sendUrl);
        Logger.info("SendParam : %s" , sendParam);
        Logger.info("backEncodType : %s" , backEncodType);
        Logger.info("SendTime : %s" , DateUtil.dateToString(new Date() , "yyyy-MM-dd HH:mm:ss"));
        StringBuffer receive = new StringBuffer();
        BufferedWriter wr = null;
        try {
            if (backEncodType == null || backEncodType.equals("")) {
                backEncodType = "UTF-8";
            }

            URL url = new URL(sendUrl);
            HttpURLConnection URLConn = (HttpURLConnection) url
                    .openConnection();

            URLConn.setDoOutput(true);
            URLConn.setDoInput(true);
            ((HttpURLConnection) URLConn).setRequestMethod("POST");
            URLConn.setUseCaches(false);
            URLConn.setAllowUserInteraction(true);
            HttpURLConnection.setFollowRedirects(true);
            URLConn.setInstanceFollowRedirects(true);

            URLConn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded;charset=UTF-8");
            URLConn.setRequestProperty("Content-Length", String
                    .valueOf(sendParam.getBytes().length));

            DataOutputStream dos = new DataOutputStream(URLConn
                    .getOutputStream());
            dos.writeBytes(sendParam);

            BufferedReader rd = new BufferedReader(new InputStreamReader(
                    URLConn.getInputStream(), backEncodType));
            String line;
            while ((line = rd.readLine()) != null) {
                receive.append(line).append("\r\n");
            }
            rd.close();
        } catch (java.io.IOException e) {
            receive.append("访问产生了异常-->").append(e.getMessage());
            e.printStackTrace();
        } finally {
            if (wr != null) {
                try {
                    wr.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                wr = null;
            }
        }

        return receive.toString();
    }

}
