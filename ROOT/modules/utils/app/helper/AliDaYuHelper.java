package helper;

import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;
import play.Logger;
import utils.DateUtil;

import java.util.Date;

/**
 * Created by buhaoba on 16/1/12.
 */
public class AliDaYuHelper {

    public static Boolean sendMsg(String phone , String code) {
        TaobaoClient client = new DefaultTaobaoClient(GlobalConfig.ALI_DAYU_URL, GlobalConfig.ALI_DAYU_APPID, GlobalConfig.ALI_DAYU_SECRET);
        AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
        req.setSmsType("normal");
        req.setSmsFreeSignName("注册验证");
        req.setRecNum(phone);
        req.setSmsTemplateCode("SMS_1920177");
        req.setSmsParam("{'code':'"+code+"','product':'优粮城'}");
        AlibabaAliqinFcSmsNumSendResponse rsp = null;
        try {
            Logger.info("发送时间 : " + DateUtil.dateToString(new Date() , "yyyy-MM-dd HH:mm:ss"));
            rsp = client.execute(req);
//            new UserPhoneMsg(phone , rsp.isSuccess() ? "成功" : "失败" , rsp.getSubCode() , rsp.getSubMsg());
            if(rsp != null && rsp.isSuccess()) {
                Logger.info("手机号码为" + phone + "发送验证码 : " + code + "成功");
                return true;
            } else {
                Logger.info("手机号码为" + phone + "发送验证码 : " + code + "失败. 原因Code :" + rsp.getSubCode() + " , 错误原因 : " + rsp.getSubMsg());
                return false;
            }
        } catch (ApiException e) {
            e.printStackTrace();
        }
        return false;
    }
}
