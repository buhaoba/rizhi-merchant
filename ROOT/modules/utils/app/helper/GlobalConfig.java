package helper;

import play.Play;

/**
 * 全局配置变量.
 */
public class GlobalConfig {

    /**
     * 微信WAP网站、请求的主域名.
     */
//    public final static String WEIXIN_BASE_DOMAIN = Play.configuration.getProperty("weixin.basedomain", "http://rizhiweixin.ulmsale.net");
    public final static String WEIXIN_BASE_DOMAIN = Play.configuration.getProperty("weixin.domain" , "http://upshan.mynatapp.cc");;
    public final static String WEIXIN_PAY_BACK_URL = "/weixin/payment/callback";
    public final static String SPBILL_CREATE_IP = "39.72.132.15";

    // Weixin Session 配置信息
    public final static String WEIXIN_MP_SESSION_USER_KEY = "wxmp_user_id";
    public final static String WEIXIN_MP_WEIXIN_SESSION_USER_KEY = "wxmp_user_id_weixindata_";
    public final static String WEIXIN_MP_WEIXIN_SESSION_LINK_ID = "wxmp_user_id_link_id";
    public final static String WEIXIN_MP_WEIXIN_SESSION_USER_ID = "wxmp_user_id_user_id";

    public final static String WEIXIN_CP_SESSION_USER_KEY = "wxcp_user_id";

    // 百度配置信息
    public final static String BAIDU_API_AK = "9fb983ecd9b505f8fedcc9ab07c65e3e";

    // 阿里云 OSS 配置信息
    public final static String ALI_OSS_ENDPOINT = "http://oss-cn-hangzhou.aliyuncs.com";
    public final static String ALI_OSS_ACCESS_KEY_ID = "dgneRI3o7IJFXSRC";
    public final static String ALI_OSS_ACCESS_KEY_SECRET = "PT74UGnspNnXS3fSUGKjYGPfF42l4l";
    public final static String ALI_OSS_BUCKET_NAME = "buhaoba";
    public final static String ALI_OSS_DOMAIN = "http://cdn.buhaoba.com";
    public final static String ALI_IMG_OSS_DOMAIN = "http://buhaoba.img-cn-hangzhou.aliyuncs.com";


    // COOKIE 配置信息
    public final static String REF_CODE_COOKIE = "REF_CODE_COOKIE_YOYO";

    // CDN配置信息
    public final static String ASSETS_BASE_URL = "http://cdn.buhaoba.com";
    public final static String ASSETS_VERSION = "v1";


    // 微信公众号 配置信息
    public final static Long WEIXIN_DATE_ID = 2l;
    public final static String WEIXIN_APP_ID = Play.configuration.getProperty("weixin.appId" , "wxbe347cb53e58db0a");
    public final static String WEIXIN_APP_SECURET = Play.configuration.getProperty("weixin.appSecret" , "8f09f34a6759df80962dc516d5978cf3");
    public final static String WEIXIN_TOKEN = Play.configuration.getProperty("weixin.token" , "youliang2015");
    public final static String WEIXIN_AES_KEY = Play.configuration.getProperty("weixin.aesKey" ,"s3VtPBoddHMdlof18cOyr8khsx1RtH58oDdZ3wkdMH9");
    public final static String WEIXIN_PAY_KEY = Play.configuration.getProperty("weixin.payKey" ,"asdfasdf009r0q90qw9r01wq09rtyeer");
    public final static String WEIXIN_MECHID = Play.configuration.getProperty("weixin.mchId" ,"1251164801");
    public final static String WEIXIN_PAY_KEY_PATH = Play.configuration.getProperty("weixin.paypath" ,"C:/apiclient_cert.p12");


    public final static String NOTIFY_URL = "http://localhost:9002/";
    public final static String WEIXIN_PARTNER_KEY = "asdfasdf009r0q90qw9r01wq09rtyeer";


    // Pingxx 支付配置信息
    public final static String PINGPP_APP_ID = Play.configuration.getProperty("pingxx.appId" , "app_0yTK4KOa94u94GSK");
    public final static String PINGPP_API_KEY = Play.configuration.getProperty("pingxx.secret" , "sk_live_0arDCODS80qLnLizn90mTCmL");

    public final static String APPLICATION_BASE_DOMAIN = Play.configuration.getProperty("base.domain" , "yoyo.buhaoba.com");


    // Ali大鱼配置信息
    public final static String ALI_DAYU_URL = "http://gw.api.taobao.com/router/rest";
    public final static String ALI_DAYU_APPID = "23298129";
    public final static String ALI_DAYU_SECRET = "5efdf6751f9c041f7feea2d25c87e837";



    public final static String ERP_APP_CODE ="siff";
    public final static String ERP_APP_SECRET = "a8931d6c100efc76z18b70ce510";


    public final static String OSS_WEIXIN_SIZE = "@620w";
    public final static String OSS_PC_SIZE = "@1000w";


    // OpenSearch
    public final static String ALI_SEARCH_ACCESSKEY = "jLWd2Fvj1FlRHU7z";
    public final static String ALI_SEARCH_SECRET = "0ai9PYlIXuwPPx4AkVapCkuyL1YvCP";
    public final static String ALI_SEARCH_API = "http://opensearch-cn-beijing.aliyuncs.com";
    public final static String ALI_SEARCH_APP_NAME = "rizhishop";
//    public final static String ALI_SEARCH_TEMP = "ulmsale_product";


     // 分页
     public final static Integer PAGE_SIZE = 10;




    /**
     * 微信第三方应用集成的suitId.
     */
    public final static String WXCP_3RD_SUITE_ID = Play.configuration.getProperty("wxcp.3rd.suite_id", "tj71ceb1ea024c8f4d");

    public final static String WXCP_3RD_SUITE_TOKEN = Play.configuration.getProperty("wxcp.3rd.suite_token", "RcYLkgoouPj9FdzxSYXTcX5");
    public final static String WXCP_3RD_SUITE_AESKEY = Play.configuration.getProperty("wxcp.3rd.suite_aesKey",
            "t9x4vWVHvuOGwFmonSF5xKx2SoemLPgs43vXItJCEG6");

    public final static String WXCP_3RD_SUITE_SECRET = Play.configuration.getProperty("wxcp.3rd.suite_secret",
            "PsjcKa-Mf5ym_naZuKc1rm-oo7QOURuD067Gf84ui4Rmx8hLuz6EylFcdAqTCcce");


    public final static Integer WXCP_SUITE_ZAINR_APPID = Integer.parseInt(
            Play.configuration.getProperty("wxcp.3rd.suite.zainr.appid", "1"));
    public final static String WXCP_SUITE_ZAINR_APPNAME =
            Play.configuration.getProperty("wxcp.3rd.suite.zainr.appname", "业务员在哪儿");




}
