package helper;

import org.apache.commons.lang.StringUtils;
import utils.RandomNumberUtil;
import utils.SignUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by buhaoba on 2017/4/24.
 */
public class UnifiedWeixinPay {


    public static final String WeixinPayUrl = "weixin://wxpay/bizpayurl";
    public static final String payKey = "";


    /**
     * 生成 扫码支付二维码
     * @param appid
     * @param mch_id
     * @param body
     * @param out_trade_no
     * @param total_fee
     * @param spbill_create_ip
     * @param openId
     */
    public static String index(String appid , String mch_id , String body , String out_trade_no , String total_fee , String spbill_create_ip, String openId) {

        String nonce_str = RandomNumberUtil.generateRandomChars(32);

        Map<String , String> signMap = new HashMap<>();
        signMap.put("appid" , appid);
        signMap.put("mch_id" , mch_id);
        signMap.put("nonce_str" , nonce_str);
        signMap.put("time_stamp" , String.valueOf(System.currentTimeMillis()));
        signMap.put("product_id" , out_trade_no);
        String sign = SignUtils.createSign(signMap , payKey);


        StringBuilder rqCode = new StringBuilder(WeixinPayUrl + "?");
        for (String key : signMap.keySet()) {
            String value = signMap.get(key);
            if (StringUtils.isNotEmpty(value)) {
                rqCode.append(key + "=" + value + "&");
            }
        }
        rqCode.append("sign=" + sign);
        return  rqCode.toString();
    }

}
