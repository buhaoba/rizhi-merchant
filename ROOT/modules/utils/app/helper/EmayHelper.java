package helper;

import cn.emay.sdk.client.api.Client;
import play.Logger;
import play.Play;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by buhaoba on 15/11/6.
 */
public class EmayHelper {

    public static final String EMAY_KEY = Play.configuration.getProperty("emay.key" , "6SDK-EMY-6688-KKRUP");
    public static final String EMAY_VALUE = Play.configuration.getProperty("emay.value" , "youliangcheng998");


    public static Map<String,Object> sendMsg(String phone , String msg) {
        Map<String , Object> resultMap = new HashMap<>();
        try {
            Logger.info("EMAY_KEY : %s  | EMAY_VALUE : %s" , EMAY_KEY , EMAY_VALUE);
            msg = "【优粮城】"+msg;
            Client client = new Client(EMAY_KEY , EMAY_VALUE);
//            Logger.info("PHONE : %s  | , msg : %s" , phone , msg);
            Integer resultStatus = client.sendSMS(new String[]{phone} , msg , 5);
            if(resultStatus == 0) {
                Logger.info("短信发送成功");
                resultMap.put("success","true");
                resultMap.put("msg","sendMsg success!");
                return resultMap;
            } else {
                Logger.info("短信发送失败 : %s" , resultStatus);
                resultMap.put("success","false");
                resultMap.put("msg","sendMsg fail!");
                return resultMap;
            }
        } catch (Exception e) {
            resultMap.put("success","false");
            resultMap.put("msg","sendMsg fail!");
            return resultMap;

        }
    }



}
