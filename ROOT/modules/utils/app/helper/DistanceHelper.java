package helper;

import java.math.BigDecimal;

/**
 * Created by buhaoba on 16/5/17.
 */
public class DistanceHelper {



    static double DEF_PI = 3.14159265359; // PI
    static double DEF_2PI = 6.28318530712; // 2*PI
    static double DEF_PI180 = 0.01745329252; // PI/180.0
    static double DEF_R = 6370693.5; // radius of earth

    /**
     *
     * @param lat  开始的纬度
     * @param lng  开始的经度
     * @param _lat 结束的纬度
     * @param _lng 结束的经度
     * @return     返回两者之间的直线距离  (KM为单位)
     */
    public static Double getDistance(String lat, String lng , String _lat , String _lng) {
        if (org.apache.commons.lang.StringUtils.isBlank(lat) || lat.equals("0") || org.apache.commons.lang.StringUtils.isBlank(lng) || lng.equals("0")) {
            return 10000d;
        }
        if(org.apache.commons.lang.StringUtils.isBlank(_lat) || _lat.equals("0") || org.apache.commons.lang.StringUtils.isBlank(_lng) || _lng.equals("0")) {
            return 10000d;
        }
        double ew1, ns1, ew2, ns2;
        double dx, dy, dew;
        double distance;
        // 角度转换为弧度
        ew1 = Double.parseDouble(lng) * DEF_PI180;
        ns1 = Double.parseDouble(lat) * DEF_PI180;
        ew2 = Double.parseDouble(_lng) * DEF_PI180;
        ns2 = Double.parseDouble(_lat) * DEF_PI180;
        // 经度差
        dew = ew1 - ew2;
        // 若跨东经和西经180 度，进行调整
        if (dew > DEF_PI) {
            dew = DEF_2PI - dew;
        } else if (dew < -DEF_PI) {
            dew = DEF_2PI + dew;
        }
        dx = DEF_R * Math.cos(ns1) * dew; // 东西方向长度(在纬度圈上的投影长度)
        dy = DEF_R * (ns1 - ns2); // 南北方向长度(在经度圈上的投影长度)
        // 勾股定理求斜边长
        distance = Math.sqrt(dx * dx + dy * dy);
        BigDecimal res = new BigDecimal(distance).divide(new BigDecimal(1000), 1, BigDecimal.ROUND_HALF_UP);
        return Math.abs(res.doubleValue());
    }


}
