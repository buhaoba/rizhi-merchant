package utils;

import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by buhaoba on 16/6/13.
 */
public class ObjectUtil {

    /**
     * 检测参数是否不为空
     * 支持 Object 、 String 、 Integer 、 Long 、 Float
     */
    public static Boolean  checkNotBlock(Object search){
        if(search == null) {
            return false;
        }
        //判断 String
        if(search instanceof String) {
            if(StringUtils.isBlank(search.toString()) && !search.toString().equals("null") && !search.toString().equals("NULL")) {
                return false;
            }
        }
        // 判断 Integer
        if(search instanceof Integer || search instanceof  Long || search instanceof Float) {
            Integer _int_search = Integer.parseInt(search.toString());
            if(_int_search <= 0) {
                return false;
            }
        }
        return true;
    }


    /**
     * 检测参数是否为空
     * 支持 Object 、 String 、 Integer 、 Long 、 Float
     */
    public static Boolean  checkBlock(Object search){
        if(search == null) {
            return true;
        }
        //判断 String
        if(search instanceof String) {
            if(StringUtils.isBlank(search.toString())) {
                return true;
            }
        }
        // 判断 Integer
        if(search instanceof Integer || search instanceof  Long || search instanceof Float) {
            Integer _int_search = Integer.parseInt(search.toString());
            if(_int_search <= 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * 将json格式的字符串解析成Map对象 <li>
     * json格式：{"name":"admin","retries":"3fff","testname"
     * :"ddd","testretries":"fffffffff"}
     */
    public static HashMap<String, Object> toHashMap(Object object)
    {
        HashMap<String, Object> data = new HashMap<>();
        // 将json字符串转换成jsonObject
        JSONObject jsonObject = new JSONObject(object);  //JSONObject.fromObject(object);
        Iterator it = jsonObject.keys();
        // 遍历jsonObject数据，添加到Map对象
        while (it.hasNext())
        {
            String key = String.valueOf(it.next());
            Object value = null;
            try {
                value = jsonObject.get(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            data.put(key, value);
        }
        return data;
    }

    public static <T> List<T> StringTolist(String jsonString, Class<T[]> type) {

        Gson gson = new Gson();
        T[] list  = gson.fromJson(jsonString, type);

        return Arrays.asList(list);

    }


    /**
     * 补位
     * @param number  要补位的数字
     * @param size    字符串长度
     * @return
     */
    public static String coverNumber(Integer number , Integer size) {
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMinimumIntegerDigits(size);
        formatter.setGroupingUsed(false);
        return formatter.format(number);
    }

}
