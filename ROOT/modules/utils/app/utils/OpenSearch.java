package utils;

import product.Product;

/**
 * Created by buhaoba on 16/7/20.
 */
public class OpenSearch {

    public Long id; //product_spec_merchant_id

    public String name; //产品 name

    public String desc; //产品描述

    public String img = "/public/sui/img/p1.jpg"; //图片

    public String spec; //型号

    public String unit; //单位

    public Double price; //价格


    public Product product;


    public String index_name; //没用

    public String merchantName; // 商户名称
    public String merchantPhone; // 商户手机号

}
