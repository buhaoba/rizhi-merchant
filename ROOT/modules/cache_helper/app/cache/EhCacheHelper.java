package cache;

//import models.sms.SMSMessage;

import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.cache.EhCacheImpl;
import play.libs.Time;

import java.util.UUID;

/**
 * 缓存使用的包装类.
 * 功能描述：缓存操作,可以预读部分key到本地，增加读取性能<br>
 * 支持多级key，可以批量失效同一个baseKey的一批缓存<br>
 * 例如:setCache(new String[]{"user_"},"1",User对象)，实际存储为:<br>
 * "user_" = "xxxxxx"，User对象存放在:<br>
 * ${"user_"}_1 = User对象，即User对象的cacheKey为xxxxxx_1<br>
 * 多个User对象存为xxxxxx_2,xxxxxx_3,xxxxxx_4<br>
 * 删除单个对象delete(new String[]{"user_"},"1")<br>
 * 删除所有用户对象delete("user_")<br>
 * "user_"删掉以后，${"user_"}_1会变成yyyyy_1,因为${"user_"}的可以找不到后系统会随机生成另一值<br>
 * 这样${"user_"}_1会变成yyyyy_1就相当于删除了以前所有xxxxxx_x的对象，因为xxxxxx是随机生成，在也不会获得到<br>
 *
 * @author <a href="mailto:tang.liqun@qq.com">唐力群</a>
 */
public class EhCacheHelper {
    private static final String defaultExpireSeconds        = "24h"; // 默认超时时间
    private static final String defaultBaseKeyExpireSeconds = "240h"; // 默认BaseKey超时时间

    private static final EhCacheImpl cacheImpl = getEhCacheImplInstance();

    private static EhCacheImpl getEhCacheImplInstance() {
        if (EhCacheImpl.getInstance() == null) {
            return EhCacheImpl.newInstance();
        }
        return EhCacheImpl.getInstance();
    }

    /**
     * 根据二级key，生成最终存储的key
     *
     * @param baseKey
     * @param subKey
     * @return
     */
    public static String getCacheKey(String baseKey, String subKey) {
        return getCacheKey(new String[]{baseKey}, subKey, defaultBaseKeyExpireSeconds);
    }

    /**
     * 根据二级key，生成最终存储的key
     *
     * @param baseKeys
     * @param subKey
     * @return
     */
    public static String getCacheKey(String[] baseKeys, String subKey) {
        return getCacheKey(baseKeys, subKey, defaultBaseKeyExpireSeconds);
    }

    /**
     * 根据二级key，生成最终存储的key
     *
     * @param baseKeys
     * @param subKey
     * @param expireSeconds
     * @return
     */
    public static String getCacheKey(String[] baseKeys, String subKey, String expireSeconds) {
        if (baseKeys == null || baseKeys.length == 0) {
            throw new IllegalArgumentException("baseKey数组不能为空");
        }
        if (StringUtils.isBlank(subKey)) {
            throw new IllegalArgumentException("subKey不能为空");
        }
        // 拼接baseKey
        StringBuilder fullBaseKey = new StringBuilder();
        for (String baseKey : baseKeys) {
            if (StringUtils.isBlank(baseKey)) {
                baseKey = "NULL";  //baseKey为空，则设置为NULL值，这样所有空值都可以得到一个randomKey.
            }
            final String _baseKey = baseKey;
            String cachedBaseKey = getCache(baseKey, expireSeconds, new CacheCallBack<String>() {
                public String loadData() {
                    String randomKey = getRandomKey(_baseKey);
                    Logger.debug("重新生成Key:" + randomKey);
                    return randomKey;
                }
            });
            fullBaseKey.append(cachedBaseKey).append("_");
        }
        Logger.debug("返回CacheKey:" + fullBaseKey + subKey);
        return fullBaseKey + subKey;
    }

    public static void setCache(String key, Object value) {
        setCache(key, value, defaultExpireSeconds);
    }

    public static boolean exists(String key) {
        return getCache(key) != null;
    }

    public static void setCache(String key, Object value, String expireSeconds) {
        if (StringUtils.isBlank(key)) {
            throw new IllegalArgumentException("key不能为空");
        }
        try {
            cacheImpl.set(key, value, Time.parseDuration(expireSeconds));
        } catch (Exception e) {
            Logger.warn(e, "When set cache[key:" + key + "] found exception.");
            checkException(e.getMessage());
            delete(key);
        }
    }

    public static void setCache(String[] baseKeys, String subKey, Object value) {
        setCache(getCacheKey(baseKeys, subKey), value);
    }

    public static void setCache(String[] baseKeys, String subKey, Object value, String expireSeconds) {
        setCache(getCacheKey(baseKeys, subKey, expireSeconds), value, expireSeconds);
    }

    public static <T> T getCache(String key) {
        return getCache(key, null, defaultExpireSeconds, null);
    }

    public static <T> T getCache(String key, Class<T> targetClass) {
        return getCache(key, targetClass, defaultExpireSeconds, null);
    }

    public static <T> T getCache(String key, CacheCallBack<T> callback) {
        return getCache(key, null, defaultExpireSeconds, callback);
    }

    public static <T> T getCache(String key, Class<T> targetClass, CacheCallBack<T> callback) {
        return getCache(key, targetClass, defaultExpireSeconds, callback);
    }

    public static <T> T getCache(String key, String expireSeconds, CacheCallBack<T> callback) {
        return getCache(key, null, expireSeconds, callback);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getCache(String key, Class<T> targetClass, String expireSeconds, CacheCallBack<T> callback) {
        if (StringUtils.isBlank(key)) {
            throw new IllegalArgumentException("key不能为空");
        }
        Logger.debug("load cache key:(" + key + ")");
        try {
            Object cacheObject = cacheImpl.get(key);
            if (cacheObject != null) {
                if (targetClass != null) {
                    try {
                        // 检测类型是否正确
                        targetClass.cast(cacheObject);
                        return (T) cacheObject;
                    } catch (ClassCastException e) {
                        Logger.warn(e, "Get Object[" + key + "] from callback Error. can't cast to " + targetClass.getName()
                                + " from " + cacheObject.getClass().getName());
                        // 防止下次出错，删除后，重新加载进缓存
                        delete(key);
                        cacheObject = null;  //置空cache对象，以进行重试loadData
                    }
                } else {
                    // 加到预读Map中，同一线程下次读取时会成为本地内存读取，速度更快
                    return (T) cacheObject;
                }
            }
            // the cacheObject always is null
            if (callback != null) {
                // TODO: 使用类或实例级别锁粒度太粗，在并发过1k后，容易因为别的key的缓存操作而阻塞.
                synchronized (EhCacheHelper.class) {
                    Object dataObject = cacheImpl.get(key);
                    if (dataObject == null) {
                        Logger.debug(" 调用loadData(), key:%s", key);
                        dataObject = callback.loadData();
                    }
                    if (dataObject != null) {
                        if (targetClass != null) {
                            try {
                                // 检测类型是否正确
                                targetClass.cast(dataObject);
                                cacheImpl.set(key, dataObject, Time.parseDuration(expireSeconds));
                                return (T) dataObject;
                            } catch (ClassCastException e) {
                                Logger.warn(e, "Get Object[" + key + "] from callback Error. can't cast to "
                                        + targetClass.getName() + " from " + dataObject.getClass().getName());
                            }
                        } else {
                            try {
                                cacheImpl.set(key, dataObject, Time.parseDuration(expireSeconds));
                            } catch (ClassCastException e) {
                                Logger.warn(e, "Put Object[" + key + "] to Cache Error.");
                            }
                            return (T) dataObject;
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.warn(e, "When get cache[key:" + key + "] found exception:" + e.getMessage());
            checkException(e.getMessage());
            delete(key);
        }
        return null;
    }

    public static <T> T getCache(String[] baseKeys, String subKey) {
        return getCache(getCacheKey(baseKeys, subKey), null, defaultExpireSeconds, new CacheCallBack<T>() {
            public T loadData() {
                return null;
            }

        });
    }

    public static <T> T getCache(String[] baseKeys, String subKey, Class<T> targetClass) {
        return getCache(getCacheKey(baseKeys, subKey), targetClass, defaultExpireSeconds, new CacheCallBack<T>() {
            @Override
            public T loadData() {
                return null;
            }

        });
    }

    public static <T> T getCache(String[] baseKeys, String subKey, CacheCallBack<T> callback) {
        return getCache(getCacheKey(baseKeys, subKey), null, defaultExpireSeconds, callback);
    }

    public static <T> T getCache(String[] baseKeys, String subKey, Class<T> targetClass, CacheCallBack<T> callback) {
        return getCache(getCacheKey(baseKeys, subKey), targetClass, defaultExpireSeconds, callback);
    }

    public static <T> T getCache(String[] baseKeys, String subKey, String expireSeconds, CacheCallBack<T> callback) {
        return getCache(getCacheKey(baseKeys, subKey), null, expireSeconds, callback);
    }

    public static <T> T getCache(String[] baseKeys, String subKey, Class<T> targetClass, String expireSeconds,
                                 CacheCallBack<T> callback) {
        return getCache(getCacheKey(baseKeys, subKey), targetClass, expireSeconds, callback);
    }

    public static void delete(String key) {
        Logger.debug("删除Key:" + key);
        if (StringUtils.isBlank(key)) {
            throw new IllegalArgumentException("key不能为空");
        }
        try {
            cacheImpl.delete(key);
        } catch (Exception e1) {
            Logger.warn(e1, "When delete cache[key:" + key + "] found exception.");
            checkException(e1.getMessage());
        }
    }

    public static void delete(String[] baseKeys, String subKey) {
        delete(getCacheKey(baseKeys, subKey));
    }

    private static String getRandomKey(String originalKey) {
        return originalKey + UUID.randomUUID().toString();
    }

    /**
     * 如果异常中是网络连接问题，直接关闭整个进程. 这是预警方案，先使用这个
     * @param message
     */
    private static void checkException(String message) {
        if (StringUtils.contains(message, "imed out waiting to add net.spy.memcached.protocol.ascii")) {
//            new SMSMessage("Memcached死机了", new String[] {"15026682165", "18121290128"});
            Logger.info("出现致命的Memcache问题，先退出进程");
            Play.fatalServerErrorOccurred();
        }
    }
}
