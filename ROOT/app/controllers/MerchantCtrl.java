package controllers;


import models.constants.DeletedStatus;
import models.merchant.Merchant;
import models.merchant.enums.MerchantType;
import models.merchant.enums.VerifyState;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.mvc.Controller;
import play.mvc.With;
import util.common.ConvertUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 商户设置
 * Created by youliangcheng on 17/6/26.
 */
@With(LoginInCtrl.class)
public class MerchantCtrl extends Controller {
    public static void index() {
        Merchant merchant = LoginInCtrl.getMerchant();
        //        Merchant merchant = Merchant.findByMerchantId(1);
        String mainImage = merchant.imagePath;
        String linkId = merchant.linkId;
        String idImg = merchant.idImg;
        String urlBase = Play.configuration.getProperty("weixin.merchant.url");
        String urlStr = Play.configuration.getProperty("weixin.merchant.qrCode.url") + merchant.linkId;
        String qrCodeBase = urlBase + urlStr;

        render(linkId, merchant, mainImage, idImg, qrCodeBase);
    }

    //保存
    public static void saveMerchant(long id,
                                    String name,
                                    String loginName,
                                    String password,
                                    String servicePhone,
                                    String imageFile,
                                    MerchantType type,
                                    String idCode,
                                    VerifyState state,
                                    String idImg,
                                    String realName,
                                    String classes,
                                    String weChat,
                                    String teacher,
                                    String team,
                                    String address
                                    ) {
        Map<String, Object> resultMap = new HashMap<>();
        Merchant merchant = new Merchant();
        Logger.info("idCode================== : %s", idCode);

        Logger.info("type================== : %s", type);
        Logger.info("state================== :%s", state);
        Merchant isExit = Merchant.findByLoginName(loginName);
        if (isExit != null && isExit.id != id) {
            resultMap.put("success", false);
            resultMap.put("msg", "登录名:" + loginName + " ,已经被使用,换一个登录名吧!");
            renderJSON(resultMap);
        }

        isExit = Merchant.findByName(name);
        if (isExit != null && isExit.id != id) {
            resultMap.put("success", false);
            resultMap.put("msg", "店铺名称:" + name + " ,已经被使用,再给店铺起个名字吧!");
            renderJSON(resultMap);
        }

        if (id > 0) {
            merchant = Merchant.findByMerchantId(id);
            merchant.modifyAt = new Date();
        } else {
            merchant.createAt = new Date();
            merchant.deleted = DeletedStatus.UN_DELETED;

        }
        if (state == VerifyState.WAIT) {
            merchant.verifyState = VerifyState.VERIFY;
        }
        merchant.type = type;
        merchant.idCode = idCode;
        merchant.name = name;
        merchant.servicePhone = servicePhone;
        merchant.imagePath = imageFile;
        merchant.loginName = loginName;
        merchant.idImg = idImg;
        merchant.realName = realName;
        merchant.teacher = teacher;
        merchant.classes = classes;
        merchant.weChat = weChat;
        merchant.team = team;
        merchant.address = address;
        if (StringUtils.isNotBlank(password)) {
            merchant.password = ConvertUtil.toMD5(password);
        }
        merchant.save();

        if (merchant.linkId == null) {
            merchant.linkId = ConvertUtil.toString(merchant.id);
            merchant.save();
        }
        resultMap.put("success", true);
        resultMap.put("linkId", merchant.linkId);
        renderJSON(resultMap);
    }


}
