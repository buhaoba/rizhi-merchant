package controllers;

import models.merchant.Merchant;
import order.OrderItem;
import play.Logger;
import play.mvc.Controller;
import play.mvc.With;
import product.Product;
import product.ProductAccessRecord;
import utils.DateUtil;

import java.util.*;

/**
 * Created by Administrator on 2018/4/23.
 */
@With(LoginInCtrl.class)
public class MerchantStatisticsCtrl extends Controller {

    public static void index() {
        Merchant merchant = LoginInCtrl.getMerchant();
        Logger.info("merchant================== : %s", merchant.id);
        // 获取昨天到今天
        Date nowDay = new Date();
        String startTime = DateUtil.getPastDate(nowDay,1);
        String endTime = DateUtil.dateToString(nowDay,"yyyy-MM-dd");
        render(startTime,endTime);
    }

    public static void merStatistics(String startTime,String endTime){
        Logger.info("startTime================== : %s",startTime);
        Logger.info("endTime================== : %s",endTime);
        Merchant merchant = LoginInCtrl.getMerchant();
        Logger.info("merchant================== : %s", merchant.id);
        Map<String, Object> resultMap = new HashMap<>();
        List<Product> productList = new ArrayList<>();
        List<Product> productAllList = Product.findAllProducts(merchant.id);
        for(Product product : productAllList){
            Product newProduct = new Product();
            newProduct.name = product.name;
            List<Map<String,Object>> sumAmountMapList = OrderItem.findGoodsBuySumByGoodId(startTime,endTime,product.id);
            newProduct.salesvolumes = 0;
            for(Map<String,Object> sumAmountMap :sumAmountMapList){
               String buyNumber = sumAmountMap.get("buyNumber").toString();
                newProduct.salesvolumes = Integer.parseInt(buyNumber);
            }
            List<ProductAccessRecord> productAccessRecords = ProductAccessRecord.findListByProductIdAndDay(DateUtil.stringToDate(startTime,"yyyy-MM-dd"),DateUtil.stringToDate(endTime,"yyyy-MM-dd"),product.id);
            Logger.info("productAccessRecords============ : %s",productAccessRecords.size());
            newProduct.visitCount = productAccessRecords.size();
            productList.add(newProduct);
        }
        resultMap.put("productList",productList);
        renderJSON(resultMap);
    }

}
