package controllers.jobs;

import bill.Bill;
import enums.BillOutStatus;
import enums.DistributeStatus;
import models.merchant.Merchant;
import order.DeliveryItem;
import order.Order;
import order.OrderItem;
import play.Logger;
import play.jobs.Job;
import play.jobs.On;
import utils.DateUtil;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2018/4/11.
 */
@On("00 00 01 * * ?")
public class OrderDistributionJob  extends Job {
    public void doJob() {
        Logger.info("OrderDistributionJob------------ : %s","订单收货定时任务");
        Date beforeDay = DateUtil.getBeforeDate(new Date(),4);
        List<DeliveryItem> deliveryItemList = DeliveryItem.find("distributeStatus = ?1 and orderItem.createdAt >= ?2 and orderItem.createdAt <= ?3" , DistributeStatus.SEND_DISTRIBUTE ,DateUtil.getBeginOfDay(beforeDay),DateUtil.getEndOfDay(beforeDay)).fetch();
        if(deliveryItemList != null && deliveryItemList.size() > 0){
            for(DeliveryItem deliveryItem : deliveryItemList){
                updateBillWithdrawalsAmount(deliveryItem);
            }
        }

    }

    public synchronized void  updateBillWithdrawalsAmount(DeliveryItem deliveryItem){
        deliveryItem.distributeStatus = DistributeStatus.USER_GET;
        deliveryItem.save();
        Logger.info("================ OrderDistributionJob 将deliveryItem :%s ,distributeStatus状态改为：USER_GET" , deliveryItem);
        Bill bill = Bill.getBillByOrderItem(deliveryItem.orderItem);
        Logger.info("================ OrderDistributionJob  获取关联账单:%s" , bill);

        if(bill != null){
            Merchant merchant = bill.merchant;
            bill.outStatus = BillOutStatus.NORMAL;
            bill.modifyAt = new Date();
            bill.save();
            Logger.info("================ OrderDistributionJob  账单状态修改为:%s", bill.outStatus);
            if(merchant.frozenAmount == null){
                merchant.frozenAmount = 0.0d;
            }else if(merchant.frozenAmount > bill.amount || merchant.frozenAmount == bill.amount){
                merchant.frozenAmount = merchant.frozenAmount-bill.amount;
            }
            merchant.withdrawalsAmount = merchant.withdrawalsAmount == null ? 0.0d : merchant.withdrawalsAmount;
            merchant.withdrawalsAmount  += bill.amount;
            merchant.save();
            Logger.info("================ OrderDistributionJob  商户可提现金额修改商户:%s", merchant);
        }
    }

//    public synchronized void orderDistributionBeforDay(){
//        Date nowDat = new Date();
//        Date beforeDay = DateUtil.getBeforeDate(nowDat,4);
//        List<Order> orders = Order.find("distributeStatus = ?1 and  createdAt between ?2 and ?3 " , DistributeStatus.SEND_DISTRIBUTE,DateUtil.getBeginOfDay(beforeDay),DateUtil.getEndOfDay(beforeDay)).fetch();
//
//
//        for(Order order : orders){
//            order.distributeStatus =  DistributeStatus.USER_GET;
//            OrderItem orderItem = OrderItem.getByOrder(order);
//            if(orderItem != null){
//                DeliveryItem  deliveryItem = DeliveryItem.findOrderItemByDeli(orderItem.id);
//                deliveryItem.distributeStatus = DistributeStatus.USER_GET;
//                deliveryItem.save();
//
//                Bill bill = Bill.getBillByOrderItem(orderItem);
//                if(bill != null){
//                    Merchant merchant = bill.merchant;
//                    bill.outStatus = BillOutStatus.NORMAL;
//                    bill.modifyAt = new Date();
//                    bill.save();
//                    if(merchant.frozenAmount == null){
//                        merchant.frozenAmount = 0.0d;
//                    }else if(merchant.frozenAmount > bill.amount || merchant.frozenAmount == bill.amount){
//                        merchant.frozenAmount = merchant.frozenAmount-bill.amount;
//                    }
//                    merchant.withdrawalsAmount = merchant.withdrawalsAmount == null ? 0.0d : merchant.withdrawalsAmount;
//                    merchant.withdrawalsAmount  += bill.amount;
//                    merchant.save();
//
//                }
//
//            }
//            order.save();
//        }
//
//    }
}
