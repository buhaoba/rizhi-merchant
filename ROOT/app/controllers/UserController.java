package controllers;

import enums.OrderStatus;
import models.merchant.Merchant;
import models.weixin.WebUser;
import models.weixin.WeixinData;
import order.Order;
import order.OrderItem;
import play.Logger;
import play.mvc.Controller;
import play.mvc.With;
import util.common.ConvertUtil;
import util.common.DateUtil;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by youliangcheng on 17/6/24.
 */
@With(LoginInCtrl.class)
public class UserController extends Controller {

    public static void index(){
        Merchant merchant = LoginInCtrl.getMerchant();
        //       Merchant merchant = Merchant.findByMerchantId(5);
        List<WebUser> userList = WebUser.loadByMerchant(merchant.id);
        render(userList);
    }

    public static void userOrders(long userId){
        //时间默认为当前时间
        Date nowTime = new Date();
        String today = DateUtil.dateToString(nowTime , "yyyy-MM-dd");
        String aMonthAgo = DateUtil.dateToString(DateUtil.getDateForLastMonth(nowTime) , "yyyy-MM-dd");
        render(userId ,  today , aMonthAgo);
    }

    public static void getOrders ( long userId ,String beganDate , String endDate ){
        Map<String , Object> resultMap = new HashMap<>();
        Merchant merchant = LoginInCtrl.getMerchant();
        //        Merchant merchant = Merchant.findByMerchantId(1);
        WebUser webUser = WebUser.findByUserId(userId);

        List<Map<String, Object>> orderList = new ArrayList<>();
        DecimalFormat b = new DecimalFormat("#0.00");
        //统计用户总订单量,总订单金额
        Map<String ,Object> orderNumAndAmount = Order.countByWebUserAndMerchant(merchant.id , webUser.id , beganDate , endDate);
        int orderNumber = 0;
        Double totalAmount = 0.00;
        if(orderNumAndAmount != null){
            totalAmount = ConvertUtil.toDouble( orderNumAndAmount.get("total_amount"));
            orderNumber = ConvertUtil.toInteger(orderNumAndAmount.get("order_num")) ;
        }
        //获取订单明细
        List<Order> orders = Order.loadByMerchantAndUser(merchant.id , webUser.id);
        if(orders.size()> 0){
            for(Order order : orders){
                Map<String, Object> orderMap = new HashMap<>();
                List<Map<String, Object>> orderItemList = Order.getOrderItemList(order.id);
                order.kindNumber = OrderItem.countByOrder(order.id);
                order.createdAtStr = DateUtil.dateToString(order.createdAt, "yyyy-MM-dd");
                order.defineStr = b.format(order.paymentedAmount);
                order.orderStatusStr = OrderStatus.getOrderStatus(order.status);


                orderMap.put("order", order);
                orderMap.put("orderItem", orderItemList);
                orderList.add(orderMap);
            }

        }
        resultMap.put("orderNumber" , orderNumber);
        resultMap.put("totalAmount" , totalAmount);
        resultMap.put("orderList" , orderList);
        renderJSON(resultMap);
    }
    public static void indexUser(){
        Merchant merchant = LoginInCtrl.getMerchant();
        WebUser webUser = new WebUser();
        if(merchant.webUserId != null){
               webUser = WebUser.findByUserId(merchant.webUserId);
          }
        render(webUser);
    }
    public static  void  userPhone(String phone,String name) {
        Map<String, Object> resultMap = new HashMap<>();
        Merchant merchant = LoginInCtrl.getMerchant();
        WebUser webUserPhone = WebUser.findByPhoneOccupy(phone, merchant.webUserId == null? 0:merchant.webUserId);
        if (webUserPhone != null) {
            resultMap.put("success", false);
        }else {
            WebUser webUser = new WebUser();
            if(merchant.webUserId != null){
                Logger.info("webUserId=========== : %s",merchant.webUserId);
                webUser = webUser.findById(merchant.webUserId);
                webUser.nickName = name;
                webUser.phone = phone;
                webUser.save();
            }else{
                webUser.nickName = name;
                webUser.weixinData = WeixinData.all().first();
                webUser.phone = phone;
                webUser.save();
                merchant.webUserId = webUser.id;
                merchant.save();
            }
            resultMap.put("success", true);
        }
        renderJSON(resultMap);
    }


}
