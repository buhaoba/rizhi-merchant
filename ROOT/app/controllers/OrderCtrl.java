package controllers;

import bill.Bill;
import enums.*;
import models.merchant.Merchant;
import models.msgtemp.merchant.WxMpMerchantMsgTemp;
import models.weixin.WebUser;
import order.DeliveryItem;
import order.OrderItem;
import play.Logger;
import play.mvc.Controller;
import play.mvc.With;

import java.util.*;

/**
 * Created by youliangcheng on 17/6/24.
 */
@With(LoginInCtrl.class)
public class OrderCtrl extends Controller {
    public static void index() {
        Merchant merchant = LoginInCtrl.getMerchant();
        //        Merchant merchant = Merchant.findByMerchantId(1);
        long merchantId = merchant.id;
        render(merchantId);
    }

    //获取相应状态下的产品订单列表
    public static void getOrderList( OrderStatus orderstatus,DistributeStatus distributeStatus) {
        Merchant merchant = LoginInCtrl.getMerchant();
        Logger.info("-----------------------------------distributeStatus : %s | status : %s",distributeStatus,orderstatus);
        Map<String, Object> resultMap = new HashMap<>();
        List<Map<String, Object>> orderList = new ArrayList<>();
        orderList = OrderItem.getListByMerchant(merchant,distributeStatus);
        resultMap.put("success", true);
        resultMap.put("orderList", orderList);
        renderJSON(resultMap);

    }

    //分配派送单
    public static void distributionOrder(long deliveryId,Double money) {
        Logger.info("-----------------------------------deliveryId : %s",deliveryId);
        Map<String, Object> resultMap = new HashMap<>();
        DeliveryItem deliveryItem = new DeliveryItem();
        deliveryItem = DeliveryItem.findById(deliveryId);
        Logger.info("-----------------------------------deliveryItem : %s",deliveryItem);
        if(deliveryItem != null){
            deliveryItem.deliveryPrice = money;
            deliveryItem.distributeType = DistributeType.OTHERS;
            deliveryItem.distributeStatus = DistributeStatus.DISTRIBUTE;
            deliveryItem.save();
            resultMap.put("success", true);
            resultMap.put("msg", "订单已分配");
        }else{
            resultMap.put("msg", "未找到订单信息");
            resultMap.put("success", false);
        }
        renderJSON(resultMap);
    }

    //自配
    public static void distributionOwn(long deliveryId) {
        Merchant merchant = LoginInCtrl.getMerchant();
        Map<String, Object> resultMap = new HashMap<>();
        Logger.info("-----------------------------------deliveryId : %s",deliveryId);
        DeliveryItem deliveryItem = new DeliveryItem();
        deliveryItem = DeliveryItem.findById(deliveryId);
        Logger.info("-----------------------------------deliveryItem : %s",deliveryItem);
        if(deliveryItem != null){
            OrderItem orderItem = OrderItem.findByIdAndCancelStatus(deliveryItem.orderItem.id);
            if(orderItem != null ){
                resultMap.put("success",false);
                resultMap.put("msg", "订单已取消");
                renderJSON(resultMap);
            }
            deliveryItem.distributeType = DistributeType.OWN;
            deliveryItem.distributeStatus = DistributeStatus.ROB_DISTRIBUTE;
            deliveryItem.createAt = new Date();
            deliveryItem.distributeAt = new Date();
            deliveryItem.save();
            WxMpMerchantMsgTemp.sendMsgForOrderDistribute(deliveryItem.orderItem.order);
            resultMap.put("success", true);
            resultMap.put("msg", "已接单");
        }else{
            resultMap.put("msg", "未找到订单信息");
            resultMap.put("success", false);
        }
        renderJSON(resultMap);
    }


    //商户端确认送达
    public static void disMerRunVoer(long deliveryId) {
        Map<String, Object> resultMap = new HashMap<>();
        DeliveryItem deliveryItem = new DeliveryItem();
        deliveryItem = DeliveryItem.findById(deliveryId);
        String orderNumber = "";
        Logger.info("-----------------------------------deliveryItem : %s",deliveryItem);
        if(deliveryItem != null){
            OrderItem orderItem = OrderItem.findByIdAndCancelStatus(deliveryItem.orderItem.id);
            if(orderItem != null ){
                resultMap.put("success",false);
                resultMap.put("msg", "订单已取消");
                renderJSON(resultMap);
            }
            deliveryItem.distributeType = DistributeType.OWN;
            deliveryItem.distributeStatus = DistributeStatus.SEND_DISTRIBUTE;
            deliveryItem.createAt = new Date();
            deliveryItem.distributeAt = new Date();
            deliveryItem.save();
            orderNumber = deliveryItem.orderItem.order.orderNumber;
            resultMap.put("success", true);
            resultMap.put("msg", "确认送达");


             WxMpMerchantMsgTemp.sendMsgForOrderArrive(deliveryItem.orderItem.order);
        }else{
            resultMap.put("msg", "未找到订单信息");
            resultMap.put("success", false);
        }
        renderJSON(resultMap);
    }


    //取消订单
    public static void cancelItem(long itemId) {
        Merchant merchant = LoginInCtrl.getMerchant();
        Map<String, Object> resultMap = new HashMap<>();
        Logger.info("-----------------------------------itemId : %s",itemId);
        OrderItem orderItem = new OrderItem();
        orderItem = OrderItem.findByIdAndCancelStatus(itemId);
        Logger.info("-----------------------------------orderItem : %s",orderItem);
        if(orderItem == null ){
            orderItem = OrderItem.findByIdDel(itemId);
            orderItem.cancelType = CancelType.MERCHANT;
            orderItem.cancelStatus = CancelStatus.CANCEL;
            orderItem.save();

//            if(merchant.withdrawalsAmount == null || merchant.withdrawalsAmount == 0 || merchant.withdrawalsAmount < orderItem.amount){
//                resultMap.put("msg", "可退款金额不足");
//                renderJSON(resultMap);
//            }
//            WithdrawalsBill withdrawalsBill = new WithdrawalsBill(merchant,IntegerorderItem.amount);
            merchant.frozenAmount =  merchant.frozenAmount == null? merchant.frozenAmount = 0.0d : merchant.frozenAmount ;
             if(merchant.frozenAmount >= orderItem.amount ){
                merchant.frozenAmount = merchant.frozenAmount-orderItem.amount;
            }
            merchant.save();
            WebUser webUser = orderItem.order.webUser;
            webUser.withdrawalsAmount = webUser.withdrawalsAmount == null? 0 + orderItem.amount:webUser.withdrawalsAmount+orderItem.amount;
            webUser.save();
            Bill bill = new Bill();
            bill.amount = orderItem.amount;
            bill.status = BillStatus.OUT_AMOUNT;
            bill.orderItem = orderItem;
            bill.createAt = new Date();
            bill.merchant = merchant;
            bill.outStatus = BillOutStatus.NORMAL;
            bill.webUser = webUser;
            bill.save();
            orderItem.cancelUserId = merchant.webUserId;
            orderItem.cancelType = CancelType.MERCHANT;
            orderItem.save();
            resultMap.put("success", true);
            resultMap.put("msg", "订单已取消");
        }else{
            resultMap.put("msg", "未找到订单信息或订单已取消");
            resultMap.put("success", false);
        }
        renderJSON(resultMap);
    }


}
