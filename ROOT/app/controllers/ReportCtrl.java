package controllers;

import enums.RefundStatus;
import enums.ReportUserType;
import enums.TrialType;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import models.msgtemp.admin.WxMpAdminMsgTemp;
import models.weixin.WebUser;
import order.Report;
import order.ReportRemark;
import play.Logger;
import play.mvc.Controller;
import play.mvc.With;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/9/7.
 */
@With(LoginInCtrl.class)
public class ReportCtrl extends Controller {

    public static  void  index(){
        Merchant merchant = LoginInCtrl.getMerchant();
        Logger.info("Merchant : %s" , merchant.id);
        List<Report> reports = Report.getListByMerchant(merchant);
        List<Report> reportList = new ArrayList<>();
        for(Report report : reports){
            List<ReportRemark>  list = ReportRemark.getListByTypeAndMerchantAndReport(merchant,report);
            Logger.info("list.size : %s" , list);
            if(list.size() == 0){
                reportList.add(report);
            }
        }
        render(reports,reportList);
    }

    public static  void  reportOrder(long id){
        Merchant merchant = LoginInCtrl.getMerchant();
        Logger.info("reportId ======= : %S",id);
        Report report = new Report();
        ReportRemark reportRemark = new ReportRemark();
        if(id > 0){
            report = Report.findById(id);
            reportRemark = ReportRemark.getByMerchantAndReport(merchant,report);
        }
        render(report,reportRemark);
    }

    public static  void  saveReport(Long reportId , String reportRemarks, RefundStatus refundStatus){
        Merchant merchant = LoginInCtrl.getMerchant();
        Map<String , Object> resultMap = new HashMap<>();
        Report report = new Report();
        if(reportId > 0){
            report = Report.findById(reportId);
            if(report != null){
                ReportRemark reportRemark = new ReportRemark();
                reportRemark.reportRemarks = reportRemarks.trim();
                reportRemark.reportUserType = ReportUserType.MERCHANT;
                reportRemark.reportUser = merchant.id;
                reportRemark.report = report;
                reportRemark.deleted = DeletedStatus.UN_DELETED;
                reportRemark.save();
                if(refundStatus != null){
                    report.trial = TrialType.USER;
                    report.refund = refundStatus;
                    report.save();

                    merchant.frozenAmount = merchant.frozenAmount == null ? 0.0d : merchant.frozenAmount;
                    merchant.frozenAmount -= report.amount;
                    merchant.complaintCount = merchant.complaintCount != null ? merchant.complaintCount+1 : 1;

                    merchant.save();
                    WebUser webUser = report.orderItem.order.webUser;
                    webUser.withdrawalsAmount = webUser.withdrawalsAmount == null? 0.0d : webUser.withdrawalsAmount;
                    webUser.withdrawalsAmount += report.amount;
                    webUser.save();
                    WxMpAdminMsgTemp.sendMsgforReportToUser(webUser ,report);
                }
//                WxMsgTempUtil.sendMsgforReportMerchant(merchant);
            }else{
                resultMap.put("success",false);
                resultMap.put("msg","投诉单不存在");
                renderJSON(resultMap);
            }
        }else {
            resultMap.put("success",false);
            resultMap.put("msg","投诉单不存在");
            renderJSON(resultMap);
        }
        resultMap.put("success",true);
        renderJSON(resultMap);
    }

//    /**
//     * 总后台，商户端同意退款：商户投诉
//     * 投诉结果,发送给用户
//     *
//     * @param webUser
//     * @param report
//     * @return
//     */
//    private static Boolean sendMsgforReportToUser(WebUser webUser, Report report) {
//        Boolean sendSuccess = false;
//
//        //判断商户用户是否存在
//        if (report == null || report.trial == null) {
//            Logger.info("------------------投诉结果没有数据,无法发送消息!");
//            sendSuccess = false;
//            return sendSuccess;
//
//        }
//
//        if (report.order == null || report.order.webUser == null) {
//            Logger.info("------------------投诉人信息获取不到,模板消息发送失败!");
//            sendSuccess = false;
//            return sendSuccess;
//        }
//        String resultType = report.trial == TrialType.MERCHANT ? "投诉不成立" : "投诉成立";
//        String resultMsg = report.refund == null ? "," : (report.refund == RefundStatus.ADMIN_AGREES ? " ,管理员同意退款" : " ,卖家同意退款");
//        //判断用户openId是否存在
//        if (webUser == null || webUser.openId == null) {
//            Logger.info("------------------用户openId不存在,无法发送模板消息!");
//
//            sendSuccess = false;
//            return sendSuccess;
//        }
//        String openId = webUser.openId;
//
//        WxMpService wxMpService = WxMpHelper.getWxMpService();
//        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
//        wxMpTemplateMessage.setToUser(openId);
////            wxMpTemplateMessage.setTemplateId("Q4i_69e9KEI649LeyTPn2AxE8f3vCsL327DRdnjmI-Y");
//        wxMpTemplateMessage.setTemplateId("NG32XEgurQOFBN4j3WZj3AgQ33KYN2bguIkVHKALRDY");
//        String weixinUrl = Play.configuration.getProperty("weixin.merchant.url");
//        wxMpTemplateMessage.setUrl(weixinUrl);
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", "您好！您投诉的问题已处理," + resultType + resultMsg));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1", report.order.webUser.nickName));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2", ""));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3", report.orderItem.goods.name));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword4", report.order.orderNumber));
////        wxMpTemplateMessage.getData().add(new WxMpTemplateData("keyword3", adminUser.phoneNo!=null ? adminUser.phoneNo:"" ));
//        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark", "如有疑问请咨询客服.15263399860"));
//        try {
//            wxMpService.templateSend(wxMpTemplateMessage);
//            sendSuccess = true;
//        } catch (WxErrorException e) {
//            Logger.info("weixinCode : %s 发送模板消息失败 . 错误原因 : %s", openId, e.getMessage());
//        }
//
//        return sendSuccess;
//
//    }

}
