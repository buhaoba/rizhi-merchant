package controllers;

import activity.Activity;
import activity.ActivityProduct;
import enums.OrderStatus;
import enums.ShowPosition;
import models.constants.DeletedStatus;
import models.merchant.Merchant;
import order.Order;
import order.OrderItem;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.mvc.Controller;
import play.mvc.With;
import product.Product;
import product.type.ProductType;
import util.common.DateUtil;

import java.util.*;


@With(LoginInCtrl.class)
public class MerchantIndex extends Controller {


    public static void index() {

        Merchant merchant = LoginInCtrl.getMerchant();
        Date beginDate = DateUtil.getBeginOfDay();
        Date endDate = DateUtil.getEndOfDay();
        List<Order> newOrderList = Order.loadByMerchantAndStatus(OrderStatus.PAID, merchant.id,beginDate,endDate);
        if(newOrderList.size() > 10){
            newOrderList = newOrderList.subList(0,10);
        }
        for(Order order : newOrderList){
            List<OrderItem> orderItemList = OrderItem.getListByOrder(order);
            Product product = Product.findByProductId(orderItemList.get(0).goods.serialId);
            order.defineStr = product.mainImage;
        }
        String urlBase = Play.configuration.getProperty("weixin.merchant.url");
        String urlStr = Play.configuration.getProperty("weixin.merchant.qrCode.url")+merchant.linkId;
        String qrCodeBase = urlBase + urlStr ;

        Logger.info("qrCodeBase================== : %s", qrCodeBase);
        render(newOrderList , merchant  , qrCodeBase);
    }

    //商城首页设置
    public static void indexSetUp(){
        Merchant merchant = LoginInCtrl.getMerchant();
        /*
         * 1:轮播图(INDEX_BLOCK)
         * 2:头条(INDEX_NOTICE)
         * 3:秒杀(INDEX_SECKILL)
         * 4:活动产品(INDEX_PROMOTION)
         * 5:宣传活动(INDEX_PRODUCT_RECOMMEND)
         */

        Map<String , Object> resultMap = new HashMap<>();
        List<Activity> recommendActivityList = new ArrayList<>();

        List<Activity> allActivityList =Activity.loadActivityByMerchant(merchant.id);
        if (allActivityList != null && allActivityList.size() > 0) {

            for (Activity activity : allActivityList) {

                if (activity.showPosition == ShowPosition.INDEX_BLOCK) {
                    renderArgs.put("blockActivity", activity);
                } else if (activity.showPosition == ShowPosition.INDEX_NOTICE) {
                    renderArgs.put("noticeActivity", activity);
                } else if (activity.showPosition == ShowPosition.INDEX_SECKILL) {
                    renderArgs.put("seckillActivity", activity);
                } else if (activity.showPosition == ShowPosition.INDEX_PROMOTION) {
                    renderArgs.put("promotionActivity", activity);
                } else if (activity.showPosition == ShowPosition.INDEX_PRODUCT_RECOMMEND) {
                    recommendActivityList.add(activity);
                }
            }
            renderArgs.put("recommendActivityList", recommendActivityList);
        }

        render();

    }

    //添加轮播图banner
    public static void bannerAdd(){
        Merchant merchant = LoginInCtrl.getMerchant();
        Activity blockActivity = Activity.findByPositionAndMer(ShowPosition.INDEX_BLOCK , merchant.id);
        render(blockActivity);
    }

    public static void bannerSave(List<String> imgList){

        Map<String , Object> resultMap = new HashMap<>();
        Merchant merchant = LoginInCtrl.getMerchant();

        if(imgList == null || imgList.size() == 0){
            resultMap.put("success",false);
            resultMap.put("msg","图片不能为空!");
            renderJSON(resultMap);
        }

        Activity blockActivity = Activity.findByPositionAndMer(ShowPosition.INDEX_BLOCK , merchant.id);
        if(blockActivity == null){
            blockActivity = new Activity();
            blockActivity.deleted = DeletedStatus.UN_DELETED ;
            blockActivity.createAt = new Date();
            blockActivity.merchant = merchant ;
            blockActivity.showPosition = ShowPosition.INDEX_BLOCK;
        }else {
            blockActivity.modifyAt = new Date();
        }
        blockActivity.save();


        ActivityProduct.deletedByActivityId(blockActivity.id);
        for(String imageFile : imgList){
            ActivityProduct image = new ActivityProduct();
            image.deleted = DeletedStatus.UN_DELETED ;
            image.createAt = new Date();
            image.activity = blockActivity ;
            image.imgSrc = imageFile ;
            image.save();
        }

        resultMap.put("success",true);
        resultMap.put("router","/setUp/index");
        renderJSON(resultMap);

    }

    //头条
    public static void notice(){

        Merchant merchant = LoginInCtrl.getMerchant();
        Activity noticeActivity = Activity.findByPositionAndMer(ShowPosition.INDEX_NOTICE , merchant.id);
        int number = 0;
        if(noticeActivity != null) {
            number = noticeActivity.getActivityProducts() == null ? 0 :   noticeActivity.getActivityProducts().size();
        }
        render(noticeActivity ,number);
    }

    public static void noticeSave(ActivityProduct activityProduct0 ,
                                  ActivityProduct activityProduct1 ,
                                  ActivityProduct activityProduct2  ){

        Merchant merchant = LoginInCtrl.getMerchant();
        Activity noticeActivity = Activity.findByPositionAndMer(ShowPosition.INDEX_NOTICE , merchant.id);
        if(noticeActivity == null){
            noticeActivity = new Activity();
            noticeActivity.deleted = DeletedStatus.UN_DELETED;
            noticeActivity.createAt = new Date();
            noticeActivity.merchant = merchant ;
            noticeActivity.showPosition = ShowPosition.INDEX_NOTICE;
        }else {
            noticeActivity.modifyAt = new Date();
        }
        noticeActivity.save();

        if(StringUtils.isNotBlank(activityProduct0.title )){
            if(activityProduct0.id == null){
                activityProduct0.deleted = DeletedStatus.UN_DELETED ;
                activityProduct0.createAt = new Date();
                activityProduct0.activity = noticeActivity ;
            }
            activityProduct0.save();
        }

        if(StringUtils.isNotBlank(activityProduct1.title )){
            if(activityProduct1.id == null){
                activityProduct1.createAt = new Date();
                activityProduct1.deleted = DeletedStatus.UN_DELETED ;
                activityProduct1.activity = noticeActivity ;
            }
            activityProduct1.save();
        }

        if(StringUtils.isNotBlank(activityProduct2.title )){
            if(activityProduct2.id == null){
                activityProduct2.deleted = DeletedStatus.UN_DELETED ;
                activityProduct2.activity = noticeActivity ;
                activityProduct2.createAt = new Date();
            }
            activityProduct2.save();
        }

        redirect("/setUp/index");
    }


    //秒杀
    public static void seckill(){
        Merchant merchant = LoginInCtrl.getMerchant();
        Activity seckillActivity = Activity.findByPositionAndMer(ShowPosition.INDEX_SECKILL , merchant.id);
        //获取商户所有产品
        List<ProductType> firstTypeList = Product.getProductListByType();

        Date nowTime = new Date();
        //将时间格式设置为:yyyy/MM/dd HH:mm,方便html页面将时间转换为Date时间格式
        String  beginAt = DateUtil.dateToString(nowTime ,"yyyy/MM/dd HH:mm");
        String  endAt = DateUtil.dateToString(nowTime ,"yyyy/MM/dd HH:mm");
        if(seckillActivity != null){
            beginAt =DateUtil.dateToString(seckillActivity.beginAt != null ? seckillActivity.beginAt : nowTime,"yyyy/MM/dd HH:mm");
            endAt =DateUtil.dateToString(seckillActivity.endAt != null ? seckillActivity.endAt : nowTime,"yyyy/MM/dd HH:mm");
        }

        render(seckillActivity , firstTypeList , beginAt , endAt);
    }

    public static void seckillSave(Activity seckillActivity ,
                                   ActivityProduct product0 ,
                                   ActivityProduct product1 ,
                                   ActivityProduct product2 ){

        Merchant merchant = LoginInCtrl.getMerchant();

        if(seckillActivity.id == null){
            seckillActivity.createAt = new Date();
            seckillActivity.deleted = DeletedStatus.UN_DELETED;
            seckillActivity.merchant = merchant;
            seckillActivity.showPosition = ShowPosition.INDEX_SECKILL;

        }else {
            seckillActivity.modifyAt = new Date();
        }
        seckillActivity.beginAt = DateUtil.stringToDate(seckillActivity.beginAtStr,"yyyy-MM-dd HH:mm");
        seckillActivity.endAt = DateUtil.stringToDate(seckillActivity.endAtStr,"yyyy-MM-dd HH:mm");

        seckillActivity.beginTimes = DateUtil.dateToString(seckillActivity.beginAt,"HH:mm");
        seckillActivity.endTimes = DateUtil.dateToString(seckillActivity.endAt,"HH:mm");
        seckillActivity.save();


        if(product0.product != null){
            if(product0.id == null){
                product0.deleted = DeletedStatus.UN_DELETED;
                product0.createAt = new Date();
                product0.activity = seckillActivity;
            }
            product0.save();
        }

        if(product1.product != null){
            if(product1.id == null){
                product1.deleted = DeletedStatus.UN_DELETED;
                product1.createAt = new Date();
                product1.activity = seckillActivity;
            }
            product1.save();
        }

        if(product2.product != null){
            if(product2.id == null){
                product2.deleted = DeletedStatus.UN_DELETED;
                product2.createAt = new Date();
                product2.activity = seckillActivity;
            }
            product2.save();
        }

        redirect("/setUp/index");


    }

    //单款活动产品
    public static void promotion(){
        Merchant merchant = LoginInCtrl.getMerchant();
        Activity promotionActivity = Activity.findByPositionAndMer(ShowPosition.INDEX_PROMOTION , merchant.id);
        render(promotionActivity);
    }

    public static void promotionSave(long promotionActivityId , String imageFile){

        Map<String , Object> resultMap = new HashMap<>();
        Merchant merchant = LoginInCtrl.getMerchant();
        if(imageFile == null || imageFile.length() == 0){
            resultMap.put("success",false);
            resultMap.put("msg","请上传图片!");
            renderJSON(resultMap);
        }



        Activity promotionActivity = Activity.findByActivityId(promotionActivityId);
        if(promotionActivity == null ){
            promotionActivity = new Activity();
            promotionActivity.createAt = new Date();
            promotionActivity.deleted = DeletedStatus.UN_DELETED;
            promotionActivity.merchant = merchant ;
            promotionActivity.showPosition = ShowPosition.INDEX_PROMOTION;

        }else {
            promotionActivity.modifyAt = new Date();
        }
        promotionActivity.mainImg = imageFile ;
        promotionActivity.save();

        resultMap.put("success",true);
        resultMap.put("routerr","/setUp/index");
        renderJSON(resultMap);

    }

    public static void recommend(long id){

        Merchant merchant = LoginInCtrl.getMerchant();
        Activity recommendActivity = Activity.findByActivityId(id);
        List<ProductType> firstTypeList = Product.getProductListByType();
        String imagefile = recommendActivity != null && recommendActivity.mainImg != null ? recommendActivity.mainImg: "";
        render(recommendActivity , firstTypeList , imagefile);
    }


    public static void recommendSave(Activity recommendActivity ,
                                     ActivityProduct product0 ,
                                     ActivityProduct product1,
                                     ActivityProduct product2,
                                     ActivityProduct product3,
                                     ActivityProduct product4,
                                     ActivityProduct product5){

        Merchant merchant = LoginInCtrl.getMerchant();
        if(recommendActivity.id== null){
            recommendActivity.deleted = DeletedStatus.UN_DELETED ;
            recommendActivity.createAt = new Date();
            recommendActivity.merchant = merchant;
            recommendActivity.showPosition = ShowPosition.INDEX_PRODUCT_RECOMMEND;
        }else {
            recommendActivity.modifyAt = new Date();
        }

        recommendActivity.save();

        if(product0.id == null){
            product0.deleted = DeletedStatus.UN_DELETED ;
            product0.createAt = new Date();
            product0.activity = recommendActivity;
        }else {
            product0.modifyAt = new Date();
        }
        product0.save();

        if(product1.id == null){
            product1.deleted = DeletedStatus.UN_DELETED ;
            product1.createAt = new Date();
            product1.activity = recommendActivity;
        }else {
            product1.modifyAt = new Date();
        }
        product1.save();


        if(product2.id == null){
            product2.deleted = DeletedStatus.UN_DELETED ;
            product2.createAt = new Date();
            product2.activity = recommendActivity;
        }else {
            product2.modifyAt = new Date();
        }
        product2.save();


        if(product3.id == null){
            product3.deleted = DeletedStatus.UN_DELETED ;
            product3.createAt = new Date();
            product3.activity = recommendActivity;
        }else {
            product3.modifyAt = new Date();
        }
        product3.save();


        if(product4.id == null){
            product4.deleted = DeletedStatus.UN_DELETED ;
            product4.createAt = new Date();
            product4.activity = recommendActivity;
        }else {
            product4.modifyAt = new Date();
        }
        product4.save();

        if(product5.id == null){
            product5.deleted = DeletedStatus.UN_DELETED ;
            product5.createAt = new Date();
            product5.activity = recommendActivity;
        }else {
            product5.modifyAt = new Date();
        }
        product5.save();

        redirect("/setUp/index");

    }

    public static void recommendDel(long id){
        Activity activity = Activity.findByActivityId(id);
        Map<String, Object> resultMap = new HashMap<>();
        if (activity == null) {
            resultMap.put("success", false);
            resultMap.put("msg", "活动不存在,可能已经被删除!");
            renderJSON(resultMap);
        }
        activity.deleted = DeletedStatus.DELETED;
        activity.save();
        resultMap.put("success", true);
        renderJSON(resultMap);
    }




}